================
Über dieses Buch
================

Das Plone-Benutzerhandbuch enthält eine Dokumentation des Content-Management-Systems
Plone_ in der Version |version|. [#]_ Es richtet sich an Benutzer, die als Autoren und
Redakteure mit Plone arbeiten sowie an Administratoren, die eine Plone-Website
konfigurieren und administrieren. Diese Version wurde speziell auf die Plone-Installation
an der Universität Bonn abgestimmt.


Dieses Handbuch finden Sie als PDF zum Download unter http://www.plone.uni-bonn.de

Die Dokumentation basiert in weiten Teilen auf dem Plone-Benutzerhandbuch [#]_
von Thomas Lotze und Jan Ulrich Hasecke.

.. toctree::
   :maxdepth: 1

   copyright.rst
   danksagungen.rst

4. erweiterte Auflage
=====================

**Hochschulrechenzentrum der Universität Bonn**

Für die vierte Auflage des Plone-Handbuches der Universität Bonn wurde dieses um
zusätzliche Informationen erweitert. Neben neuen Plone-Produkten wurde vor allem die FAQ
der Webseite in das Handbuch integriert - hier werden Fragen von Plone-Anwendern der
Universität Bonn aufgegriffen. Zusätzlich wurde das Stichwortverzeichnis umfassend
überarbeitet. Die neue Version bezieht sich auf die Plone-Version 3.3.6.

**Ausblick**
Die fünfte Auflage ist schon in Vorbereitung. Sie wird im WiSe 2015/16 erscheinen und ein Kapitel zu Barrierefreiheit und generellen Tipps für Redakteure zum Thema "Schreiben im Web" enthalten.


Kommentare und Fragen nehmen wir gern per E-Mail entgegen: plone@uni-bonn.de

Projektleiter: Martin Ragg

Redakteure: Arthur Lackner, Florian Lanzmich, Regine Gerhards, Dietmar Rempel,  Florian Schneider

.. only:: html

    .. rubric:: Fußnoten

.. [#] http://plone.org/products/plone/releases/3.3.5

.. [#] http://www.plone-entwicklerhandbuch.de

.. [#] Lotze, Thomas, und Jan Ulrich Hasecke. Plone-Benutzerhandbuch. gocept, 2008.


.. _Plone: http://plone.org

.. _Plone-Entwicklerhandbuch: http://www.plone-entwicklerhandbuch.de

.. _Creative-Commons-Lizenz: http://creativecommons.org/licenses/by-nc-sa/2.0/de/

.. _Launchpad: https://launchpad.net/plone-benutzerhandbuch
