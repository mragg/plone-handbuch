﻿.. index:: Benutzer
.. _sec_benutzer:

========
Benutzer
========

Plone bringt eine Verwaltung für Benutzer und Gruppen mit. Sie haben für Ihre Instanz die
vollständige Hoheit über die Zuweisung von Rollen und Rechten.

Plone macht es möglich, dass einzelne Benutzer vom globalen Administrationsrecht bis hin
zum Bearbeiten einer einzigen Seite jedes Recht zugewiesen bekommen können. Benutzer des
Uni-Bonn Plone können immer nur Mitarbeiter sein, die einen Account im
Hochschulrechenzentrum angelegt haben. Dieser Account gilt dann mit Alias und Passwort
als Zugang zu Plone.

Wenn die oben genannte Vorraussetzung erfüllt ist, können Sie als Administrator die
Zugriffsrechte vergeben.

:guilabel:`Anmerkung: Auch, wenn Ihnen Plone an  mancher Stelle zeigt, dass Sie neue
Benutzer anlegen können, so funktioniert das im Uni-Bonn Plone nicht!`

Als erstes erklären wir Ihnen, wie Sie :guilabel:`globale Portalrechte` vergeben. Hierzu
findet sich in der »Konfiguration« der Bereich »Benutzer und Gruppen«. Dort gibt es die
Punkte »Benutzer«, »Gruppen« und »Einstellungen«. Die Einstellungen sind standardmäßig
schon korrekt eingestellt und benötigen keine weitere Bearbeitung.

Benutzer
========

Sie finden hier ein Suchfeld, dass auf Vor- und Nachname der Mitarbeiterdatenbank
zugreift. Auch die Suche nach dem Vornamen allein oder einer Teileingabe ist zum Beispiel
möglich. Auch die Suche nach Namen mit Umlauten ist inzwischen kein Problem mehr.

Wenn der gesuchte Benutzer gefunden ist, erscheint eine Tabelle in der mittels
Mehrfachauswahl die Zugriffsrechte verteilt werden können. Beachten Sie hierbei, dass es
sich immer um globale Rechte handelt - sich also auf alle Artikel ihres Portals auswirkt.

.. index:: Zugriffsberechtigungen

Lokale Zugriffsberechtigungen
=============================

Wie bereits oben kurz erwähnt, kann auch der Zugriff an einzelnen Teilbereichen Ihres
Portals vergeben werden. So sollen zum Beispiel die Mitarbeiter aus Abteilung A nicht die
News von Abteilung B bearbeiten können. Für diese Einstellung hat jeder Artikeltyp, bei
dem eine solche Option verfügbar ist, den Reiter »zugriff« in der Bearbeitungsleiste.
Achten Sie bei der Anwendung auf die Informationsbox, ob Sie die Standardseite eines
Ordners bearbeiten.

Die erste Ansicht zeigt Ihnen, welche Benutzer oder Gruppen Zugriffsrechte an diesem
Artikel haben. Allerdings gilt das nicht für Benutzer und Gruppen, die über die bereits
erläuterten globalen Rechte verfügen.

Die Rechte an dieser Stelle lassen sich genau so vergeben, wie auch in der Konfiguration:
Namen suchen & Haken setzen.

Vergebene Zugriffsrechte vererben sich von diesem Ort an abwärts, es sei denn, die
Vererbung wird durch deaktivieren der Option »Berechtigungen von übergeordneten Ordnern
übernehmen« unterbrochen.

Zum Abschluss hier eine Übersicht über alle Zugriffsrechte, die Sie vergeben können:

   Zielgruppenredakteur
        kann bestehende Zielgruppen zu Artikeln zuordnen.

   Zielgruppenmanager
        kann neue Zielgruppen erstellen und diese zuordnen.

   Hinzufügen
        ermöglicht es, neue Artikel zu erstellen und zu speichern. Veröffentlichen oder
        das Bearbeiten von anderen Artikeln ist nicht möglich. Dieses Recht beinhaltet
        nur das Recht »Ansehen«.

   Discussionmanager
        kann Kommentare bearbeiten, löschen oder freischalten, sofern diese an Artikeln
        aktiviert sind.

   Bearbeiten
        schließt die Rechte »Hinzufügen« und »Ansehen« mit ein. Zusätzlich ist es möglich,
        jeden Artikel im zugewiesenen Bereich des Zugriffs zu verändern.

   Benutzer
        bedeutet, Sie können weiteren Benutzern den Zugriff zu den Artikeln gewähren, für
        die Sie selbst über Benutzerrechte verfügen.

   Ansehen
        ist sozusagen das einfachste Recht. Es ermöglicht das Ansehen von privaten
        Artikeln. Jegliche Bearbeitung ist allerdings nicht möglich.

   Veröffentlichen
        schließt die Rechte »Hinzufügen«, »Bearbeiten« und »Ansehen« mit ein. Zusätzlich
        lassen sich private Artikel direkt veröffentlichen.

   Verwalten
        umfasst alle anderen Benutzerrechte und ermöglicht darüber hinaus das Bearbeiten
        der Portletspalten.

   Passwort zurücksetzen
        steht im Uni-Bonn Plone nicht zur Verfügung.

   Benutzer löschen
        steht im Uni-Bonn Plone nicht zur Verfügung.

Benutzerrechte auflisten
------------------------
Mit dem Tool Cassandra können Sie sich die in einem Plone vergebenen Rechte auswerten lassen: :ref:`sec_rechte_uebersicht`
