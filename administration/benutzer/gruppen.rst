.. index:: Gruppen
.. _sec_gruppen:

======================
Gruppen und Funktionen
======================

Wir empfehlen bei den meisten Portalen das Arbeiten mit Benutzergruppen. Das ermöglicht
Ihnen auch bei hoher Mitarbeiterfluktuation (SHK's, WHK's), die Übersicht zu behalten.
Schon in der einfachen Ansicht bei Klick auf den Reiter »Gruppen« erkennt man den größten
Vorteil: Auf einen Blick sehen Sie alle Gruppen, die erstellt worden sind und derzeit
über bestimmte Rechte verfügen. Haben Sie einem Benutzer direkte Rechte gegeben, müssen
Sie sich selbst merken, welcher Benutzer über Rechte auf Ihrem Portal verfügt. Es gibt
leider keine Anzeige, die Ihnen alle Mitarbeiter mit Zugriffsrechten auf Ihrem Portal
anzeigt.

Gruppen
=======

Plone kann Benutzer einer Website zu Gruppen zusammenfassen. Gruppen bilden beispielsweise
die Organisationsstruktur der Firma oder Gemeinschaft ab, die die Website betreibt. Ein
Benutzer kann mehreren Gruppen angehören.

Gruppen anzulegen und ihnen Benutzer zuzuordnen ist Aufgabe des Administrators. Plone
bringt zwei vordefinierte Gruppen mit: »Administratoren« und »Redakteure«. Zudem gibt es
die Gruppe »Angemeldete Benutzer«, die immer die gerade an der Website angemeldeten
Benutzer enthält. Weitere Gruppen müssen Sie zu Beginn einmal selbst erstellen und können
dieser Gruppe dann wie einem einzelnen Benutzer ein oder mehrere Rechte zuweisen.

Bei Klick auf den Gruppennamen gelangen Sie zur Übersicht der Gruppenmitglieder. Hier
sehen Sie, welche Mitarbeiter bereits hinzugefügt wurden und können mittels Suchfeld -
wie oben unter Benutzer beschrieben - neue Mitarbeiter einfügen. Jedes Gruppenmitglied
verfügt automatisch über die Rechte der Gruppe.

Ist Ihre Website entsprechend konfiguriert, legt Plone für jede neue Gruppe einen
Gruppenarbeitsplatz an. Dabei handelt es sich um Ordner, die in einem eigenen
Gruppenbereich liegen. Der Gruppenarbeitsplatz hat für die Gruppe die gleiche Bedeutung
wie der persönliche Ordner für einen Benutzer. Für die vordefinierten Gruppen gibt es
keine Gruppenarbeitsplätze.

Sie erreichen den Gruppenbereich über den Reiter »Gruppen« in der Hauptnavigation.

Da eine Gruppe der Besitzer ihres Gruppenarbeitsplatzes ist, können alle Gruppenmitglieder
dort Artikel anlegen, bearbeiten und löschen. Der Gruppenarbeitsplatz eignet sich somit
beispielsweise dafür, gemeinsam einen Artikel für die Veröffentlichung auf der Website
vorzubereiten.

Gruppenarbeitsplätze sind zunächst nicht öffentlich einsehbar, da sie den Status »privat«
haben. Wenn der Gruppenarbeitsplatz veröffentlicht wurde, kann er von allen eingesehen
werden, Artikel im Revisionsstatus »privat« bleiben jedoch nur für die Mitglieder der
jeweiligen Gruppe sichtbar. Jedes Mitglied der Gruppe kann den gesamten
Gruppenarbeitsplatz privat schalten oder zur Veröffentlichung einreichen.


.. index:: Funktionen (Zugriff), Zugriffsrechte
.. _sec_benutzer-rollen:

Funktionen
==========

Ein CMS wie Plone ermöglicht es Ihnen, auf Ihrer Website mit anderen
Personen zusammenzuarbeiten. Dabei übernehmen die einzelnen Benutzer
häufig ganz unterschiedliche Funktionen und benötigen dafür bestimmte
Zugriffsrechte. Während einige Benutzer Artikel verfassen, sind andere
dafür verantwortlich, sie zu prüfen und zu veröffentlichen. Wiederum
andere Benutzer können beispielsweise die Struktur der Website
verändern und neue Benutzer hinzufügen.

Ein und derselbe Benutzer kann auch an unterschiedlichen Stellen der
Website verschiedene Funktionen ausüben. So ist es möglich, dass Sie
auf einige Bereiche Ihrer Website gar keinen Zugriff haben, in anderen
lediglich Artikel lesen und nur in einem bestimmten Bereich selbst
Artikel verfassen können.

All diese Fälle werden von Plone berücksichtigt und unterstützt. Plone
unterscheidet sehr genau, welche Benutzer an welchen Stellen der
Website Artikel anschauen, anlegen, bearbeiten oder veröffentlichen
dürfen.

Um Berechtigungen und Einschränkungen übersichtlich und detailliert
verwalten zu können, gibt es Funktionen. Wer beispielsweise dafür
zuständig ist, Artikel vor ihrer Veröffentlichung auf der Website zu
prüfen, muss die Funktion »Veröffentlichen« ausüben dürfen.

Wenn Sie sich als Benutzer an einer Plone-Website registrieren, erhalten Sie
bereits eine oder mehrere Funktionen. Sie können weder eigenständig weitere
Funktionen übernehmen, noch Funktionen abgeben, die Sie bereits haben. Ihr
Administrator kann Ihnen jedoch jederzeit neue Funktionen zuteilen oder
bestehende entziehen.

Funktionen können nicht nur einzelnen Benutzern, sondern auch Gruppen
zugeordnet werden. Sollen mehrere Benutzer die gleiche Funktion
ausüben, ist es oft sinnvoll, sie in einer Gruppe zusammenzufassen und
die Funktion der Gruppe zu übertragen. Die Funktion gilt dann für alle
Gruppenmitglieder.

.. _sec_standardfunktionen:

Standardfunktionen
==================

Jede Plone-Website kennt wenigstens sieben Funktionen:


Benutzer
  Wer sich auf der Website als Benutzer registriert, erhält je
  nach Voreinstellung bestimmte Rechte zugeteilt. Unter Umständen dürfen nur
  registrierte Benutzer überhaupt auf die Website zugreifen (etwa bei einem
  Intranet).

Besitzen
  Wer als Benutzer einen Artikel anlegt, »besitzt« ihn. Mit
  dieser Funktion gehen eine Reihe von Berechtigungen einher. So kann man
  Artikel, die man besitzt, bearbeiten und wieder löschen.

Hinzufügen
  Der Benutzer kann neue Artikel hinzufügen, nicht aber
  bestehende Artikel bearbeiten.

Bearbeiten
  Der Benutzer kann Artikel bearbeiten, er kann den Inhalt und
  die Metadaten eines Artikels verändern.

Ansehen
  Der Benutzer kann Artikel im Webbrowser aufrufen und anschauen.

Veröffentlichen
  Der Benutzer kann Artikel veröffentlichen, sodass sie
  von allen Benutzern und Besuchern der Website eingesehen werden können.

Verwalten
  Der Benutzer darf im CMS Verwaltungsaufgaben
  erledigen. Darunter fallen unter anderem die Benutzerverwaltung, also
  beispielsweise die Vergabe von Funktionen an Benutzer und Gruppen, sowie
  Änderungen an der Struktur und an grundlegenden Funktionen der Website.

Sie bekommen in Plone nur die Funktionen zu Gesicht, die Sie selbst ausüben
und in der Artikelansicht »Zugriff« delegieren dürfen.

.. index:: Zugriff
.. _sec_reiter_zugriffsrechte:

Artikelansicht »Zugriff«
=========================

Wenn Sie Besitzer oder Verwalter eines Artikels sind, steht Ihnen die
Ansicht »Zugriff« zur Verfügung. Dort können Sie Funktionen an andere
Benutzer und Gruppen übertragen, um beispielsweise Aufgaben an Mitarbeiter zu
delegieren.

.. _fig_zugriff:

.. figure:: ./images/zugriff.*
   :width: 100%
   :alt: Die Artikelansicht Zugriff

   Die Artikelansicht »Zugriff«

Die Ansicht »Zugriff« (siehe Abbildung :ref:`fig_zugriff`) ist
folgendermaßen aufgebaut:


Suchfeld
  Mit dem Suchfeld können Sie nach Benutzern und Gruppen suchen,
  wenn deren Namen nicht bereits in der Tabelle darunter aufgeführt sind.

Tabelle der übertragenen Funktionen
  Die Tabelle gibt Ihnen einen
  Überblick darüber, an welche Benutzer und Gruppen welche Funktionen
  übertragen wurden.

Berechtigungen von übergeordneten Ordnern übernehmen
  In der Regel ist diese Option eingeschaltet. In diesem Fall werden
  Funktionen von übergeordneten Ordnern an den aktuellen Artikel
  vererbt. Wer beispielsweise in einem Ordner Artikel hinzufügen darf,
  kann das dann auch in seinen Unterordnern.

  Übernommene Funktionen werden mit einem grünen Häkchen symbolisiert. Ein
  Kreis mit drei roten Punkten steht für Funktionen, die vom Administrator
  global auf der gesamten Website vergeben worden sind.

Wenn Sie einem Benutzer eine Funktion übertragen wollen, suchen Sie zunächst
im Suchfeld nach seinem Namen. Alle Benutzer, auf die Ihr Suchbegriff passt,
werden in der Tabelle aufgelistet. Sie übertragen eine Funktion, indem Sie
in der Zeile mit dem Namen des Benutzers ein Häkchen in dem entsprechenden
Kästchen setzen. Wenn der Benutzer beispielsweise den Inhalt von Artikeln
verändern soll, setzen Sie ein Häkchen bei der Funktion »Kann bearbeiten«.

Wenn Sie Funktionen an eine Gruppe übertragen wollen, verfahren Sie
genauso. Sie erkennen eine Gruppe an dem Gruppensymbol neben dem Namen.
