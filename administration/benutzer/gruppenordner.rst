.. index:: Gruppenordner
.. _sec_gruppenordner:

======================
Gruppenordner in Plone
======================

Sie können in Plone Ordner für Gruppen aktivieren - dies ist z.B. vor allem für den
Aufbau eines Intranets interessant, da so automatisch gemeinsame Arbeitsbereiche angelegt
werden.


Aktivierung
===========

Gruppenarbeitsplätze müssen einmalig in Plone durch den Administrator aktiviert werden.
Dazu müssen Sie unter »Konfiguration« -> »Zope-Management-Interface« auf den Punkt
»portal_group« wechseln. Klicken Sie hier bitte auf »Turn workspace creation on«
(Hinweis: Plone erzeugt keine Arbeitsordner für schon angelegte Gruppen!)
 

Gruppenarbeitsplätze
====================

Wenn Sie nun eine neue Benutzergruppe anlegen, bekommen alle Mitglieder dieser Gruppe
automatisch einen gemeinsamen Arbeitsbereich zugewiesen, zu dem nur Mitglieder dieser
Gruppe Zugriff haben.

Nach dem Login haben alle Benutzer, die Mitglied einer Gruppe sind, den neuen
Navigationspunkt »groups«, hier finden sich dann als Unternavigation alle Arbeitsbereiche
der Benutzergruppen.

Diese Gruppenarbeitsplätze lassen sich dann ganz individuell in Ihre Szenarien eines
Intranets einbinden. Wenn Sie z.B. für Ausschüsse etc. eigene Arbeitsbereiche mit
Informationsseiten, Dateiablagemöglichkeit etc. aufbauen möchten, könnten Sie hierfür
jeweils eigene Gruppen anlegen und haben so den Vorteil, dass Sie sich nicht mehr um die
Navigationsstruktur/Rechtevergabe in Plone kümmern müssen.

(Sie können dasselbe Ergebnis natürlich auch mit ganz normalen Bordmitteln erreichen,
indem Sie eine Navigationsstruktur anlegen und den Zugriff jeweils pro Ordner
einschränken. Die Gruppenarbeitsplätze lohnen sich, wenn Sie viele Arbeitsgruppen
haben, da sich der Aufwand nur noch auf das Anlegen der Gruppe und der späteren Zuordnung
der Benutzer zu den Gruppen beschränkt).