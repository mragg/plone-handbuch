﻿.. index:: Gruppenportlet
.. _sec_gruppenportlet:

==============
Gruppenportlet
==============

Sie können einer Gruppe gezielt Portlets zuweisen (Konfiguration->Benutzer->Gruppen->Portlet). Gruppenportlets sehen nur angemeldete Mitglieder der Gruppe. Sie können darüber z.B. Nachrichten an eine Gruppe einblenden lassen. Interessant ist es auch darüber "Helfer" einzublenden, die den Redaktionsalltag erleichtern, z.B. eine Kollektion, die die letzten 10 veröffentlichten Artikel anzeigt.
