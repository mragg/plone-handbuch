.. index:: Profil, Gruppen, Funktionen (Zugriff)
.. _sec_benutzer_und_gruppen:

====================
Benutzer und Gruppen
====================

Die meisten Benutzer einer Plone-Website sind in der Regel anonyme Besucher, die die
angebotenen Artikel lesen möchten. Einige sind darüber hinaus an der Veröffentlichung der
Inhalte aktiv beteiligt. Sie sind der Website als angemeldete Benutzer bekannt.
Angemeldete Benutzer besitzen individuelle Rechte, um an Artikeln zu arbeiten. Sie können
zudem in Gruppen zusammengefasst werden.

.. toctree::
   :maxdepth: 1

   benutzer.rst
   gruppen.rst
   gruppenordner.rst
   gruppenportlet.rst
   intranet.rst
