.. index:: Intranet
.. _sec_intranet:

===================
Intranet einrichten
===================

Intranet - keep it simple
-------------------------
Die einfachste und schnellste Methode wie Sie in Ihrem Plone eine Art Intranet erzeugen können: Legen Sie einen Ordner an in dem Ihr Intranet stattfinden soll und lassen Sie diesen immer auf "Privat" stehen. Unter "Zugriff" geben Sie jetzt den Gruppen das Recht "Lesen", die auf diesen Ordner zugreifen sollen. Bei kleineren Einrichtungen ist es sicherlich möglich die Uni-IDs aller Mitarbeiter in einer eigenen Gruppe zu pflegen. Bei größeren Gruppen kann dies aufwändig sein - je nachdem wie schützenswert Ihre Daten sind, reicht es aber unter Umständen aus, den Schutz auf "Angemeldete Benutzer" einzuschränken. Darüber haben Sie den Zugriff pauschal auf alle Uni-IDs eingerichtet. Für viele Intranets in Einrichtungen der Uni Bonn reicht dieses Level aus.

Intranet - größer und sicherer
------------------------------
Wenn Sie ein größeres Intranet planen und das Sicherheitsniveau hierfür höher ansetzen möchten, empfehlen wir Ihnen das Intranet in einer eigenen Plone-Instanz zu pflegen und die Daten nicht in einem Unterordner Ihres normalen Außen-Webauftritts zu pflegen. Sie sind hier deutlich flexibler und können beispielsweise einen IP-Schutz auf das komplette Intranet legen lassen (zusätzlich zu der Zuweisung von Leserechten).

Intranet-Workflow
-----------------
Es gibt in Plone einen eigenen Intranet-Workflow. Dieser ermöglicht Ihnen einen zusätzlichen Status "Intern veröffentlichen". Wir können diesen Workflow nur in Ausnahmefällen empfehlen, da er in der Bearbeitung nur bei versierten Redakteuren sinnvoll einsetzbar ist.
