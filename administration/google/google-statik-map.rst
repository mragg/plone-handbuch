.. index:: Google-Maps
.. _sec_goole_statik_maps:

==================
Google Static Maps
==================

.. note::
   Die hier beschriebene Static Maps API ist veraltet, funktioniert bislang aber noch.
   Wenn Sie lieber die aktuellen (und leistungsfähigeren) Schnittstellen verwenden
   möchten, konsultieren Sie bitte die umfangreiche Dokumentation_ auf den Entwicklerseiten von Google.

Mit Google Static Maps können Kartenausschnitte als Bilder auf Ihrer Seite eingebunden
werden. Wir erklären Ihnen hier kurz, wie das geht.

Google Static Maps bietet - ähnlich wie Google Maps - die Möglichkeit, Kartenausschnitte
in beliebiger Größe und Zoom-Stufe einzubinden. Der Kartenausschnitt kann per URL
definiert werden.

Der Unterschied zu unserem Plone-Plugin ist, dass bei den Static Maps ein simples Bild
eingebunden wird (zum Betrachten wird kein Javascript etc. benötigt).

Am Beispiel des HRZ zeigen wir Ihnen hier kurz die Vorgehensweise.

.. _fig_statik_map:

.. figure:: ./images/statik-map.*
   :width: 70%
   :alt: Google Statik Map

   Google Statik Map


Der entsprechende HTML-Code hierzu lautet wie folgt: ::

 <img src="https://maps.google.com/maps/api/staticmap?center=50.728780,7.088370
 &amp;zoom=13&amp;markers=color:blue|50.728780,7.088370&amp;size=500x300
 &amp;sensor=false" alt="alternativer Text" />

Hier die Parameter im Einzelnen:

:guilabel:`center:`
    Die Koodinaten des Rechenzentrums (ermittelt über Google Maps).

:guilabel:`zoom:`
    Wert zwischen 0 und 19, wobei 0 den ganzen Globus und 19 einzelne Häuser zeigt.

:guilabel:`markers (optional):`
    Gibt Farbe und Position der Markierung an.

:guilabel:`size:`
    Größe des dargestellten Bildes in Pixeln.

:guilabel:`sensor:`
    Kann `true` oder `false` sein. Bezieht sich auf den Standort des betrachtenden Benutzers. Wir empfehlen »false«.


.. _Dokumentation: https://developers.google.com/maps/web/?hl=de
