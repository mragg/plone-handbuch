.. index:: Google-Maps
.. _sec_googlemaps:

===========
Google Maps
===========

Auf allen Instanzen steht das Product »Maps« zur Verfügung, mit dem Sie Google-Maps
integrieren können.

.. _fig_google_map_anzeigen:

.. figure:: ./images/google-map-anzeigen.*
   :width: 80%
   :alt: Goole Map anzeigen
   
   Goole Map anzeigen

Folgende Schritte sind notwendig: Der Administrator muss Maps unter »Konfiguration«
aktivieren. Dort sind die Keys für `Google Maps`_ und `Google Ajax Search`_ zu
hinterlegen. Falls Ihre Instanz über mehrere URLs erreichbar ist, müssen Sie mehrere Keys
eintragen! Wenn Sie URLs .uni-bonn.de haben, reicht es aus, wenn Sie Keys für
http://uni-bonn.de beantragen (ohne Subdomain, ohne www), diese Keys sind dann
automatisch für die Subdomains mit gültig!

.. _hier: ../administration/zusatzprodukte.html
.. _`Google Maps`: http://code.google.com/intl/de-DE/apis/maps/signup.html
.. _`Google Ajax Search`: http://code.google.com/intl/de-DE/apis/loader/signup.html

Bitte beachten Sie auch die Google-Lizenzbedingungen, alle dezentralen Instanzen sind
selbst für die Konformität Ihrer Seiten und zum Einhalten der Lizenzbedingungen
verantwortlich.

Sie können dann einen Ordner unter »Darstellung« auf eine Google-Maps-Ansicht umschalten. 

Unter »hinzufügen« gibt es »Pfad«, damit können Sie einen Marker hinzufügen - geben Sie
im Suchen-Dialog einfach die Adresse an und klicken auf »suchen«. Sie können so mehrere
Marker in einem Ordner anlegen, die Ordneransicht sammelt dann automatisch alle Marker
ein und stellt diese gemeinsam auf einer Karte dar.

.. _fig_google_map_hinzufuegen:

.. figure:: ./images/google-map-hinzufuegen.*
   :width: 80%
   :alt: Goole Map einbinden
   
   Goole Map einbinden

Plone unterscheidet dann noch bei der »Darstellung« zwischen »location view« (kleinere
Karte) und »map view« (Karte über volle Breite der Seite).