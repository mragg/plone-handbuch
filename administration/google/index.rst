.. index:: Google, Google-Dienste
.. _sec_google:

=========================
Google - Dienste in Plone
=========================

Im Internet geht heutzutage wenig ohne Google. Auch in Plone können zahlreiche
Google-Produkte bzw. Google-Dienste integriert werden, die die Arbeit und den Besuch
unserer Webseiten attraktiver gestalten können. Wir geben eine Übersicht.

.. toctree::
   :maxdepth: 1

   rund-google.rst
   google-statik-map.rst
   googlemaps.rst
   seitenmap.rst
   robots-txt.rst
