.. index:: Zugriff für Suchmaschinen verbieten, robots.txt
.. _sec_robots_txt:

==============================================
robots.txt - Zugriff für Suchmaschinen steuern
==============================================

Manchmal benötigen Sie die Möglichkeit, den Zugriff für Suchmaschinen auf bestimmte
Bereiche Ihrer Seite zu verbieten. Dafür existiert die Möglichkeit, eine so genannte
robots.txt-Datei zu erstellen, die von den namhaften Anbietern bei der Indexierung Ihrer
Seite berücksichtigt wird. Wir beschreiben hier, wie Sie diese Datei anpassen können.

Diese Einstellungen kann nur noch vom Plone-Support vorgenommen werden, bitte wenden Sie sich an plone@uni-bonn.de und teilen Sie uns die gewünschten Einstellungen mit, die wir für Sie vornehmen sollen.

Begeben Sie sich zunächst in das ZMI Ihrer Website (»Konfiguration« → »Zope-Management-
Oberfläche«). Klicken Sie Sie dort auf den Punkt ``portal_properties``:

.. _fig_portal_properties:

.. figure:: ./images/portal-properties.*
   :width: 90%
   :alt: portal_properties

   `portal_properties` unter Konfiguration der Webseite

Auf der Seite ``portal_properties`` klicken Sie bitte nun auf den Link ``site_properties``:

.. _fig_site_properties:

.. figure:: ./images/robots-02.*
   :width: 90%
   :alt: site_properties

   `site_properties unter` Konfiguration der Webseite

Es öffnet sich ein relativ langes Eingabeformular mit sehr vielen Einstellmöglichkeiten
für Ihr Plone-Portal. Scrollen Sie nach unten, bis Sie das Feld »robots_txt« sehen.
Bitte verändern Sie hier keinesfalls auf Verdacht andere Werte, da Sie damit Ihre
Installation unbrauchbar machen könnten!

.. _fig_robots_txt_feld:

.. figure:: ./images/robots-txt-feld.*
   :width: 100%
   :alt: Den Zugriff der Suchmaschinen auf den Seiten verwalten

   Den Zugriff der Suchmaschinen auf den Seiten verwalten

Tragen Sie im Textfeld alle Pfade (ohne vorangestelltes http://www.ABC.uni-bonn.de) ein,
die Sie von Suchmaschinen ausschließen lassen wollen. Für jeden Pfad sollten Sie eine
neue ``Disallow`` - Zeile beginnen: ::

  Disallow: /Pfad/zu/Ordner1
  Disallow: /Pfad/zu/anderem/Ordner

Speichern Sie abschließend Ihre Änderungen mittels Save Changes. Das Ergebnis können Sie
unter ``www.IhreURL.uni-bonn.de/robots.txt`` überprüfen. Die Einträge, die sich ggf. unter
den Abschnitten »# generated robots.txt« und »# dynamic added subportals« befinden,
werden automatisch generiert und können nicht von Ihnen verändert werden.

Spezialfall: robots.txt und Navigation Root
===========================================

Bitte beachten Sie folgenden Automatismus: Wenn Sie für einen Ordner eine _`navigation_root`
definieren, gehen wir davon aus, dass Sie eine eigene URL auf diesen Ordner zeigen
lassen. Daher wird standardmäßig für alle Navigation Roots automatisch ein Eintrag in
die robots.txt vorgenommen (s. oben, unterhalb des Abschnittes »# dynamic added portals«).
So verhindern wir eine doppelte Indexierung von Seiten und damit eine Abstrafung auf
Grund von "Duplicate Content" bei Suchmaschinen.

Wenn Sie einmal eine Navigation Root definiert haben, ohne eine eigene URL auf diesen
Ordner zeigen zu lassen, denken Sie bitte daran, in der robots.txt dann einen
Allow-Eintrag zu erstellen! Nur dann wird dieser Ordner von Suchmaschinen erfasst. Der
Allow-Eintrag überschreibt den automatisch erstellten Eintrag.
