.. _sec_google_webmaster:

========================
Google Webmaster - Tools
========================

Eine Anmeldung bei den Google-Webmaster-Tools ist auf jeden Fall empfehlenswert. Unter
der Oberfläche der Webmaster-Tools erhalten Sie gute Auswertungen des Google-Robots,
gleichzeitig erhalten Sie auch jede Daten über Ihr Ranking, zu welchen Keywords Ihre URL
gelistet wird und können einige Einstellungen vornehmen. Voraussetzung für die Nutzung
ist ein Google-Konto, Sie erreichen die Google Webmaster-Tools unter:
https://www.google.com/webmasters/tools/home?hl=de

Diagnose/Sinnvolle Checks
-------------------------

Überprüfen Sie in den Webmaster-Tools auf jeden Fall die Einträge, die Google dort unter
»Diagnose« anbietet. Vor allem die Einträge unter »Crawling-Fehler« geben häufig
Anhaltspunkte auf Probleme bzw. falsche Einstellungen.


Sitemaps einreichen
-------------------

Der klassische Weg wie neue Webseiten in den Google-Such-Index kommen ist der Weg über
die Google-Bots. Diese besuchen in regelmäßigen Abständen ihre Webseiten und schauen nach
Neueinträgen bzw. Veränderungen. Mit Sitemaps unterstützt Google einen offenen Standard,
der das Auffinden neuer Seiten vereinfacht. In der Sitemaps-Datei sind alle nach außen
verfügbaren Webseiten aufgeführt und mit einem Zeitstempel versehen. Plone unterstützt
das Bereitstellen einer Sitemaps-Datei. Klicken Sie dazu unter »Konfiguration« → »Website«
den Haken bei »Sitemaps erstellen«. Danach müssen Sie in Google-Sitemaps unter
»Website-Konfiguration« → »XML-Sitemaps« die Sitemaps-URL eintragen, die Standardadresse
ist hierbei: [ihre_url]/sitemap.xml.gz


Seiten aus dem Suchindex entfernen
----------------------------------

Etwas versteckt im Menü bietet Google Webmastern die Möglichkeit einzelne
Seiten oder Verzeichnisse aus dem Google-Suchindex zu entfernen. Wenn Sie
aus Versehen etwas publiziert haben, das im schlimmsten Fall im Google-Cache
gelandet ist, aber nicht für die Öffentlichkeit gedacht war, können Sie
hier den Prozess des Austragens und Löschens beschleunigen:
»Website-Konfiguration« → »Crawler-Zugriff« → »URL entfernen«

Dort bekommen Sie auch eine Statusmeldung über den Stand der Entfernung.


.. index:: Google Analytics, PIWIK

Google Analytics
================
Die Nutzung von Google Analytics ist an der Universität Bonn derzeit nicht gestattet.
 Nutzen Sie für Webstatistiken das vom HRZ betriebene PIWIK, s. :ref:`sec_piwik`

.. _Datenschützer: https://www.datenschutzzentrum.de/presse/20080807-google-analytics.htm
