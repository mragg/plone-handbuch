.. index:: Sitemap
.. _sec_seitenmap:

==================
Sitemap einrichten
==================

Mit einer XML-Sitemap können Sie Suchmaschinen über zu indizierende Seiten gezielt
informieren.


Was ist Sitemap?
================

Eine Sitemap ist eine Übersicht aller verfügbaren (freigeschalteten) Seiten in Ihrem
Webauftritt. Die Sitemap auf die dieser Artikel Bezug nimmt ist nicht die »Klassische
HTML-Sitemap«, die es auf vielen Webauftritten gibt und die schlicht eine Übersicht der
verfügbaren Navigationsstruktur und Seiten gibt. Vielmehr ist die hier gemeinte Sitemap
eine XML-Datei, die URLs Ihrer Webseite in einem speziellen Format aufbereiten und
verfügbar machen.


Wozu kann man das benutzen?
===========================

Eine XML-Sitemap ist ein alternativer Weg, wie eine Suchmaschine Ihren Webauftritt
erreichen kann. Klassischerweise basieren Suchmaschinen auf kleinen »Robots«, d.h.
Programmen, die selbstständig das Internet durchforsten und katalogisieren. Diese hangeln
sich jeweils an Verlinkungen entlang. Dies birgt einige Probleme mit sich, so wird z.B.
eine nicht verlinkte Seite von Suchmaschinen meist nicht erfasst. Gleichzeitig besucht
der Roboter ihre Webseiten sehr häufig, um z.B. Änderungen mitzubekommen.

Über eine XML-Sitemap hat z.B. Google nun einen alternativen Zugang. Sie können Google
als Webmaster mitteilen, dass Ihr Webauftritt über eine XML-Sitemap verfügt,
normalerweise lädt Google dann einmal am Tag diese Sitemap herunter und nutzt die dort
enthaltenen URLs um seine Robots gezielt loszuschicken. Eine XML-Sitemap enthält z.B.
auch häufig eine Kennzeichnung der letzten Bearbeitung, so dass ein Suchmaschinenbetreiber
auch die Möglichkeit hat hierüber gezielt z.B. aktualisierte Seiten anzusteuern.

(Es gibt keine Garantie, dass eine Suchmaschine die Sitemap nutzt - auch heißt es z.B.
nicht, dass Google damit alle in der Sitemap enthaltenen URLs auch wirklich indiziert.
Aber man macht es der Suchmaschine einfacher und erhöht einfach die Wahrscheinlichkeit,
jeweils komplett und mit den aktuellen Seiten indiziert zu werden.


Wann auf Sitemap verzichten?
============================

Sie sollten auf die Erstellung der Sitemap verzichten, wenn Sie mehrere Bereiche mit
Navigation-Root mit eigenen URLs innerhalb eines Portals erfasst haben, da für Google
dann nicht mehr klar ist, was die »führende« URL ist.

Hintergrund ist, dass Sie immer darauf achten sollten eine Seite nur unter einer URL nach
außen zu kommunizieren. Ein Navigation-Root verhindert normalerweise, dass z.B. ein
Instituts-Ordner mit eigener URL unter der Fakultäts-URL abgerufen wird. Erstellen Sie
nun eine Sitemap auf Portal-Ebene (mit der Fakultäts-URL) sind dort alle Links zu den
Instituts-Seiten mit der Fakultäts-URL erfasst. Gleichzeitig indiziert der normale
Google-Robot Ihre Instituts-Seite mit der Instituts-URL und es kommt zu klassischem
»Duplicate Content«. Das mag Google erstens nicht (und es führt u.U. auch zu einer
Abwertung im PageRang der Seite), gleichzeitig möchte Google die »Original-URL« bestimmen
und hier gewinnt momentan wohl immer der Sitemap-Eintrag.

Daher: **Wenn Sie Navigation-Roots mit eigenen URLs in Ihrem Portal haben, verzichten Sie auf Sitemaps!**


Sitemap aktivieren
==================

Schritt 1: Sitemap in Plone freischalten
----------------------------------------

Der Schritt in Plone ist einfach - Voraussetzung ist, dass Sie Administrator für Ihre
Plone-Site sind, Ihr Plone sollte nicht über Navigation-Roots mit zusätzlichen URLs
verfügen.

Klickweg

* Konfiguration → Website → Haken setzen bei »sitemap.xml.gz verfügbar machen«

.. _fig_seitenmap_freischalten:

.. figure:: ./images/seitenmap.*
   :width: 75%
   :alt: Seitenmap freischalten
   
   Seitenmap freischalten


Schritt 2: Google informieren
-----------------------------

Schritt 2 ist einmalig etwas aufwändiger, Sie müssen Google informieren, dass Ihre
Webseite über eine XML-Sitemap verfügt. Wir fassen die Schritte zusammen:
 
1. Sie benötigen ein Google-Konto
2. Melden Sie sich in den Webmaster Tools an unter:
 http://www.google.de/webmaster.
 

   .. _fig_seitenmap_webmasterzentr:

   .. figure:: ./images/webmasterzentr.*
      :width: 70%
      :alt: Google Webmaster-Zentrale
   
      Google Webmaster-Zentrale

3. Jetzt müssen Sie Google informieren, dass Sie der zuständige Admin für Ihre URL sind,
   hierzu müssen Sie Ihre Website hinzufügen. Google überprüft dann die
   »Eigentümerschaft«, d.h. dass Sie nachweisen, dass Sie für die URL zuständig sind.
    
   Wir empfehlen den Weg über die HTML-Datei. Google stellt Ihnen eine HTML-Datei zur
   Verfügung, diese müssen Sie in Ihr Plone in die oberste Ebene hochladen. Auf oberster
   Ebene in Plone auf »Hinzufügen« → »Datei« (Wichtig ist hier, dass Sie als Namen den
   Dateinamen der Datei angeben, die Google Ihnen gegeben hat (incl. der Endung .html !)
 
   Klicken Sie in den Webmaster-Tools dann auf »Bestätigen« - Google überprüft live, ob
   die Datei verfügbar ist und schaltet die Webmaster-Funktionen bei erfolgter Prüfung
   frei.
 
4. Klicken Sie dann im Webmaster-Bereich auf Ihre URL, Dann auf: »Website-Konfiguration«
   → »XML-Sitemaps« → »XML-Sitemap einreichen«
 
   Die URL zu Ihrer Sitemap ist: ihreurl/sitemap.xml.gz

Sie erhalten in den Webmaster-Tools jeweils auch Rückmeldungen ob der Abruf der
Sitemap erfolgreich war und ob es bei der Vearbeitung zu Problemen gekommen ist.

Weitere Informationen zum Sitemap-Protokoll unter http://www.sitemap.org
 
