.. index:: Editor, External Editor
.. _sec_external_editor:

=========================
External Editor mit Plone
=========================

Sie können Daten in Ihrem Plone nicht nur mit den Editoren Kupu bzw. Fck-Editor
bearbeiten sondern auch einen externen Editor verwenden, um z.B. Seiten direkt im
Quelltext mit einem lokal installierten Programm zu bearbeiten.

**Disclaimer:**
:guilabel:`Wir empfehlen die integrierten Standardeditoren zu benutzen, mit einem
externen Editor zu arbeiten sollten nur erfahrene Benutzer. Das Verfahren wird vom
Plone-Support an dieser Stelle nur dokumentiert, wir können leider keinen Support hierfür
anbieten und können keine Unterstützung für die Installation auf Ihrem lokalen Rechner in
Ihrer Umgebung anbieten.`

**Sie benötigen folgende Schritte:**

* der Administrator der Plone-Instanz muss unter »Konfiguration« → »Website« die
  Einstellung »externer Editor« aktivieren
* Sie müssen als Redakteur zusätzlich unter »Meine Einstellungen« die Option »externer
  Editor« aktivieren
* Installieren Sie sich auf Ihrem lokalen System (derzeit haben wir nur Windows
  erfolgreich getestet) das Programm ZopeEdit. `Download ZopeEdit`_
* Öffnen Sie nach der Installation die Datei ZopeEdit.ini und ergänzen Sie bei den
  Dateitypen, die Sie lokal bearbeiten möchten den gewünschten Editor. Fügen Sie jeweils
  eine Zeile hinzu in der Form: ::
  
   editor=Pfad zur Editor-exe

  .. _fig_external_editor:

  .. figure::
     ./images/external-editor.*
     :width: 60%
     :alt: external-editor
   
     External Editor (Bild-Quelle: `Web Support HU Berlin`_)

   
* Sie können nun den ersten Versuch starten: Rufen Sie Ihre Plone-Seite über die www3-URL
  auf - nur diese URL funktioniert für den Webdav/External Editor-Zugriff. Sie finden im
  Portlet »Dokumentenaktionen« als Redakteur ein zusätzliches Symbol Edit-Icon Klicken
  Sie auf dieses wird die Plone-Webseite lokal in dem von Ihnen angegebenen Programm
  geöffnet. Ein Abspeichern speichert die Datei wiederum in Plone. Das erfolgreiche
  Speichern wird in Plone in einem kleinen JS-Fenster bestätigt. Sie müssen in Plone die
  Seite dann jeweils neu laden, um die Änderungen zu sehen.

**Tipp:** Sie können Ihr Portal auch über ``www3.uni-bonn.de/portalname`` aufrufen - die
Anbindung des externen Editors funktioniert wenn nur über diese URL (und nicht über die
ansonsten aufgerufene securewww-Adresse).

Wenn man mit einem externen Editor arbeitet und dann doch mal dieselbe Datei mit
FCKeditor editiert, wird anschließend die ganze HTML als ein einzeiliger String an den
externen Editor übergeben, so dass man nicht mehr vernünftig mit dem externen Editor in
der Quelle editieren kann. Der Kupu dagegen übergibt offensichtlich die ganze Quelle
mitsamt den Umbrüchen richtig. Daher empfiehlt sich die Einstellung Kupu, wenn man mit
einem externen Editor arbeiten will.


.. _Download ZopeEdit: http://plone.org/products/zope-externaleditor-client/releases/1.0.0/zopeedit-win32-1.0.0.exe
.. _Web Support HU Berlin: http://web-support.hu-berlin.de/hu-plone/benutzer/editor/external-editor