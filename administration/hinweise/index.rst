.. _sec_hinweise_fuer_administration:

============================
Hinweise für Administratoren
============================


.. toctree::
   :maxdepth: 1

   kopfleiste.rst
   profil.rst
   external-editor.rst
   jquery.rst
   rewritet.rst
   mailingliste.rst
