﻿.. index:: jQuery
.. _sec_jquery:

======================
jQuery in Plone nutzen
======================

Die Javascript-Bibliothek jQuery gehört inzwischen zu einer der besten
Javascript-Frameworks, damit sind einfache umfangreiche DOM-Manipulationen möglich.
Jquery vereinfacht aber auch ganz simple Javascript-Aktionen. 

jQuery ist in unserem Plone als Bibliothek verfügbar, zur Nutzung gibt es einige Dinge
zu beachten.

Generell muss auf Ihrer Instanz die Nutzung mind. des Javascript-Tags freigeschaltet
sein, im Standard ist dies deaktiviert d.h. Javascript-Anweisungen werden gefiltert.
Diese Einstellung kann nur vom Administrator Ihrer Instanz geändert werden
(»Konfiguration« → »HTML-Filter«). Gerade bei großen Instanzen mit vielen Redakteuren
ist eine Freigabe von Javascript immer zu überdenken.

Der Aufruf von Jquery erfolgt in Plone durch ein vorangestelltes jq

Nachfolgend ein Beispiel für eine "Direkt-Linkbox", die mit jQuery für die Webseiten der
ULB realisiert wurden. Sie finden es "live" nebenstehend in der rechten Portlet-Spalte,
das Auswählen aus dem Dropdown führt Sie direkt auf die verlinkte Webseite, mit
"Standard-HTML-Mitteln" ist dies nicht möglich, hier brauchen Sie in irgendeiner Form
z.B. Javascript. Nachfolgend der verwendete Code (hier ist es auch notwendig im Filter
neben Javascript auch die Elemente form, option und select aus dem Filter herauzunehmen).

Weitere Informationen zu jQuery:

* jQuery.com_
* `Deutsche Tutorials`_ ::

    <p><script type="text/javascript">
      jq(document).ready(function() {
        jq("#redirSelect").change(function() {
          window.location = jq(this).val();
        });;
      });
    </script></p>
    <form id="redirForm">
       <select id="redirSelect" name="redir">
       <option>Bitte Webseite wählen</option>
       <option value="http://www.heise.de">Heise.de</option>
       <option value="http://www.golem.de">Golem.de</option>
       <option value="http://www.zeit.de">ZEIT.de</option>
       </select>
    </form>

.. _jQuery.com: http://jquery.com/
.. _`Deutsche Tutorials`: http://docs.jquery.com/Tutorials#Tutorials_auf_Deutsch