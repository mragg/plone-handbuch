﻿.. index:: Kopfleiste
.. _sec_kopfleiste_link:

=======================================
Kopfleiste und Standardlinks bearbeiten
=======================================

Diese Einstellungen kann nur noch vom Plone-Support vorgenommen werden, bitte wenden Sie sich an plone@uni-bonn.de und teilen Sie uns die Einstellungen für Ihr Portal mit, die wir vornehmen sollen.

In der oberen Kopfleiste finden Sie im Standard die Links »Übersicht«, »Kontakt« und
»Barrierefreiheit«. Sie können sowohl die Bezeichnungen dieser Links ändern als auch
neue Verweise aufnehmen bzw. vorhandene löschen. Sie müssen als Administrator angemeldet
sein

* »Konfiguration« → »Zope-Management-Oberfläche« → »portal-actions« → »site_actions«

Hier finden Sie die voreingestellten Einträge (jeweils mit der englischen Bezeichnung).

.. _fig_link_kopfleiste:

.. figure:: ./images/kopflink-1.*
   :width: 70%
   :alt: Links der Kopfleiste

   Links der Kopfleiste

Reihenfolge ändern
==================

Ganz rechts außen sehen Sie das Positionsfeld, das angibt in welcher Reihenfolge die
Elemente ausgegeben werden. Sie können die Reihenfolge ändern, markieren Sie dazu vorne
den Punkt, den Sie verändern möchten und verändern Sie durch Klicken auf »Up« bzw. »Down«
die Reihenfolge.

Eintrag bearbeiten
==================

Zum Bearbeiten klicken Sie einfach einen Eintrag an, als Beispiel nehmen wir »Contact«.
Wichtig ist die Zeile »URL Expression«, diese enthält den Verweis auf die Datei in Plone,
die angezeigt wird. ``string:${portal_url}/kontakt`` heißt demnach, dass nach
dem Klick auf »Kontakt« die Datei kontakt aus dem Root des Portals geladen wird.

Neuer Eintrag
=============

Der einfachste Weg ist, wenn Sie sich einen existierenden Eintrag als Vorlage nehmen.
Nehmen Sie beispielsweise den Eintrag »Contact«, markieren diesen in der Übersicht und
klicken unter erst auf »Copy« und anschließend auf »Paste«. Sie haben jetzt eine Kopie
von Contact erzeugt. Im nächsten Schritt die Kopie markieren, auf »Rename» klicken und
einen neuen Namen vergeben, z.B. »Impressum«. Klicken Sie dann auf den Eintrag
»Impressum«. Unter Title tragen Sie nun auch »Impressum« ein und in der URL-Expression
tauschen Sie am Schluss des Strings »contact« durch »impressum« aus, so dass dort nun
``string:${portal_url}/impressum`` steht.

**Tipp:** Wenn mehrere Webauftritte in Ihrer Instanz mit der Funktion
:ref:`sec_navigation_root` enthalten sind, die jeweils ein eigenes Impressum erhalten
sollen, verwenden Sie ``string:${globals_view/navigationRootUrl}/impressum`` - das
bewirkt, dass jeweils die Datei impressum aus dem Navigation-Root-Verzeichnis geladen
wird.

Sprachabhängige Einträge
========================

Sie können Einträge in der Kopfleiste auch abhängig von der jeweiligen Sprache setzen
(um z.B. ein deutsches und ein englisches Kontaktformular zu haben). Sie müssen in diesem
Fall für jede Sprachenversion einen eigenen Eintrag anlegen und diesem eine Bedingung
mitgeben, tragen Sie dazu jeweils unter Condition ein: ::

  python: request.get('LANGUAGE','')=='de'

(für Englisch anstelle von 'de' 'en' verwenden, französisch ist analog 'fr')

.. index:: Login-Button

Anmelde-Link
============

Sie können auch einen Link zur Anmeldung dort erstellen. Legen Sie sich dazu bitte einen
Eintrag an (oder kopieren einen existierenden). In das Feld URL tragen Sie dann bitte
folgenden Code-Schnippsel ein. Dieser bewirkt, dass an die aktuell aufgerufene URL ein
/login angehängt wird. ::

 string:${object_url}/login
