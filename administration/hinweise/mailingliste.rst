.. index:: Mailingliste
.. _sec_mailingliste:

============
Mailingliste
============

.. index:: Majordomo

Majordomo – Mailingliste
========================

Das Hochschulrechenzentrum betreibt einen zentralen Listserver, auf dem Mailinglisten
eingerichtet werden können. Die An- und Abmeldung zu Mailinglisten kann mittels eines
Plone-Produkts komfortabel in den eigenen Webauftritt integriert werden.

Wie können sich Besucher in eine Mailingliste eintragen?

Um einem Besucher Ihrer Internetseite zu ermöglichen, sich in eine Mailingliste
einzutragen, gehen Sie folgendermaßen vor: Erstellen Sie das Portlet »Management Actions«.
Wechseln Sie in einen Ordner, in dem das soeben erstellte Portlet »Management Links« zu
sehen ist. Klicken Sie auf »Add mailinglist form« und geben Sie die erforderlichen
Daten Ihrer vorhandenen Mailinglist ein.

Sie können das Formular auch direkt über einen URL-Aufruf anlegen, indem Sie
``@@majordomo-form`` an die URL des Ordners anhängen in dem das Listenformular angelegt
werden soll.

.. _sec_mailingliste_opt_in:
