﻿.. index:: Profil
.. _sec_profil:

===================
Persönliches Profil
===================

Ihr Profil gibt anderen Benutzern der Website einen Überblick über
Ihre Person und Ihre Tätigkeit (siehe Abbildung :ref:`fig_profil`).

.. _fig_profil:

.. figure:: ./images/profil.*
   :width: 100%
   :alt: Die Darstellung des Profils mit Informationen über den Benutzer

   Beispiel eines Profils

Verweise auf Ihr Profil finden sich in Ihren Artikeln und einigen automatisch erzeugten
Übersichtslisten. Sie selbst können Ihr Profil außerdem über einen Verweis auf Ihrer
persönlichen Seite erreichen.

Das Profil enthält folgende Informationen:

* Name
* Standort und Muttersprache
* Porträt und Biographie
* einen Verweis zu Ihrem persönlichen Ordner
* ein Rückmeldeformular
* Verweise zu Ihren aktuellen Artikeln


Ihre Profilangaben können Sie in Ihren Einstellungen aktuell halten.

Mit dem Rückmeldeformular können Besucher mit Ihnen Kontakt aufnehmen. Das
Formular verschickt Nachrichten an die E-Mail-Adresse, die Sie in Ihren
Einstellungen angegeben haben, sodass anonyme Besucher die Adresse nicht zu
sehen bekommen. Nachrichten bestehen aus Betreff und Text. Wenn Sie selbst
Ihre Profilseite betrachten, wird das Rückmeldeformular ausgeblendet.

Die Liste Ihrer aktuellen Artikel ist nach Artikeltypen sortiert und
enthält Titel und Änderungsdatum jedes aufgeführten Artikels. Darunter
finden Sie einen Verweis zu einer Liste aller von Ihnen verfassten
Artikel, beginnend mit dem neuesten.

