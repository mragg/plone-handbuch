.. index:: Rewrite, Redirect
.. _sec_rewrite_redirekt:

=============================================
Rewrite: Was ist das und wie funktioniert es?
=============================================

Immer wieder sehen wir überraschte Reaktionen, wenn wir darauf hinweisen, dass es für
eine Plone-URL mehrere Webadressen geben kann. Häufig ist nicht bekannt, dass wir über
einen »Rewrite«-Mechanismus verfügen. Was das ist und wie es funktioniert, soll hier
beschrieben werden.

Unser CMS besteht aus einem mehrstufigen Verbund zahlreicher Server, einem so genannten
:guilabel:`Cluster`. An "vorderster Front" stehen die :guilabel:`Cache-Server`, die die
Anfragen entgegennehmen und anhand der vom Browser empfangenen Webadresse wissen, welches
Portal ein Besucher aufrufen möchte. Dorthin leiten sie die Anfragen dann weiter. 

In diesen Vorgang lässt sich über Konfigurationsdateien eingreifen. So ist es
beispielsweise möglich, in einem beliebigen Unterverzeichnis eine vollkommen neue
Navigation aufzubauen (eine :ref:`sec_navigation_root` zu setzen) und diesen Ordner über
eine eigene Webadresse zugänglich zu machen. Ein typischer Fall wäre eine Fakultät, die
den Instituten mehr Freiheit geben will. Normalerweise ist ja über die Plone-Struktur
z.B. die Form ``www.uni-bonn.de/fakultät/institut`` vorgegeben. Hier schafft ein
»Rewrite« oder auch ein »Redirect« Abhilfe und es wird möglich, die URL auch über die
Adresse ``institutsname.uni-bonn.de`` oder eine beliebige andere Adresse zugänglich zu
machen.

.. _sec_rewrite:

Rewrite
-------

Ein »Rewrite« bezeichnet den Vorgang, eine eingegebene Webadresse auf eine Zieladresse
umzuleiten, ohne dass dies im Browser sichtbar wird. Als Beispiel hierzu kann die
Webadresse ``www.fs-agrar.uni-bonn.de`` dienen: Der Besucher tippt diese Adresse in
seinen Browser ein, wird aber per »Rewrite« auf die Adresse
``https://www3.uni-bonn.de/landwirtschaft/studium-lehre/fachschaften/fachschaft-agrar``
umgeleitet, ohne dass dies für ihn erkennbar wäre. 

Der Nachteil eines »Rewrite« ist jedoch, dass der Besucher nun auf der Seite
»eingesperrt« ist. Denn liegt die Seite eigentlich unterhalb einer anderen Webpräsenz,
stimmen die vorhandenen Links heraus aus der aktuellen Seite nicht mehr. Sie
funktionieren zwar, hierarchisch sind sie aber falsch zusammengesetzt. **Daher kann ein
»Rewrite« nur bei Ordnern benutzt werden, in denen eine Navigation Root
gesetzt wurde oder es sich um den obersten Ordner eines Portals handelt.**

.. _sec_redirect:

Redirect
--------

Eine andere Möglichkeit zur Umleitung auf eine andere Adresse ist »Redirect«. Der Effekt
ist derselbe, allerdings sieht der Besucher hierbei, wohin er umgeleitet worden ist.
Dieser Fall ist eigentlich die "sauberere Methode", obendrein auch noch deutlich
flexibler, weil sie in allen Fällen ohne Verlust gültiger Navigationsstrukturen
einsetzbar ist. Zu jeder Zeit weiß der Besucher anhand der Adresszeile im Browser, wo er
sich in der Ordnerhierarchie auf welcher Instanz/Fakultät befindet. Daher ist einem
»Redirect« im Zweifel der Vorzug zu geben.

Grenzen des Verfahrens
======================
URL-Manipulation ist kein triviales Unterfangen. Anhand von :guilabel:`regulären
Ausdrücken` muss nämlich jede eingehende Anfrage auf den :guilabel:`Servern` untersucht
und passend ausgewertet werden. Diese Tätigkeit ist rechenintensiv und verlangsamt mit
einer steigenden Anzahl an Regeln den Zugriff auf das CMS spürbar. So leid es uns tut,
können wir nicht für jeden Ordner in jeder Instanz eine eigene Webadresse erstellen.
Bitte haben Sie dafür Verständnis, dass wir deshalb nicht jeder Anfrage nach »Rewrites«/
»Redirects« entsprechen können.


