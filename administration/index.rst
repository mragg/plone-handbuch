.. index:: Administration
.. _sec_administration:

==================================
Administration eines Plone-Portals
==================================

.. _sec_admin:

Dieser *Anhang* soll Ihnen Hinweise geben, welche Einstellungen an einer frisch
aufgesetzten Plone-Website vorzunehmen sind, um alle im Buch beschriebenen
Funktionen verfügbar zu machen. Er ist jedoch keineswegs
als umfassende Anleitung für die Administration einer Website gedacht.

.. toctree::
   :maxdepth: 1

   voreinstellungen.rst
   konfiguration/index.rst
   benutzer/index.rst
   google/index.rst
   piwik/index.rst
   hinweise/index.rst
