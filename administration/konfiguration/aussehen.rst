.. _sec_konfiguration_aussehen:

========
Aussehen
========

Im Bereich :guilabel:`Aussehen` der Website-Konfiguration können Sie Einstellungen
vornehmen, die das Design der Website verändern.

.. _fig_konfiguration_aussehen:

.. figure::
   ./images/konfiguration-aussehen.*
   :width: 100%
   :alt: Konfiguration des Aussehens von Plone

   Konfiguration des Aussehens von Plone

Standardaussehen
   Im Standard ist hier das Uni Bonn Theme eingestellt. Ändern Sie diese Einstellung nicht ohne weitere Absprachen.

Externe Links markieren
   Wenn Sie diese Option auswählen, werden Links, die auf andere Websites führen, mit
   einem Globus-Icon markiert. Diese Option wird, anders als im Hilfetext vermerkt, nicht
   benötigt, um die folgende Option einsetzen zu können.

Nach extern verweisende Links werden in einem neuen Fenster geöffnet
   Wenn Sie diese Option auswählen, werden Links, die auf andere Websites führen in einem
   eigenen Fenster im Browser geöffnet.

Zeige Artikeltypenspezifische Icons
   In Übersichten wie der Navigation oder bestimmten Ordneransichten erscheint vor dem
   Titel eines Artikels ein Icon, an dem man erkennen kann, um welchen Artikeltyp es sich
   handelt. In der Voreinstellung werden diese Icons immer gezeigt. Sie können dies hier
   ändern, indem Sie die Icons nur angemeldeten Benutzern zeigen oder ganz ausblenden.
