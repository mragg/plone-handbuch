.. _sec_konfiguration_benutzer_gruppen:
.. index: Benutzer, Gruppen, Rechte verwalten

====================
Benutzer und Gruppen
====================

Benutzer und Gruppen sind ein mächtiges Werkzeug, um effektiv Rechte in Ihrem Plone zu verwalten.

Benutzer
--------

In der Benutzerübersicht können Sie nach einzelnen Benutzern suchen. Bitte beachten Sie, dass Sie an dieser Stelle nur nach Vornamen und Nachnamen suchen können (nicht nach der Uni-ID).
Sie können theoretisch auch an dieser Stelle einen Benutzer suchen und diesem Rechte zuweisen (z.B. Bearbeiten, Verwalten etc.). Sie sollten dies im Regelfall nicht tun. Die hier zugewiesenen Rechte wirken "global" für das ganze Plone. D.h. ein Benutzer, der hier "Bearbeiten" zugewiesen bekommt, hat das Recht alle Seiten in dem Plone zu bearbeiten. Für die spätere Verwaltung ist die Zuweisung von Rechten an dieser Stelle problematisch, da Sie keine Übersicht haben, wer welche Rechte zugewiesen bekommen hat.
Wir empfehlen ausdrücklich Benutzer in Gruppen zusammenzufassen und die Rechte einer Gruppe zuzuweisen.

Weitere Informationen zu Gruppen erhalten Sie im Kapitel :ref:`sec_benutzer`

Gruppen
-------
Am sinnvollsten gruppieren Sie Benutzer in Gruppen und weisen einer Gruppe Rechte zu.
Im Tab "Gruppen" erhalten Sie eine Übersicht aller angelegten Gruppen und können die Rechte verwalten.
Rechte, die Sie einer Gruppe zuweisen wirken sich auch global in Ihrem ganzen Plone aus. Bitte überlegen Sie genau ob Sie einer Gruppe globale Rechte einräumen möchten.
Der übliche Weg ist, dass Sie einer Gruppe im Regelfall keine globalen Rechte einräumen, sondern an dieser Stelle nur die Gruppe und ihre Mitglieder pflegen. Die konkreten Rechte weisen Sie dann an dem jeweiligen Objekt (z.B. Order oder Seite) dort unter dem Reiter "Zuweisung" zu.

Weitere Informationen zu Gruppen erhalten Sie im Kapitel :ref:`sec_gruppen`
