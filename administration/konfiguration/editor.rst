.. _sec_konfiguration_editor:

=====================
Visueller Editor Kupu
=====================


Tab config
==========

Reference Browser
+++++++++++++++++

Hier kann Kupu als Werkzeug für die Referenzsuche eingeschaltet werden, wenn man unter Bearbeiten/Kategorisierung sogenannte „Verweise“ hinzufügen will. Normalerweise öffnet sich dafür ein kleines Fenster.
Bei uns funktioniert das derzeit nicht.

HTML View
+++++++++
Hier kann man einstellen, ob Kupu bereits clientseitig unerwünschte HTML tags herausfiltern soll, sodass der Redakteure in der Quellcodeansicht sieht, was entfernt wird, oder dies erst beim Absenden geschehen soll.
Welche HTML tags gefiltert werden sehen Sie unter dem Punkt Konfiguration/HTML-Filter

Original Image Size
+++++++++++++++++++
Fügt man Bilder hinzu, kann man zwischen verschiedenen Größen wählen, in denen das Bild in die Seite eingefügt werden soll. Standardmäßig steht keine Option bereit, das Bild in originaler Größe einzufügen. Mit einem gesetzten Haken an dieser Stelle, taucht die Größenoption „Original“ ganz unten in der Auswahl auf.

.. _fig_konfiguration_kupu:

.. figure::
   ./images/konfiguration-kupu.png
   :width: 100%
   :alt: Visueller Editor Kupu

   Konfigurationsmenü zur Visueller Editor Kupu


Styles
++++++

Tables
------
Hier sind zeilenweise alle im Kupu verfügbaren Tabellenstile aufgelistet. Die Syntax ist: „CSS-Klassenname|Titel in der Auswahl“
Es können weitere Tabellenstile definiert werden - gegebenenfalls müssen Sie dafür in der Zope-Management-Oberfläche eigene CSS Klassen anlegen. Sprechen Sie hierfür den Plone Support an.

Styles
------
Hier sind zeilenweise alle im Kupu verfügbaren Stile aufgelistet.
Die Syntax ist „Titel in der Auswahl|HTML tag“ oder „Titel in der Auswahl|HTML tag|CSS Klasse(n)“

HTML Filter
+++++++++++
Inzwischen ist der HTML Filter ein eigener Menüpunkt unter Konfiguration/HTML Filter.

Tab libraries
=============
Wenn man mit Kupu den Server durchsucht (für interne Links oder das Einfügen von Bildern) hat man im Dialog links eine Auflistung von vordefinierten Bibliotheken zum Schnellzugriff (wie im Dateibrowser). Hier werden diese definiert.
Wir empfehlen hier nichts zu ändern. Ansonsten vorher Plone-Support fragen.
official: „This form supplies the list of libraries which form the leftmost
column of the image and internal link drawers.“

.. _fig_konfiguration_kupu_libraries:

.. figure::
   ./images/konfiguration-kupu-libraries.png
   :width: 100%
   :alt: Kupu-Libraries

   Kupu-Libraries

Tab resource types
==================

Wieder etwas, wo man nichts zu ändern braucht.
Während die Bibliothek Orte auf dem Server oder Zusammenstellungen definierten, können diese hier um einen Typfiter ergänzt werden. So werden für Mediatypen nur Bilder angezeigt, Collectionfilter zeigen nur Ordner etc. pp.

Darunter können action urls für bebilderte Inhalte wie Bilder oder Newsitems angegeben werden - im Dialogfenster wird dann mit dieser URL eine Vorschaugrafik erzeugt.

Tab documentation
=================

Kupu liefert eine eigene Dokumentationsdatei mit, die sowohl beschreibt wie Kupu installiert wird (das haben wir schon für sie übernommen ;) gern geschehen) und dann die einzelnen Konfigurationsmöglichkeiten. Kupus Dokumentation ist nur in englisch verfügbar.


Tab links
=========
Hier können Links in allen von Kupu bearbeitbaren Textfeldern geprüft und verarbeitet werden. Ein sehr mächtiges Werkzeug!


.. _fig_konfiguration_kupu_links:

.. figure::
   ./images/konfiguration-kupu-links.png
   :width: 100%
   :alt: Links im Kupu

   Links im Kupu

Type (Field Name)
+++++++++++++++++
Auflistung aller verfügbaren Inhaltstypen und Feldnamen, aus dem Katalog. Setzen Sie den Radiobutton, um im folgenden diesen Inhaltstyp zu verarbeiten.

Folders
+++++++
Schränkt die Suche der zu verarbeitenden Inhaltstypen bei Bedarf auf einen einzelnen Ordner im Webauftritt ein.

Info
++++
Zeigt Ihnen die Anzahl der aktuell durch die beiden oberen Filtern ausgewählten Objekte im Katalog.


command buttons
+++++++++++++++

Führt eine von drei Aktionen aus. Beim Klick wird die Resultpage geladen und im Hintergrund die Anfrage an den Server gesendet - je nach Menge der zu verarbeitenden Objekte kann die Bearbeitung mehrere Sekunden dauern. Ihnen wird eine Progressbar angezeigt, sobald die Ergebnisse beim Browser ankommen.

Verarbeitet werden dabei die HREF Attribute von Links und die SRC Attribute von Bildern in den gewählten Feldern. URLs die nicht relativ sind, sondern andere Protokolle verwenden (mailto-Links) oder auf externe Webseiten zeigen werden übersprungen.

check links
-----------
Hier werden keine Inhalte verändert. Sie bekommen eine Auflistung aller Seiten mit Links/Bilder, deren relative URLs von Kupu als fehlerhaft erkannt wurden.

relative path -> uids
---------------------
Sie bekommen eine vorab Auflistung aller URLs die als relative Pfade angegeben wurden, die Kupu in UID-Referenzen umwandeln könnte. (Ihnen wird angezeigt, welche Textpassagen Kupu durch welche UID-Referenzen ersetzen würde.)
Vor jedem Eintrag wird Ihnen eine Checkbox angezeigt - hier können Sie nun auswählen, welche Links umgewandelt werden sollen und dann mit einem Klick auf den commit-button die Änderung durchführen lassen.

uids -> relative path
---------------------
Der Befehl sucht nach allen resolveuid Einträgen in HREF und SRC Attributen und ersetzt diese durch relative Pfadangaben. Auch hier wird Ihnen erst die Eregebnismenge angezeigt und Sie können festlegen, für welche Felder Sie die Umwandlung durchführen möchten.

Tab toolbar
===========

Hier kann die Werkzeugleiste des Kupu konfiguriert werden, also welche Schaltflächen angezeigt werden sollen.
Entfernen Sie einfach den Haken bei „visible“ um eine ganze Buttongruppe, oder nur einen einzelnen Button zu deaktivieren. Einzelne Buttons können auch mit einer Bedingung verknüpft werden, was im Normalbetrieb aber nicht nötig seien sollte.
