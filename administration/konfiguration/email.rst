.. _sec_konfiguration_email:

======
E-Mail
======

Um wichtige Funktionen in Plone nutzen zu können, muss dem CMS ein Ausgangsserver für
E-Mails zur Verfügung stehen.
Uni-Bonn Plones kommen hier mit einer Voreinstellung, die ein Verschicken von Mails über die zentralen Mailserver des HRZ ermöglicht. Sie sollten diese Einstellungen nicht abändern.
(siehe Abbildung :ref:`fig_konfiguration_e_mail`).

.. _fig_konfiguration_e_mail:

.. figure::
   ./images/konfiguration-e-mail.*
   :width: 100%
   :alt: E-Mail-Konfiguration

   E-Mail-Konfiguration

SMTP-Server
   Dies ist der Ausgangsserver für E-Mails. Im Standard ist hier mail.uni-bonn.de voreingestellt. Sie sollten diesen Wert nicht ändern.

SMTP-Server-Port
   Der Standard-Port für E-Mail lautet »25«. Fragen Sie Ihren Systemadministrator nach
   dem richtigen Port.

ESMTP-Benutzername
   ESMTP wird an der Universität Bonn derzeit nicht genutzt.

ESMTP-Passwort
   ESMTP wird an der Universität Bonn derzeit nicht genutzt.

Absendername der Website
   Plone verschickt in verschiedenen Situationen E-Mails. Tragen Sie hier einen sinnvollen
   Absendername ein, sodass der Empfänger weiß, woher die E-Mail kam.

Absenderadresse der Website
   Tragen Sie hier eine gültige E-Mail-Adresse ein. Plone versendet E-Mails mit dieser
   Absenderadresse, sodass etwaige Antworten an diese E-Mail-Adresse zugestellt werden.
   Es muss sich hierbei um eine Mailadresse handeln, die auf dem zentralen Mailserver des HRZ liegt. 
