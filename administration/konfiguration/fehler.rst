.. _sec_konfiguration_fehler:

======
Fehler
======

Im Fehlerprotokoll werden klassischerweise Fehler innerhalb Ihres Plones gelistet. Sie können diesen Dialog nur selten zur konkreten Fehlersuche benutzen. Das Bonner Plone-Setup verteilt die einzelnen Plone-Auftritte über viele Server. Sie können an dieser Stelle nur die Fehler des einzelnen Servers sehen, dem Sie gerade zugewiesen wurden. Wenn Sie eine genaue Fehleranalyse benötigen, wenden Sie sich bitte an den Plone-Support, nur der Betrieb hat die Möglichkeit in alle Fehlerlogs Einsicht zu nehmen.

.. _fig_konfiguration_fehler:

.. figure::
   ./images/konfiguration-fehler.*
   :width: 100%
   :alt: Konfiguration-Fehler

   Fehlerprotokoll
