.. index: Konfiguration

============================
Die Konfigurationsoberfläche
============================

.. sectionauthor:: Jan Ulrich Hasecke

Plone ist ein konfigurierbares und erweiterbares CMS. Die Bedienung, das Aussehen und
diverse Funktionen lassen sich individuell anpassen. Funktionen, die in der Basisversion
von Plone nicht vorhanden sind, können durch die Installation von so genannten
Zusatzprodukten nachgerüstet werden.

Das Konfigurationsmenü
======================

Die zentrale Schaltstelle, um Plone zu erweitern und zu konfigurieren, ist das
Konfigurationsmenü (siehe Abbildung :ref:`fig_website_konfiguration`).

.. _fig_website_konfiguration:

.. figure::
   ./images/website-konfiguration.*
   :width: 100%
   :alt: Die Übersichtsseite der Konfiguration

   Website-Konfiguration

Das Konfigurationsmenü ist in drei Teile gegliedert:

Plone-Konfiguration
   In diesem Bereich werden die Funktionen der Basisversion von Plone konfiguriert.

Konfiguration von Zusatzprodukten
   Nachinstallierte Zusatzprodukte können teilweise ebenfalls konfiguriert werden.
   Verweise zu den jeweiligen Konfigurationsmenüs finden sich unter dieser Überschrift.

Plone-Versionsübersicht
   Unter dieser Überschrift finden Sie Angaben zu den Versionen der Software-Komponenten,
   die für den Betrieb von Plone benötigt werden. Außerdem finden Sie hier einen Hinweis,
   ob Plone im `Produktionsmodus` oder im `Entwicklungsmodus` läuft. Konsultieren Sie
   diese Angaben vor allem dann, wenn Sie bei Problemen in einem öffentlichen
   Support-Forum Rat suchen.


Im Folgenden werden die einzelnen Bereiche der Website-Konfiguration erklärt.

.. toctree::
   :maxdepth: 1

   aussehen.rst
   benutzer-und-gruppen.rst
   email.rst
   fehler.rst
   html-filter.rst
   kalender.rst
   kollektionen.rst
   navigation.rst
   regeln.rst
   sicherheit.rst
   sprache.rst
   suche.rst
   textauszeichnung.rst
   editor.rst
   wartung.rst
   website.rst
   zusatzprodukte.rst
   zmi.rst
