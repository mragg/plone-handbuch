.. _sec_konfiguration_kalender:

========
Kalender
========

.. _fig_konfiguration_kalender:

.. figure::
   ./images/konfiguration-kalender.*
   :width: 100%
   :alt: Kalendereinstellungen

   Kalendereinstellungen

In den Kalendereinstellungen (Abbildung :ref:`fig_konfiguration_kalender`) können Sie
einstellen, mit welchem Tag die Woche im Kalenderportlet (siehe Abbildung
:ref:`fig_portlet_calendar`) beginnen soll. Voreingestellt ist Montag.

Darüber hinaus haben Sie die Möglichkeit zu bestimmen, welchen Status im Arbeitsablauf
Termine haben müssen, um im Kalenderportlet angezeigt zu werden. Voreingestellt ist
»Veröffentlicht«. 
