.. _sec_konfiguration_kollektionen:

============
Kollektionen
============

Sie können hier Einstellungen für Kollektionen vornehmen (z.B. welche Felder für den Suchindex benutzt werden). Im Regelfall sollten Sie hier keine Änderungen vornehmen.

.. _fig_konfiguration_kollektionsindicies:

.. figure::
   ./images/konfiguration-kollektionsindicies.*
   :width: 100%
   :alt: Kollektionsindicies

   Kollektionsindicies
