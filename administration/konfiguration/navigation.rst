.. _sec_konfiguration_navigation:

==========
Navigation
==========

In diesem Bereich der Website-Konfiguration (siehe Abbildung
:ref:`fig_konfiguration_navigation`) können Sie bestimmen, was in der Hauptnavigation,
dem Navigationsportlet und der Übersicht angezeigt wird.

.. _fig_konfiguration_navigation:

.. figure::
   ./images/konfiguration-navigation.*
   :width: 100%
   :alt: Konfigurationsmöglichkeiten für die Navigation

   Konfiguration der Navigationselemente

Erzeuge Hauptnavigation automatisch
   In der Voreinstellung ist diese Option angewählt. Sie bewirkt, dass Artikel, die in
   der obersten Ebene einer Plone-Website angelegt werden, in der Hauptnavigation
   erscheinen. 

Erstelle für alle Artikel Einträge in der Hauptnavigation
   Diese Option ist ebenfalls in der Voreinstellung aktiviert. Es werden alle Artikeltypen
   in der Hauptnavigation angezeigt, die unter der nachfolgenden Überschrift »Erscheinende
   Artikeltypen« ausgewählt sind. In der Voreinstellung sind dies alle Artikeltypen.
   Schaltet man diese Option aus, werden nur Ordner automatisch in der Hauptnavigation
   angezeigt.

Erscheinende Artikeltypen
   Wenn die vorherige Option aktiviert wurde, kann man hier im Einzelnen bestimmen, ob ein
   Artikeltyp in der Hauptnavigation, dem Navigationsportlet und der Übersicht erscheinen
   soll. 

Filtere nach Veröffentlichungsstatus
   Man kann die Artikel, die in Hauptnavigation, Navigationsportlet und Übersicht
   erscheinen nach ihrem Status filtern. Diese Option ist nicht zu verwechseln mit dem
   Grundsatz, das nur die Artikel angezeigt werden, die der betreffende Benutzer auch
   sehen darf. Aktiviert man beispielsweise die Option und wählt den Status »Privat«,
   verschwinden auch für einen Administrator mit weitgehenden Rechten alle Artikel aus den
   Navigationselementen, die nicht »privat« sind.   
