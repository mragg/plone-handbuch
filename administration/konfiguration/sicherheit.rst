.. _sec_konfiguration_sicherheit:

==========
Sicherheit
==========

Behalten Sie an dieser Stelle die Grundeinstellungen Ihres Plones bei und ändern Sie diese nicht ab.

.. _fig_konfiguration_sicherheitseinstellungen:

.. figure::
   ./images/konfiguration-sicherheitseinstellungen.*
   :width: 100%
   :alt: Sicherheitseinstellungen

   Sicherheitseinstellungen
