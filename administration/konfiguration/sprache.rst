.. _sec_konfiguration_sprache:

=======
Sprache
=======

Im Bereich :guilabel:`Sprache` der Website-Konfiguration lässt sich einstellen, in welcher
Sprache die Benutzeroberfläche von Plone erscheinen soll (siehe Abbildung
:ref:`fig_konfiguration_sprache`). 

.. _fig_konfiguration_sprache:

.. figure::
   ./images/konfiguration-sprache.*
   :width: 100%
   :alt: Konfigurationsmenü zur Einstellung der Sprache

   Konfigurationsmenü zur Einstellung der Sprache

Im Auswahlmenü :guilabel:`Sprache der Website` kann man eine der über fünfzig zur
Verfügung stehenden Sprachen auswählen. Wählen Sie die gewünschte Sprache aus und
speichern Sie die Eingabe. Nach dem erneuten Laden der Website erscheint die
Benutzeroberfläche in der ausgewählten Sprache.

Falls eine länderspezifische Sprachvariante wie zum Beispiel das Deutsche in der Schweiz
ausgewählt werden soll, muss zunächst die Option :guilabel:`Benutze länderspezifische
Sprachkodes` angewählt werden. Das Auswahlmenü :guilabel:`Sprache der Website` enthält
dann auch die diversen Sprachvarianten, die denkbar sind. Beachten Sie jedoch, dass in
der Grundversion von Plone für die meisten länderspezifischen Sprachvarianten noch kein
Wörterbuch vorliegt.  
