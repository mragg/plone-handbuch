.. _sec_konfiguration_suche:

=====
Suche
=====

Die Standard-Plone-Suche ist im Bonner Setup deaktiviert. Im Normalfall führt das Suchenfeld im Kopf der Seite Ergebnisse aus der Google Search Appliance auf und nicht aus der beschränkten Plone-Suche.
Für die Suche z.B. in einem einzelnen Ordner können Sie das Suchenportlet benutzen. Hier in der Konfiguration legen Sie fest welche Artikeltypen in der Suche dort berücksichtigt werden sollen.

.. _fig_konfiguration_suche:

.. figure::
   ./images/konfiguration-sucheinstellungen.*
   :width: 100%
   :alt: Konfigurationsmenü zur Einstellung der Suche

   Konfigurationsmenü zur Einstellung der Suche
