.. _sec_konfiguration_textauszeichnung:

================
Textauszeichnung
================

Das Standardformat bei Texteingaben ist HTML. Der Benutzer gibt HTML-formatierten Text in
der Regel mit Hilfe des Texteditors Kupu (siehe Kapitel :ref:`sec_kupu`) ein, sodass ihm
oft nicht bewusst ist, dass Plone HTML verarbeitet und speichert. Der Benutzer kann jedoch
auch direkt HTML-formatierten Text eingeben. Kupu bietet ihm dazu eine passende Funktion
an.

Im Bereich :guilabel:`Textauszeichnung` der Website-Konfiguration (siehe Abbildung
:ref:`fig_konfiguration_textauszeichnung`) können Sie ein anderes Standardformat
auswählen, alternative Formate zur Eingabe anbieten und Funktionen einschalten, die aus
Wikis bekannt sind.

.. _fig_konfiguration_textauszeichnung:

.. figure::
   ./images/konfiguration-textauszeichnung.*
   :width: 100%
   :alt: Auswahl der Formate zur Texteingabe

   Einstellungen für Textauszeichnung


Teilformular »Textauszeichnung«
===============================

Im Teilformular »Textauszeichnung« legen Sie fest, in welchen Formaten Text eingegeben
werden kann. Als Formate stehen Ihnen zur Verfügung:

text/html
   HTML Auszeichnungssprache (in der Voreinstellung aktiviert)

text/plain
   Einfacher Text ohne Formatierungen

text/plain-pre
   Einfacher Text, der in ein <pre>-Tag für `Preformatted Text` gepackt
   wird.

text/restructured
   `Restructured Text` ist eine vereinfachte Markup-Sprache.

text/structured
   `Structured Text` ist eine vereinfachte Markup-Sprache.

text/x-python
   Für Python-Code. Eingegebener Code wird syntaktisch eingefärbt.

text/x-rst
   Zur Eingabe von `Restructured Text`.

text/x-web-intelligent
   Zur Eingabe von einfachem Text. Einrückungen und Absätze bleiben erhalten. Webadressen
   und E-Mail-Adresse werden so umgewandelt, dass sie zu anklickbaren Links werden.

text/x-web-markdown
   `Markdown` ist eine vereinfachte Markup-Sprache. Um Markdown nutzen zu können, ist die
   Installation des Python-Moduls Markdown_ erforderlich.

text/x-web-textile
   `Textile` ist eine vereinfachte Markup-Sprache. Um Textile nutzen zu können, ist die
   Installation des Python-Moduls Textile_ erforderlich.


.. _Markdown: http://pypi.python.org/pypi/Markdown/2.0.3

.. _Textile: http://pypi.python.org/pypi/textile/2.1.4
