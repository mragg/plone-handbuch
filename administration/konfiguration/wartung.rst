.. _sec_konfiguration_wartung:

=======
Wartung
=======

Einrichtungen haben nicht das Recht ein Plone in den Wartungsmodus zu versetzen, dieser Bereich ist ohne Funktion.
