.. _sec_konfiguration_zmi:

==========================
Zope-Management-Oberfläche
==========================

Aus Sicherheitsgründen wird der Zugriff auf die Administrationsoberfläche gesperrt, diese Einstellungen können nur noch vom Plone-Support vorgenommen werden.
Sollten Sie hier Einträge vornehmen wollen, wenden Sie sich bitte an den Plone-Support plone@uni-bonn.de

Über die Zope-Management-Oberfläche (ZMI) erreichen Sie weitere Konfigurationsmöglichkeiten Ihres Plones. Bitte beachten Sie, dass Sie hier nur tätig werden sollten, wenn Sie "wissen, was Sie tun". Sie laufen hier ansonsten Gefahr die grundlegende Funktionalität Ihres Webauftritts zu gefährden. Bitte testen Sie mögliche Änderungen zuerst in der Testumgebung und nehmen Sie bei Fragen Kontakt mit dem Plone-Support auf.

.. _fig_konfiguration_zmi:

.. figure::
   ./images/konfiguration-zmi.*
   :width: 100%
   :alt: ZMI

   Zope-Management-Oberfläche
