﻿.. index: Webstatistik
.. _sec_piwik:

========================
Webstatistiken mit Piwik
========================

Piwik ist das Statistik-Tool unseres Content Management Systems. Wir erklären
Ihnen, was genau Piwik kann und wie Sie damit arbeiten.

.. toctree::
   :maxdepth: 1

   piwik.rst
   piwik-impressum.rst
