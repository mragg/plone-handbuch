.. index:: Impressum (PIWIK)
.. _sec_piwik_impressum:

============================
Impressum für Piwik anpassen
============================

Plone-Nutzern, die unseren PIWIK-Statistik-Service nutzen, wird empfohlen das Impressum
zu aktualisieren und Opt-Out-Möglichkeit zu integrieren, um Besuchern ein
»No-tracking«-Cookie zu ermöglichen.

Fügen Sie nachfolgenden Code-Block bitte in der HTML-Ansicht des Editors ein (am besten
im FCK-Editor - evtl. müssen Sie im HTML-Filter vorher iframe freischalten):

Fügen Sie hierzu unter - »Konfiguration« → »HTML-Filter« → »HTML-Tags« 
ganz unten unter »Benutzerdefinierte Tags« die Option »iframe« hinzu

.. _fig_piwik_impressum:

.. figure:: ./images/impress-piwik.*
    :width: 40%
    :alt: Impressum fuer Piwik
      
    Impressum für Piwik

Fügen Sie nachfolgenden Code-Block bitte in der HTML-Ansicht des Editors ein: ::

	<p><strong>Statistische Auswertungen mit PIWIK<br /></strong>
	Diese Website benutzt Piwik, eine Open-Source-Software zur statistischen Auswertung
	der Besucherzugriffe. Piwik verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer
	gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen.
	Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieses Internetangebotes
	werden auf dem Server des Anbieters in Deutschland gespeichert. Die IP-Adresse wird sofort
	nach der Verarbeitung und vor deren Speicherung anonymisiert. Sie können die Installation
	der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir
	weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche
	Funktionen dieser Website vollumfänglich nutzen können.</p>
	<p><iframe src="https://cms-webstat.rhrz.uni-bonn.de/index.php?module=CoreAdminHome&amp;action=optOut" 
	frameborder="no" height="220px" width="500px">
	</iframe></p>

`Tipp: Am besten im FCK-Editor - evtl. müssen Sie im HTML-Filter vorher iframe freischalten:`

Fügen Sie hierzu unter »Konfiguration« → »HTML-Filter« → »HTML-Tags« ganz unten unter
»Benutzerdefinierte Tags« die Option »iframe« hinzu.

Wenn Sie Rückfragen haben, wenden Sie sich bitte an unseren Plone-Support, wir helfen
gerne bei der Integration des Optouts.
