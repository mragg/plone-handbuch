.. index:: PIWIK
.. _sec_piwik_benutzen:

==================
Übersicht zu Piwik
==================


Viele Einrichtungen möchten gerne Statistiken über die Nutzung der eigenen Webseiten
auswerten, um auf Grund dieser Daten die Webseiten besser zu strukturieren und zu
gestalten. 

Wir bieten einen Service mit Hilfe der Open Source Software »Piwik« 
(http://www.piwik.org) an, die Zugriffsstatistiken erstellt. Piwik protokolliert 
die Zugriffe auf Ihre Webseite und bereitet diese in Statistiken auf. 

Sie können so z.B. den zeitlichen Verlauf der Zugriffe auf Ihre Webseite sehen, 
Sie erhalten Daten, über welche Suchmaschinen und mit welchen Suchbegriffen die 
Besucher auf Ihre Webseite kommen. Die Statistiken werden zum einen »kummuliert«, 
Sie können aber auch recht einfach beispielsweise Seitenbereiche identifizieren, 
die von Besuchern nur sehr selten angesteuert werden.

.. _fig_piwik_anzeigen:

.. figure:: ./images/piwik_1.*
   :width: 60%
   :alt: Piwik
   
   Piwik

Standardmäßig ist das Erstellen von Zugriffsstatistiken deaktiviert. Auf Wunsch können
wir die Erstellung von Statistiken aktivieren. Hierzu benötigen wir vom **Administrator
der Webseite** einen Auftrag.

Sämtliche Zugriffe werden nur anonymisiert erfasst (keine Protokollierung von
IP-Adressen). Sie bekommen keine »Live-Statistiken«.

Die Auswertungen sind nur vom Plone-Administrator einsehbar und durch ein Login
geschützt. Auf die Nutzung von :ref:`Webstatistiken <sec_piwik_impressum>` soll prominent
im Impressum hingewiesen werden. Sie finden unten einen Textbaustein der Pressestelle, den
Sie verwenden können.

Die von uns eingesetzte Konfiguration von Piwik erfolgt in Abstimmung mit dem
Datenschutzbeauftragten der Universität Bonn.

Wir haben Plone zudem dahingehend erweitert, dass Sie mehrere »Piwik-Codes« innerhalb
einer Installation verwenden können, um separate Statistiken z.B. für Teilseiten
erstellen zu können. Zusätzliche Piwik-Codes sind dann jeweils an die »Navigation Root«
gekoppelt.


Piwik aktivieren
================

Damit Sie Piwik nutzen können sind mehrere Schritte notwendig:

  1. Die Piwik-Nutzung kann nur von einem Plone-Administrator beantragt und aktiviert
     werden, nur er bekommt direkten Zugang zu den Statistiken.
  2. Nutzen Sie zur Piwik-Beantragung bitte das zuständige Support-Formular.
  3. Wir richten Ihnen dann einen Zugang auf dem Statistik-Server ein und tragen den
     notwendigen Code, in Ihrem Plone-Portal ein. (»Konfiguration « → »Webseite«)
     
     .. _fig_piwik_aktivieren:
     
     .. figure:: ./images/piwik_2.*
        :width: 85%
        :alt: Piwik aktivieren
      
        Piwik aktivieren
   
     Wenn Sie wollen, können Sie sich das Javascript im Abschnitt "Javascript für
     Web-Statistik-Unterstützung" ansehen. Führen Sie hier bitte keine Änderungen an den
     Umbrüchen usw. durch, weil dies unter Umständen die Protokollierung verhindert.
    
     .. _fig_piwik_statistik:
    
     .. figure:: ./images/piwik_3.*
        :width: 85%
        :alt: Javascript für Web-Statistik
      
        Javascript für Web-Statistik
      
  
  4. Sie bekommen von uns gleichzeitig die Adresse des Statistik-Servers mitgeteilt, auf
     dem Sie die Statistiken abrufen können. Dieser ist nur aus dem IP-Bereich des BONNETs
     erreichbar.
  5. Informieren Sie im Impressum Ihrer Webseite über die Nutzung von Piwik zur
     Erstellung von Besucherstatistiken


Kein hochverfügbares System
===========================
Wir weisen darauf hin, dass es sich bei unseren Piwik-Servern 
nicht um Kerndienste des HRZ handelt. Die Server werden nicht doppelt verfügbar 
betrieben, daher kann es durchaus auch zu unangekündigten Downtimes, bedingt 
durch Wartungs- oder Konfigurationsarbeiten kommen. 

Der CMS-Betrieb als solcher wird dadurch allerdings in keinster Weise beeinträchtigt.


Aufbewahrungsdauer der Statistiken
==================================
Die erfassten Daten können aus Performance- und Platzgründen nicht unbegrenzt 
vorgehalten werden. Daher werden Detaildaten, die älter als 1 Jahr sind, 
automatisch gelöscht. Montats- und Jahresstatistiken werden derzeit noch 
dauerhaft aufbewahrt. 

Wir hoffen, Ihnen damit einen guten Kompromiss zu bieten, 
um Trends im Auge behalten zu können. Wir beobachten das Datenaufkommen 
allerdings noch und behalten uns vor, zukünftig auch diese Daten nach einem 
Jahr zu löschen.

