.. _sec_voreinstellungen_administration:

================================
Grundlegende (Vor-)Einstellungen
================================

Die folgenden Einstellungen sind für die gesamte Website vorzunehmen und nicht
mit persönlichen Einstellungen einzelner Benutzer zu verwechseln.
Als Administrator erreichen Sie die Plone-Konfiguration über einen Verweis
rechts oben auf jeder Seite.

Persönliche Ordner
==================

Per Voreinstellung werden keine persönlichen Ordner für registrierte Benutzer
der Website angelegt. Gegebenenfalls müssen Sie persönliche Ordner erst
einschalten:

* Wählen Sie im Konfigurationsmenü den Eintrag :guilabel:`Sicherheit`.
* Kreuzen Sie das Feld :guilabel:`Persönliche Benutzerordner` an und speichern Sie
  das Formular.


Syndizierung
============

Für Kollektionen ist die Syndizierung bereits eingeschaltet. Um die
Syndizierungseinstellungen bei Kollektionen ändern zu können und die
Syndizierung bei Ordnern einzuschalten, muss die Artikelansicht :guilabel:`Syndizierung`
sichtbar gemacht werden.

Diese Einstellungen kann nur noch vom Plone-Support vorgenommen werden, bitte wenden Sie sich an plone@uni-bonn.de
* Wechseln Sie in das ZMI (Zope Management Interface).
* Wählen Sie das Werkzeug :guilabel:`portal_actions`, darin den
  Unterordner :guilabel:`object` und in diesem wiederum die Aktion
  :guilabel:`syndication` (Syndizierung).
* Kreuzen Sie das Feld :guilabel:`Visible` (»sichtbar«) an, und speichern Sie
  das Formular.
