.. index:: Accordion Menü, Aufklappbare Absätze
.. _accordion-menue:
.. _sec_accordion-menue:

==============
Accordion Menü
==============

Sie können in Plone inzwischen sogenannte Accordion-Menüs verwenden, d.h. Abschnitte
werden durch einen Klick aufgeklappt.

Nachfolgend zuerst ein Beispiel:

.. figure:: ../arbeiten-mit-plone/images/accordion-beispiele-auf.png
   :width: 70%
   :alt: Accordions Beispiele


»Accordion Heading« und »Accordion Paragraph« lassen weiter folgende Textabsätze aus- und aufklappen. Dabei sind die beiden Stiles der Überschrift gleichwertig. Weisen Sie »Accordion Heading« der Zeile zu, die später als Überschriften stehen bleiben und damit "klickbar" sein soll. Die Zeile bekommt Design der blauen Überschrift zweiten Grades.

Darunter schreiben Sie wie gewohnt den Text, der später aufgeklappt wird. Benutzen Sie »Accordion Paragraph« für ein Accordion Menü mit einer kleineren Überschrift. Das Design entspricht dem fett markierten Text. Ein Ineinanderschachteln von Accordion Heading und Paragraph ist nicht möglich.

Ein Accordion endet an der Stelle, an der entweder das nächste Accordion anfängt oder an der Sie eine Überschriften zweiten Grades (in Kupu heißt diese schlicht »Überschrift«) zuweisen. Falls beides nicht ihr Fall ist, können Sie einem Textabschnitt auch den Stil »Stop Collapse« zuweisen - das Accordion endet dann zu Beginn dieses Absatzes. Den zweiten und dritten Fall benötigen Sie, wenn Sie Accordions in die Seite integrieren wollen und unter einem Accordion die Seite normal weiter geht.

Die Accordion-Funktion (d.h. das aktive Ein- und Ausklappen) ist im Redaktionsbereich innerhalb des Editors deaktiviert, da die Funktion ansonsten mit der Funktionalität des Editors kollidiert. Für Redakteure heißt das: Lassen Sie sich nicht dadurch irritieren, dass Sie ein Accordion angelegt haben und es scheinbar nicht funktioniert. Die Funktionalität können Sie erst nach dem Veröffentlichen (unangemeldet von außen) über Ihre normale Webseite testen.

Sie können festlegen, dass ein Accordion beim Laden der Seite automatisch aufgeklappt wird. Hierzu verwenden Sie bitte die Klasse »Accordion Heading open« und »Accordion Paragraph open«.

Anwendung
=========

Accordions können Sie selbst beliebig frei erzeugen.
Sie finden dazu in den Editoren Kupu und FCK einen
Eintrag in dem Menü, in dem Sie z.B. auch Überschriften
etc. zuweisen.

.. figure:: ./images/accordion-kupu.png
   :width: 25%
   :alt: Im Kupu

   Im Kupu

.. figure:: ./images/accordion-fck.png
   :width: 25%
   :alt: Im FCK

   Im FCK

Weisen Sie »Accordion Heading« der Zeile zu, die später als Überschriften stehenbleiben
und damit "klickbar" sein soll. Darunter schreiben Sie wie gewohnt den Text, der später
aufgeklappt wird. Benutzen Sie »Accordion Paragraph« für ein Accordion Menü mit einer
kleineren Überschrift. Ein Ineinanderschachteln von Accordion Heading und Paragraph ist
nicht möglich.

**Hinweis:** In Portlets funktionieren Accordion Menüs nicht.


Accordion-Abschnitt beenden
===========================

Ein Accordion endet an der Stelle, an der entweder das nächste Accordion anfängt oder an
der Sie eine Überschriften zweiten Grades (in Kupu heißt diese schlicht »Überschrift«)
zuweisen. Falls beides nicht ihr Fall ist, können Sie einem Textabschnitt auch den
Stil »Stop Collapse« zuweisen - das Accordion endet dann zu Beginn dieses Absatzes. Den
zweiten und dritten Fall benötigen Sie, wenn Sie Accordions in die Seite integrieren
wollen und unter einem Accordion die Seite normal weiter geht.


Accordion testen
================

Die Accordion-Funktion (d.h. das aktive Ein- und Ausklappen) ist im Redaktionsbereich
innerhalb des Editors deaktiviert, da die Funktion ansonsten mit der Funktionalität des
Editors kollidiert. Für Redakteure heißt das: Lassen Sie sich nicht dadurch irritieren,
dass Sie ein Accordion angelegt haben und es scheinbar nicht funktioniert. Die
Funktionalität können Sie erst nach dem Veröffentlichen (unangemeldet von außen) über
Ihre normale Webseite testen.


Accordion automatisch aufklappen
================================

Sie können festlegen, dass ein Accordion beim Laden der Seite automatisch aufgeklappt
wird. Hierzu verwenden Sie bitte die Klasse ``Accordion Heading open``.


Navigationsboxen mit Akkordions
===============================
Auf vielen Seiten im Webauftritt der Universität werden inzwischen "Navigationsboxen" verwendet, die eine Kombination aus Bild und darunter befindlichem Akkordion darstellen.
Im FCKEditor gibt es vordefinierten Site-Vorlagen, mit denen Sie barrierfreie und für mobilgeräte optimierte Navigationsboxen mit enthaltenen Akkordions anlegen können.
:ref:`sec_fck`


Accordion aufklappen über Links
===============================

Wenn Sie auf eine Seite linken, die Accordions enthält, können Sie über die Verlinkung
mitgeben, dass ein Accordion beim Laden der Seite automatisch aufgeklappt wird. Hierzu
ist etwas Handarbeit notwendig.

Alle Accordions einer Seite werden automatisch durchnummeriert - beginnend mit 0. Aus
dem Bereich der Verlinkung kennen Sie vermutlich den »Anker« über den Sie zu einer
bestimmten Stelle im Text springen können. Alle Accordions sind automatisch auch immer
Anker. Diese Seite hier hat die URL ``www.plone.uni-bonn.de/faq/accordion-menu`` - wenn ich
auf diese Seite verlinke, kann ich gezielt z.B. das erste Accordion anspringen und beim
Laden der Seite direkt aufklappen lassen. Die URL für den Link ist dann:
``www.plone.uni-bonn.de/faq/accordion-menu#0``

Bitte beachten Sie, dass sich diese Methode nur eignet, wenn Ihre Seite "stabil" ist.
Sobald Sie die Reihenfolge der Accordions ändern, müssen Sie alle Verlinkungen ändern,
da sich die Accordionnummerierung für die Anker mitändert.
