﻿.. index:: Backup, Restore
.. _sec_backup:

========================
Daten gelöscht, was tun?
========================

Dieser Artikel gibt Ihnen eine Übersicht der Dinge, die Sie im Falle eines Datenverlusts
tun können, dank einer ausgefeilten Backupstrategie können wir in den allermeisten
Notfällen helfen.

Ein Datenverlust ist "der schlimmste Fall", wir haben versucht, möglichst viele
Vorkehrungen zu treffen, um einen Datenverlust nicht auftreten zu lassen bzw. Ihnen auch
bei versehentlichem Löschen von Daten weiterhelfen zu können.

Wie wir Daten sichern
=====================
Wir legen innerhalb des CMS-Systems alle Daten in unserer Datenbank ab.

Texte, Navigation, Seitenlayout, binäre Daten wie Dateien etc. landen in unserer Datenbank - diese Daten werden von uns einmal am Tag (um 05:00 Uhr) auf Bandlaufwerke gesichert. Wir können aus diesen Daten von jedem beliebigen Zeitpunkt ein Restore (Wiederherstellen) vornehmen. Das Backup der Datenbank wird über einen längeren Zeitraum (mehrere Wochen) vorgehalten.

Wir sind dabei in der Lage, auch Teilzweige wiederherzustellen: Wenn Ihr Plone beispielsweise die Daten der kompletten Fakultät enthält und versehentlich z.B. Teile eines Instituts gelöscht wurden, können nur die Instituts-Daten wiederhergestellt werden und wieder in den Komplettauftritt integriert werden. Änderungen, die in der Zwischenzeit an anderer Stelle erfolgten, bleiben also  erhalten.

Notfall-Checkliste
==================

Wenn Sie Daten verloren haben, gehen Sie bitte nach folgender Checkliste vor:

* **Selbsthilfe**

  In jedem Plone gibt es für Administratoren ein »Undo«, um Änderungen/Löschen etc.
  rückgängig zu machen. Sie finden das Undo unter »Konfiguration« →
  »Zope-Management-Oberfläche« → Register »Undo«. Dort sind alle Transaktionen
  aufgelistet, welche einzeln bzw. gruppiert rückgängig gemacht werden können.
  *Bitte beachten Sie, dass dies seit der Sperrung des ZMI-Zugangs für lokale Administratoren
  nur noch vom Plone-Support durchgeführt werden kann!*

  **Wichtig: Es zählt jede Minute**

  Das Zeitfenster in dem Datenbank-Transaktionen rückgängig gemacht werden können ist
  recht klein. Faustregel hierbei: :guilabel:`In den ersten 20 Minuten` sind die Chancen
  gut - möglichst weitere Änderungen auf dem Webauftritt nach der Löschung vermeiden.

  * Kontaktieren Sie Ihren Administrator
  * Wenn Sie diesen nicht erreichen, wenden Sie sich an das HRZ/Plone-Team, hier bitte
    telefonisch, da das Zeitfenster so klein ist (Oliver Jähn: 73-5202, Martin Ragg:
    73-4415, Ulrich Marder: 73-3435, IT-Helpdesk: 73-2751)

  **Undo nicht immer erfolgreich**

  Sind Sie "zu spät" für ein »Undo«, dann ist der nächste Ausweg der Rückgriff auf das Backup.
* **Backup benötigt**

  Backups können nur durch das HRZ eingespielt werden. Wenn Sie Daten verloren haben und
  ein Backup benötigen, wenden Sie sich bitte an uns. Im Normalfall sollte dies per
  E-Mail geschehen an plone[at]uni-bonn.de

  Bitte geben Sie unbedingt den genauen Pfad an, der gelöscht wurde und wiederhergestellt
  werden soll - wir brauchen einen Ansprechpartner, den wir telefonisch erreichen können.

  Bitte definieren Sie genau, was wiederhergestellt werden soll, seit wann der
  Datenverlust besteht etc. - je genauer Ihre Angaben, desto besser können wir Ihnen
  helfen.

  Wir bevorzugen hier den E-Mail-Weg - wenn es bei Ihnen allerdings "brennt" und Ihnen
  z. B. versehentlich entscheidende Daten verloren gegangen sind, die zeitnah benötigt
  werden, da z. B. gerade mit einem hohen Besucheransturm auf genau die gelöschten Seiten
  gerechnet werden muss - bitte kontaktieren Sie uns telefonisch:
  (Oliver Jähn: 73-5202, Martin Ragg: 73-4415, Ulrich Marder: 73-3435, IT-Helpdesk: 73-2751)
