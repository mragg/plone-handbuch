.. index:: Editor, FCKeditor, Navigationsboxen
.. _sec_fck:

=============
Der FCKeditor
=============

Eine Alternative zu Kupu ist der FCKeditor. Dieser hat eine deutlich
größere Anzahl von Werkzeugen und Funktionen, aber er ist kein Standardeditor für Plone.
D.h. viele zusätzliche Funktionen, die er mit sich bringt, sind standardmäßig im Plone
deaktiviert. Wir erläutern hier nur die Funktionen, die im Kupu entweder fehlen oder sehr
umständlich organisiert sind, und die Sie ohne Weites im FCKeditor benutzen können. Der
Vergleich der grundlegenden Funktionen in beiden Editoren, machen wir im nächstem Kapitel
"Kupu vs. FCKeditor".

Site-Vorlagen
=============
Der FCKeditor verfügt über Vorlagen, mit denen Sie vorgefertigte Elemente in eine Seite einfügen können. Sie fügen so ein Beispiel/Dummy in Ihre Seite ein, das bereits den korrekten HTML-Code enthält und mit Beispieltext und Beispielbildern vorgefüllt ist. Sie können diese Elemente dann im Anschluss gegen Ihre Inhalte austauschen und behalten dabei den gewünschten Effekt.
In den Site-Vorlagen finden Sie Vorlagen um Bild und Text nebeneinander (ohne Tabellenlayout) zu positionieren.

Navigationsboxen
----------------

Weiterhin finden Sie dort Vorlagen für ein mehrspaltiges Layout/Navigation. Dieses basiert auf reinem CSS ohne Tabellen und ist damit barrierefrei und für Mobilgeräte optimiert. Es werden Navigationsboxen angelegt, die jeweils aus einem Bild und einem Akkordion als Beispiel bestehen.

.. _fig_fck:

.. figure:: ./images/site-vorlagen-1.*
   :width: 80%
   :alt: Sitevorlagen im FCKEditor


Sie finden dort vorformatierte Elemente für ein- bis dreispaltige Layouts.

.. _fig_fck:

.. figure:: ./images/site-vorlagen-2.*
   :width: 60%
   :alt: Sitevorlagen für mehrspaltige Layouts



Sie können auch mehrere mehrspaltige Layouts untereinander positionieren. Wir empfehlen Ihnen eine Seite erst mit Dummies komplett aufzubauen und Ihre eigenen Inhalte erst am Schluss auszutauschen.

.. figure:: ./images/site-vorlagen-3.*
   :width: 60%
   :alt: Sitevorlagen für mehrspaltige Layouts



Die Vorlagen sind jeweils eine Kombination aus einem Bild und einem darunter befindlichen "Akkordion". Sie können alle Elemente mit Ihren eigenen Inhalten austauschen, z.B. auch beim Akkordion die Stil-Klasse ändern um ein eingeklapptes Akkordion zu verwenden.

Einschränkungen
---------------
Bitte beachten Sie, dass eine derart aufgebaute Seite *nur* noch mit dem FCKEditor bearbeitet werden darf. Ein Bearbeiten der Seite mit Kupu zerstört den dahinter liegenden HTML-Code. Es empfiehlt sich bei der Bearbeiten gerade komplexer Seite zur Sicherheit vor der Bearbeitung eine Kopie anzulegen, damit Sie auf die vorhergende Version zurückkönnen. Es kann ab und an zu "unerwünschten" Effekten im FCKEditor kommen bei denen Sie u.U. im Quelltext der Seite eingreifen müssen bzw. wenn dies zu kompliziert ist, Sie eine Sicherheitskopie haben auf die Sie selbst zurückgehen können.

Extrafunktionen
===============

* Aus MS Word einfügen
* Drücken
* Suchen / Suchen und ersetzen
* Alles auswählen
* Formatierung entfernen
* Image Map
* Horizontale Linie einfügen
* Sonderzeichen einfügen/editieren
* Seitenumbruch einfügen
* Smiley einfügen

.. _fig_fck:

.. figure:: ./images/fck.*
   :width: 80%
   :alt: Der Texteditor FCKeditor

   Der FCKeditor

Beachten Sie bitte, dass nur kleingeschriebene Ankernamen funktionieren. Umlaute werden
dabei vom FCK-Editor anders dargestellt z.B.: Überschrift (_berschrift)

Die Anker lassen sich seitenübergreifend realisieren. Nachdem man den Anker definiert
hat, geht man auf die gewünschte 2. Seite und setzt den Link als URL auf die
entsprechende 1. Seite. Im Linkeditor fügt man dann noch am Ende den Ankernamen von
Hand ein (Raute nicht vergessen): ::

 ./resolvuid/...#ankername


.. index:: Image Map
.. _sec_image-map:

Image Map
---------

Der FCK-Editor bietet Ihnen die Möglichkeit an, sehr schnell und einfach eine »Image Map«
zu erstellen.

Mit Hilfe der »Image Map« können Sie verschiedene Bereiche eines Bildes bzw. einer
Grafik auf andere Seiten verlinken. Besonders praktisch kann eine »Image Map« bei der
grafischen Darstellung der Fachmodule sein, auf der jedes Modul bzw. bestimmte Teile der
Grafik zu Seiten mit genaueren Beschreibungen führt.

Voraussetzung: ein eingebautes Bild oder Foto in der Seite.

Im FCK-Editor finden Sie rechts neben dem Button »Bild hinzufügen« den Button »Image Map
einfügen/entfernen«. Es ist empfehlenswert, alle »Image Maps« mit einem eindeutigen
Titel im Feld »Map name« zu versehen, für den Fall, dass man in Zukunft mit dem
Quelle-Code der Seite arbeiten muss. Im Bereich »Image Map Bereiche« kann man wählen,
welche Form der Bildausschnitt haben soll. Sie können eine beliebige Anzahl an Bereichen
markieren und alle Bereichsformen beliebig miteinander kombinieren.

Dabei sind alle Felder optional, was Ihnen ermöglicht, bestimmte Bereiche des Bildes mit
einem kleinen Kommentar in Form eines Popup Fensters zu versehen.

.. _fig_fck_image_map_hinzufuegen:

.. figure:: ./images/fck-image-map-hinzufuegen.*
   :width: 80%
   :alt: Image Map hinzufuegen

   Image Map hinzufügen

.. index:: Link-Symbol

Stiles
======

Die Buttons »Link einfügen/editieren« und »Bild einfügen/editieren« ermöglichen Ihnen
einige vordefinierte Style-Klassen benutzen oder selbst neue Style-Elemente hinzufügen.
Unter dem Reiter »Erweitert« (siehe Abbildung :ref:`fig_fck_link_erweitert`) können Sie
im Feld »Stylesheet Klasse« folgende Style-Klassen aktivieren:

image-left
 positioniert das Bild am linken Rand und macht den folgenden Textabsatz rechts
 umfließend. Entspricht der Kupu-Funktion "Ausrichtung: links".
image-right
 positioniert das Bild am rechten Rand und macht den folgenden Textabsatz links
 umfließend. Entspricht der Kupu-Funktion "Ausrichtung: rechts".
link-plain
 entfern das Link-Symbol (Weltkugel-Symbol, PDF-Symbol usw.)

.. _fig_fck_link_erweitert:

.. figure:: ./images/fck-link-erweitert.*
   :width: 80%
   :alt: Erweiterte Einstellungen fuer den Link

   Erweiterte Einstellungen für den Link oder das Bild

Außerdem kann man im Feld »Style« eigene CSS-Befehle eingeben, wie zum Beispiel: ::

 border:none;

oder ::

 border:2px, dashed, blue;

Mehrere Befehle könne hintereinander folgen: ::

 border:none; text-decoration:blink; align:right;

Achten Sie darauf, dass die Verwendung der eigenen Stile, soll zuerst von dem
Administrator Ihres Portals freigegeben werden. Dies geschieht durch die Anpassung des
HTML-Filters und hat Wirkung auf ganzes Portal. Wenn Sie Admin-Rechte besitzen,
absprechen Sie am besten vorher das geplante Design-Know-how, mit der Pressestelle der
Universität Bonn.
