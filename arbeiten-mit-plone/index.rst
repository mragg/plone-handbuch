.. _sec_tutorien:

===================================
Arbeiten mit Plone - erste Schritte
===================================

Die folgenden Tutorien führen Sie schrittweise in den Gebrauch von Plone ein. Sie bauen
aufeinander auf und ermöglichen Ihnen einen leichteren Zugang zu den später folgenden
Referenzkapiteln.

Beachten Sie, dass der Text und die Abbildungen dieses Buchs eine Beispiel-Website
benutzen, die sich mit der Organisation von Seminaren beschäftigt. Auf dieser Website
gibt es einen Ordner »Veranstaltungen« mit einigem Inhalt. Der Inhalt Ihrer Website hängt
natürlich davon ab, was Ihr Administrator dort bereits angelegt hat, und wird sich in
Einzelheiten vom Buch unterscheiden. Der Lesbarkeit halber geht der Buchtext auf diese
Abweichungen nicht ein.

Aktivieren Sie in den Einstellungen Ihres Webbrowsers Javascript, bevor Sie mit den
Tutorien beginnen, sonst stehen Ihnen einige Funktionen in Plone nur eingeschränkt zur
Verfügung.

.. toctree::
   :maxdepth: 1

   rundgang.rst
   umgang-dokument.rst
   umgang-ordner.rst
   redakteur.rst
   workflow.rst
   persoenliche-seite.rst
   kupu.rst
   fck.rst
   kupu-vs-fck.rst
   accordion-menue.rst
   backup.rst
