.. index:: Kupu, FCKeditor, Editor
.. _sec_kupu_vs_fck:


==================
Kupu vs. FCKeditor
==================

Eine Gegenüberstellung der beiden Editoren im Uni Bonn Plone.

Im Folgenden wollen wir Ihnen unsere beiden Editoren zur Bearbeitung der Plone Seiten
vor- und gegenüberstellen. Wir behandeln hierbei die Standardkonfiguration beider
Editoren, um einen leichten Einstieg und guten Überblick zu ermöglichen. Dieser Überblick
soll die Entscheidung erleichtern, den passenden Editor auszuwählen.

.. _fig_standartansicht_kupu:

.. figure:: ./images/kupu.*
   :width: 80%
   :alt: Kupu

   Standardansicht Kupu-Editor

.. _fig_standartansicht_fck:

.. figure:: ./images/fck.*
   :width: 80%
   :alt: fck

   Standardansicht FCKeditor

Auf den ersten Blick ist der FCKeditor also wesentlich umfangreicher und bietet Ihnen
mehr Möglichkeiten. Die Vorteile des Kupu Editors liegen in seiner Kompakt- und
Einfachheit. Dort finden sie sofort, was sie brauchen und die Fehlerquellen sind auf ein
Minimum beschränkt. Für den FCKeditor lassen sich noch einige weitere Buttons aktivieren,
wie zum Beispiel eine :guilabel:`Blocksatz-Funktion`.

Generell können und möchten wir Ihnen an dieser Stelle keine Empfehlung mitgeben. Jeder
sollte den Editor gemäß seiner Aufgabenbereiche wählen. So lassen sich einfache
Textbearbeitungen und der grundlegende Seitenaufbau mit dem Kupu Editor sicher schneller
und leichter lösen, der FCKeditor setzt aber dort an, wo der Kupu an seine Grenzen stößt
- zum Beispiel können Sie mit dem Kupu keine :guilabel:`Imagemaps` anlegen.

Die größte Gemeinsamkeit der beiden Editoren unseres CMS ist die Beschränkung auf das
Corporate Design. Zwar geben beide bestimme Formatierungen vor, mit denen Textabschnitte
besonders hervorgehoben werden können - Schriftart, -größe oder -farbe lassen sich aber
mit keinem der beiden anpassen.


Besonderheiten des Ankers in den beiden Editoren
================================================

Ein Anker ist ein interner Verweis auf eine Textstelle auf einer Seite.

Wenn Sie »Anker« zum direkten Anspringen von Stellen einer Webseite nutzen, so
unterscheiden sich hier die beiden Editoren, die wir anbieten. Während Sie im
Standard-Editor »Kupu« Anker nur anhand der Formate (also CSS-Auszeichnungen wie
»Call-Out« oder »Überschrift 2« etc.) verwenden können, müssen Sie im FCKeditor die
Ankernamen von Hand setzen.

.. _fig_anker_kupu:

.. figure:: ./images/anker-kupu.*
   :width: 75%
   :alt: Im Kupu

   Anker im Kupu

.. _fig_anker_fck:

.. figure:: ./images/anker-fck.*
   :width: 50%
   :alt: Im FCK

   Anker im FCKeditor

Wenn Sie dies im FCK-Editor nutzen, beachten Sie bitte, dass nur kleingeschriebene
Ankernamen funktionieren. Umlaute werden dabei vom FCKeditor anders dargestellt z.B.:
Überschrift (_berschrift)

Seitenübergreifende Anker
-------------------------

Die Anker lassen sich seitenübergreifende realisieren. Im Kupu: Gehen Sie hier vor, wie
bei einem normalen Verweis. Im FCKeditor: Nachdem man den Anker definiert hat, geht man
auf die gewünschte 2. Seite und setzt den Link als URL auf die entsprechende 1. Seite. Im
Linkeditor fügt man dann noch am Ende den Ankernamen von Hand ein (Raute nicht
vergessen): ::

 ./resolvuid/...#ankername
