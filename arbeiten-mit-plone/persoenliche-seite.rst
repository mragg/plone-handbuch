﻿.. index:: Meine Einstellungen
.. _sec_persoenliche_einstellungen:

=========================
Persönliche Einstellungen
=========================

Wenn Sie auf Ihrer persönlichen Seite dem Verweis »Meine Einstellungen« folgen, können
Sie einige Voreinstellungen für das Verhalten von Plone bearbeiten. Dieses Formular ist
zugleich die Bearbeitungsansicht Ihres Profils (siehe Abbildung
:ref:`fig_meine_einstellungen`).

.. _fig_meine_einstellungen:

.. figure:: ../administration/hinweise/images/meine-einstellungen.*
   :width: 100%
   :alt: Die persönlichen Einstellungen, die ein Benutzer verändern kann

   Persönliche Einstellungen

Zu Ihren Profildaten zählen folgende Angaben:

Vor- und Nachname
  Geben Sie hier Ihren vollständigen Namen ein. Mit diesem Namen werden Sie beispielsweise
  in der Anzeige Ihrer Artikel als Verfasser genannt. Beachten Sie, dass dieses Feld derzeit keine Umlaut akzeptiert.

E-Mail
  Geben Sie eine gültige E-Mail-Adresse ein, unter der Sie erreichbar sind. Dieses Feld
  müssen Sie ausfüllen.

Ort
  Die Stadt oder das Land, wo Sie wohnen oder arbeiten.

Sprache
  Ihre Muttersprache.

Biographie
  Ein paar Sätze über Ihre Person und Ihre Arbeit. Mit diesem Text stellen Sie sich in
  Ihrem Profil vor.

Homepage
  Falls Sie eine eigene Website haben, so können Sie sie hier eintragen.

Porträt
  Ein Foto von Ihnen, das in Ihrem Profil angezeigt wird. Wenn Sie ein zu großes Bild
  hochladen, wird es auf eine sinnvolle Größe skaliert. Um das Bild zu löschen, kreuzen
  Sie »Porträt löschen« an.


In den übrigen Feldern können Sie Plones Verhalten beeinflussen:

Texteditor
  Hier wählen Sie aus, mit welchem Texteditor Sie den Haupttext Ihrer Seiten bearbeiten
  wollen. Es gibt zwei Möglichkeiten:

  * FCKeditor ist ein WYSIWYG-Editor. Er ist ein leichter Texteditor und bietet die
    meisten häufig verwendeten Funktionen von Desktop-Editoren wie Microsoft Word und
    OpenOffice. Mit FCKeditor können Sie Text schreiben, formatieren, Tabellen und vieles
    mehr erstellen.
  * Kupu ist ein komfortabler, visueller Editor. Mit ihm können Sie Ihren Text bei der
    Eingabe direkt formatieren und sehen ihn dabei so, wie er später von Plone angezeigt
    wird. Die meisten Anwender werden Kupu bevorzugen.
  * Der normale Formulareditor ist einfach ein mehrzeiliges Eingabefeld und wird von jedem
    Webbrowser zur Verfügung gestellt. Sie können dort neben einfachem Text und HTML
    möglicherweise auch andere Textauszeichnungssprachen eingeben.

    Mehr über den Texteditor finden Sie in Abschnitt :ref:`sec_kupu_vs_fck`.
