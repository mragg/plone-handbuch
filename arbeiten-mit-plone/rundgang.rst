.. _sec_tutorium_rundgang:

========
Rundgang
========

Im ersten Tutorium machen Sie sich mit grundlegenden Tätigkeiten wie dem Anmelden, dem
Abmelden und dem Navigieren durch eine Plone-Website vertraut.

Besuchen Sie als erstes Ihre Website. Die Internetadresse Ihrer Website erfahren Sie von
Ihrem Administrator. Sie sehen nun in Ihrem Webbrowser die Startseite, deren Aufbau in
Kapitel :ref:`sec_aussehen` erläutert wurde.


Die Website vor der Anmeldung
=============================

Solange Sie sich nicht an der Website angemeldet haben, stellt sie sich Ihnen dar wie
allen anderen Besuchern. Erst nach der Anmeldung mit Benutzernamen und Passwort können
Sie als Autor oder Redakteur tätig werden und die dafür notwendigen Informationen und
Bedienelemente sehen.

Machen Sie sich zunächst mit Ihrer Website aus der Sicht eines nicht angemeldeten
Besuchers vertraut.

* Folgen Sie den Verweisen in der Hauptnavigation zur Nachrichten- und Terminübersicht.
* Schauen Sie das Anmeldeformular an, das Sie über das Benutzermenü erreichen.
* Benutzen Sie die Verweise im Seitenkopf, um zur Übersicht, den Anmerkungen zur
  Barrierefreiheit oder dem Kontaktformular zu gelangen.


.. index:: Benutzerzugang
.. _sec_benutz_registr_und:

Benutzerzugang mit Uni-ID
=========================

Um Plone an der Universität Bonn nutzen zu können, benötigen Sie eine Uni-ID, diese wird
durch das Hochschulrechenzentrum verwaltet. Diese Uni-ID muss dann durch den jeweiligen
Plone-Administrator für die Mitarbeit in Plone freigeschaltet werden. Wenn Sie noch keine
Uni-ID haben, nutzen Sie bitte das Online-Antragsformular unter:
http://www.hrz.uni-bonn.de/service/benutzerverwaltung/benutzungsantrag

.. index:: Anmelden
.. _sec_tut_anmelden:

Anmelden
========

Sobald Ihr Benutzerzugang eingerichtet und aktiviert wurde, können Sie sich an der Website
anmelden. Rufen Sie dazu Ihre Webseite auf und hängen an eine beliebige URL Ihres Webauftritts /login an. Sie werden dann automatisch auf die Login-Seite geleitet.

* Geben Sie Ihren Benutzernamen und Ihr Passwort in die Eingabefelder ein.
* Betätigen Sie die Schaltfläche »Anmelden«.

Ist die Anmeldung erfolgreich, gelangen Sie in beiden Fällen wieder auf die Seite, die
Sie vorher besucht hatten.

Fehler beim Anmelden
====================

Haben Sie sich bei der Eingabe des Benutzernamens oder des Passworts vertan, teilt Ihnen
Plone mit, dass die Anmeldung fehlgeschlagen ist. Wiederholen Sie den Anmeldeversuch mit
richtigen Anmeldedaten.

Die Website nach der Anmeldung
==============================

Sie befinden sich nach der Anmeldung zwar wieder optisch auf derselben Seite wie vorher,
aber einige Dinge haben sich geändert (siehe Abbildung :ref:`fig_plonebase_logged_in`).
Die Bearbeitung erfolgt über ene SSL-geschützte Verbindung.

.. _fig_plonebase_logged_in:

.. figure::
   ./images/plonebase-logged-in.*
   :width: 100%
   :alt: GUI nach der Anmeldung

   Plone-Oberfläche nach der Anmeldung


Statusmeldung
=============

Oberhalb des Inhaltsbereichs sehen Sie eine gelblich hinterlegte Statusmeldung. Sie
informiert Sie darüber, dass Sie nun angemeldet sind. Verlassen Sie die Seite, so
verschwindet die Meldung. Im Laufe Ihrer Arbeit wird es häufig vorkommen, dass Sie von
Plone eine solche Statusmeldung erhalten. Sie werden damit über den Erfolg oder
Misserfolg der jeweils unmittelbar zuvor ausgeführten Aktion unterrichtet.

Benutzermenü
============

Das Benutzermenü bietet Ihnen nun einige personalisierte Einträge.

* Der erste Eintrag ist Ihr Name. Dabei handelt es sich um einen Verweis auf Ihre
  persönliche Seite.
* Ganz rechts finden Sie einen Menüpunkt, mit dem Sie sich von der Website abmelden
  können.


.. index:: Abmelden
.. _sec_persoenliche_seite:

Abmelden
========

An dieser Stelle beenden wir unseren ersten Rundgang durch die Website. Melden Sie sich
am Ende jeder Arbeitssitzung von der Website ab.

* Betätigen Sie die Schaltfläche »Abmelden« im Benutzermenü.


Sie erhalten daraufhin von Plone eine Bestätigung, dass Sie sich abgemeldet haben. Das
Benutzermenü sieht nun wieder genauso aus wie vor der Anmeldung, und die Website stellt
sich Ihnen so dar, wie sie für alle Besucher aussieht.
