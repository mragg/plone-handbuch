
==================
Umgang mit Ordnern
==================

Dieses Tutorium beschäftigt sich mit den Besonderheiten von Ordnern gegenüber anderen
Artikeln.

Ordner anlegen
==============

Legen Sie einen Ordner an, indem Sie ähnlich vorgehen wie im vorigen Tutorium:

* Wechseln Sie in den Ordner »Veranstaltungen«.
* Wählen Sie aus dem Hinzufügemenü den Artikeltyp »Ordner« aus (siehe Abbildung
  :ref:`fig_add_menu_ordner`).

.. _fig_add_menu_ordner:

.. figure::
    ./images/add-menu-ordner.*
    :width: 100%
    :alt: Das Hinzufügemenü mit dem Eintrag Ordner

    Anlegen eines Ordners

Sie gelangen in das Bearbeitungsformular des neuen Ordners, das zwei Felder enthält:
Titel und Beschreibung. Diese Felder haben bei Ordnern die gleiche Bedeutung wie bei
Seiten.

* Bearbeiten Sie den neu angelegten Ordner. Im Rest dieses Tutoriums gehen wir davon aus,
  dass Sie ihm den Titel »Kochseminar« geben.

Nach dem Speichern gelangen Sie zur Anzeige des Ordners. Die Statusmeldung informiert
Sie darüber, dass die Änderungen gespeichert wurden. Die Anzeige des Ordners informiert
Sie darüber, dass der Ordner noch leer ist.

.. index:: Inhalte

Inhalt eines Ordners
====================

Die Inhaltsansicht eines leeren Ordners unterscheidet sich nicht wesentlich von seiner
Anzeige (siehe Abbildung :ref:`fig_folder_empty`).

.. _fig_folder_empty:

.. figure::
    ./images/folder-empty.*
    :width: 100%
    :alt: Inhaltsansicht eines leeren Ordners

    Inhaltsansicht eines neu angelegten Ordners

* Rufen Sie die Inhaltsansicht des Ordners »Kochseminar« auf.
* Legen Sie im Ordner »Kochseminar« eine Seite an, wie Sie es im vorigen Tutorium gelernt
  haben.
* Begeben Sie sich wieder zur Inhaltsansicht des Ordners »Kochseminar«.

Dort hat Plone jetzt eine Tabelle erzeugt, deren bisher einziger Eintrag die gerade
angelegte Seite ist (siehe Abbildung :ref:`fig_folder_with_object`).


.. _fig_folder_with_object:

.. figure::
    ./images/folder-with-object.*
    :width: 100%
    :alt: Inhaltsansicht eines Ordners

    Inhaltsansicht eines Ordners mit einem Artikel

* Legen Sie weitere Artikel im Ordner »Kochseminar« an. Beobachten Sie dabei stets die
  Ansichten »Inhalte« und »Anzeigen« des Ordners.

Sowohl in der Inhaltsansicht als auch in der von Plone erzeugten Anzeige des Ordners
kommen neue Einträge am unteren Ende hinzu. Die bestehenden Einträge behalten dabei ihre
Reihenfolge bei (siehe Abbildung :ref:`fig_folder_order`).

.. _fig_folder_order:

.. figure::
    ./images/folder-order.*
    :width: 100%
    :alt: Anzeige eines Ordners, in dem sich mehrere Artikel befinden

    Anzeige eines Ordners mit mehreren Artikeln

Ändern Sie nun die Reihenfolge der Einträge. Die Inhaltsansicht des Ordners enthält dazu
in der Tabellenspalte »Reihenfolge« für jeden Artikel ein Symbol, das aus zwei
Doppelpunkten besteht.

* Wechseln Sie in die Inhaltsansicht des Ordners »Kochseminar«.
* Gehen Sie mit dem Mauszeiger über die Doppelpunkte in der Tabelle. Je nach den
  Einstellungen Ihres Betriebssystems verwandelt sich der Mauspfeil dabei möglicherweise
  so, dass er Anfassen oder Bewegen symbolisiert.
* Greifen Sie nun mit einem Mausklick einen Artikel, und verschieben Sie ihn in der Liste
  bei gedrückter Maustaste nach oben oder unten. Wenn Sie die Maustaste loslassen, wird
  der Artikel an der entsprechenden Stelle einsortiert.
* Wechseln Sie zwischendurch in die Anzeige des Ordners, und vergewissern Sie sich, dass
  auch dort die Reihenfolge geändert wurde.

Falls Javascript an Ihrem Rechner nicht aktiviert ist, erscheinen statt der Doppelpunkte
in jeder Tabellenzeile Pfeile, mit denen Sie den jeweiligen Artikel mit seinem Vorgänger
oder Nachfolger vertauschen können.

.. index:: Darstellung

Ordneranzeige
=============

Plone kennt verschiedene Vorlagen für die Anzeige eines Ordners.

* Begeben Sie sich zum Ordner »Kochseminar«.
* Öffnen Sie das Menü »Darstellung« und wählen Sie »Tabelle« aus (siehe Abbildung
  :ref:`fig_ansicht`).

.. _fig_ansicht:

.. figure::
    ./images/ansicht-tabelle.*
    :width: 100%
    :alt: Das Menüs Darstellung mit dem Eintrag Tabelle

    Darstellungen der Ordneranzeige

Die Anzeige des Ordners enthält jetzt anstelle der Liste eine Tabelle mit Einträgen für
jeden Artikel des Ordners.

* Probieren Sie nacheinander die anderen Ansichten aus. Die Albenansicht kommt nur dann
  zur Geltung, wenn Sie Bilder im Ordner erstellt haben.

Plone kann anstelle von Übersichtslisten oder -tabellen auch einen Artikel aus dem Ordner
als Anzeige verwenden.

* Öffnen Sie das Darstellungsmenü und wählen Sie den Punkt »Artikel aus dem Ordner...«.
* Sie gelangen zu einem Formular, das alle im Ordner befindlichen Artikel mit Ausnahme
  der Unterordner auflistet (siehe Abbildung :ref:`fig_standardseite`).

.. _fig_standardseite:

.. figure::
    ./images/standardseite.*
    :width: 100%
    :alt: Formular zur Auswahl eines Artikels als Ordneranzeige

    Auswahl eines Artikels als Ordneranzeige

* Kreuzen Sie den gewünschten Artikel an und speichern Sie das Formular.
* Plone leitet Sie nun zur Anzeige des Ordners »Kochseminar« weiter. Sie sehen dort
  anstelle einer Übersichtsliste oder -tabelle den gewählten Artikel.
* Wechseln Sie zur Inhaltsansicht. Sie sehen dort, dass der gewählte Artikel durch
  Fettschrift hervorgehoben ist.

.. index:: Blog
.. _sec_blog:

Blogview
========

Mit dem »Blogview« kann die Darstellung von kompletten Artikeln untereinander auf der
Ebene eines Ordners bzw. einer Kollektion erreicht werden.

Das Produkt muss einmalig vom Administrator der Plone-Instanz installiert werden.

Hierbei werden alle Elemente des Artikels dargestellt, oberhalb der Überschrift wird das
Freischaltdatum ausgegeben. Hiermit kann auf der Ebene eines Ordners dafür gesorgt
werden, dass alle Artikel innerhalb des Ordners in voller Länge untereinander ausgegeben
werden. Im klassischen Plone gab es bisher nur die Möglichkeit Artikel "anzureißen" und
dann über einen Link auf die Volldarstellung zu gelangen.

Die gleiche Funktionalität ist auch in Kollektionen integriert, d.h. es können über
Kollektionen Artikel automatisch zusammengestellt werden, die in der Ausgabe jeweils in
voller Länge angezeigt werden.

Die Überschrift ist jeweils auf die Einzeldarstellung des Artikels verlinkt, hier kann
dann im Sinne einer Blogfunktionalität z.B. mit Kommentaren etc. weitergearbeitet werden.

.. _fig_blogview_kollekltion:

.. figure:: ./images/blog-view-kollektion.*
   :width: 60%
   :alt: Blogfullview in der Kollektion

   Blogfullview in der Kollektion

Aktivieren der Darstellung im Ordner/Kollektion:

So sieht beispielhaft eine Ansicht auf einen Ordner aus:

.. _fig_blogview_ordner:

.. figure:: ./images/blog-view-ordner.*
   :width: 60%
   :alt: Blogfullview im Ordner

   Darstellung von zwei Artikeln in voller Länge


.. index:: Slideshow, Galerie
.. _sec_slideshow:

Slideshow
=========
Unter "Darstellung" können Sie für einen Ordner eine Slideshow-Ansicht aktivieren.
Um eine Galerie anzulegen, folgen Sie dieser kleinen Anleitung.
* Legen Sie einen Ordner an
* Laden Sie in diesen Ordner die Bilder hinein (hinzufügen --> Bild)
* Gehen Sie im Ordner auf "Inhalte" und aktivieren Sie unter "Darstellung" die Option "Galerieansicht"

Portlet-Spalten ausschalten; die Galerie benötigt klassischerweise mehr Platz an den inneren Contentbereich, im Regelfall empfielhlt es sich die Portletspalten auszuschalten
Die Galerie kommt mit einigen Voreinstellungen her, so dass nach diesen Vorbereitungen die Galerie schon voll funktionsfähig ist:

Im Standard sind folgende Optionen aktiviert:
- der Player startet sofort (Zeitabstand 3 Sekunden)
- unterhalb der großen Darstellung Thumbnails der gesamten Galerie
- vor- und zurück-Pfeile zum Blättern in der Galerie

Ein Klick auf den Info-Button blendet ein schmales Fenster ein, mit den Angaben zur Datei und einem Download-Link
Wenn an dem Ordner unter "Einstellungen" die Urheber-Informationen hinterlegt sind, werden diese zentral an der Galerie ausgegeben (Wichtig z.B. für Copyright-Hinweise etc.)
"Galerie-Einstellungen": Diese Option finden Sie an jedem Ordner, dessen Darstellung auf "Galerie" gesetzt ist. Er ermöglicht eine Vielzahl an weiteren Optionen, im "Normalfall" benötigen Sie diese nicht - Spezialisten können sich Ihre Galerie aber damit weiter anpassen
Die Galerie ist automatisch "responsiv", d.h. die Galerie-Darstellung optimiert sich selbsttätig auf mobilen Endgeräten und versucht den Platz dort optimal auszunutzen und wenig "Scrollerei" zu erzwingen

Galerie als Portlet
-------------------
Neben der großen Darstellung kann eine Galerie auch als Portlet wiederverwendet werden. Nötig ist dazu erst, eine Galerie an einem Ordner zu erstellen (s. die Vorarbeiten oben). Sie finden dann unter "Portlet hinzufügen" die neue Auswahl "Plone True Gallery Portlet". Hier müssen Sie den Ordner angeben (suchen Sie einfach nach dem Ordnernamen). Sie können dann weitere Optionen festlegen - auch hier gilt: der Standard ist schon ziemlich gut, probieren Sie es ersteinmal damit.
Zugeordnete Kategorie(n): Arbeiten mit Plone

Ein Beispiel finden Sie in der rechten Portletspalte dieser Seite.

.. index:: Kopieren, Verschieben

Artikel kopieren und verschieben
================================

Plone erlaubt Ihnen nicht nur, Inhalte anzulegen und zu löschen. Sie können Artikel und
Ordner auch von einem Ort in der Website an einen anderen verschieben oder kopieren.

Erzeugen Sie dazu im Ordner »Kochseminar« einen Unterordner und kopieren Sie einen
Artikel aus dem Ordner »Kochseminar« dort hinein.

* Legen Sie im Ordner »Kochseminar« einen Ordner an.
* Rufen Sie anschließend im Ordner »Kochseminar« den Artikel auf, den Sie kopieren
  möchten.
* Öffnen Sie das Menü »Aktionen« und wählen Sie den Eintrag »Kopieren« aus.
* Wechseln Sie in den Unterordner.
* Fügen Sie eine Kopie des ausgewählten Artikels dort ein, indem Sie den Eintrag
  »Einfügen« im Aktionsmenü auswählen.

Die Anzeige des Unterordners enthält nun einen neuen Eintrag. Vergewissern Sie sich, dass
sich am Inhalt des Ordners »Kochseminar« nichts geändert hat.

Verschieben Sie als nächstes einen Artikel aus dem Ordner »Kochseminar« in den
Unterordner. Dabei gehen Sie ähnlich vor wie beim Kopieren.

* Wechseln Sie in den Ordner »Kochseminar« und rufen Sie den Artikel auf, den Sie
  verschieben möchten.
* Öffnen Sie das Menü »Aktionen« und wählen Sie den Eintrag »Ausschneiden« aus.
* Wechseln Sie in den Unterordner.
* Fügen Sie den ausgewählten Artikel dort ein, indem Sie den Eintrag »Einfügen« im
  Aktionsmenü benutzen.

Sie werden bemerken, dass der Artikel nicht gleich beim Ausschneiden aus dem Ordner
»Kochseminar« verschwindet. Erst beim Einfügen wird er an seinem Ursprungsort tatsächlich
gelöscht. Kontrollieren Sie nach dem Verschieben den Inhalt des Ordners »Kochseminar«.

Sie können Artikel nicht nur einzeln mit Hilfe der Einträge im Aktionsmenü kopieren und
verschieben. In der Inhaltsansicht eines Ordners können Sie mehrere Artikel markieren,
um sie gemeinsam zu kopieren oder zu verschieben.

* Wechseln Sie in die Inhaltsansicht des Ordners »Kochseminar«.
* Markieren Sie in der Spalte ganz links einige Artikel, die Sie kopieren möchten.
* Betätigen Sie die Schaltfläche »Kopieren« unterhalb der Übersichtstabelle. Achten Sie
  auf die Statusmeldung.
* Wechseln Sie nun in den Unterordner.
* Betätigen Sie die Schaltfläche »Einfügen«. Lesen Sie die Statusmeldung und schauen Sie
  nach, wie sich die Übersichtsliste verändert hat.

Wenn Sie einen Ordner kopieren oder verschieben, werden alle Artikel, die sich in dem
Ordner befinden, mit dem Ordner verschoben oder kopiert.

* Legen Sie im Ordner »Kochseminar« einen weiteren Ordner an.
* Wechseln Sie in die Inhaltsansicht des Ordners »Kochseminar«.
* Markieren Sie den ersten Unterordner zum Kopieren.
* Wechseln Sie in den neuen Unterordner.
* Fügen Sie den markierten Ordner ein.

Der Unterordner mit seinem gesamten Inhalt befindet sich nun auch in dem zweiten
Unterordner.

* Vergewissern Sie sich, dass beide Ordner den gleichen Inhalt besitzen.

.. index:: Löschen

Ordner löschen
==============

Ordner werden wie alle anderen Artikel mit der Aktion »Löschen« im Aktionsmenü gelöscht.
Beachten Sie, dass beim Löschen eines Ordners auch die darin enthaltenen Artikel
gelöscht werden.
