.. index:: Bibliographieordner
.. _sec_bibliographie:


=============
Bibliographie
=============

Sie können in Ihrem Plone einen Ordner zur Verwaltung von Literatur bzw. Literaturreferenzen erzeugen. Innerhalb eines Plone-Auftritts kann es beliebig viele Bibliographie-Ordner geben.
Zum Anlegen eines solchen Ordners gehen Sie bitte auf "Hinzufügen" und dann auf "Bibliographie Ordner".

Innerhalb eines Bibliographieordners können Sie dann verschiedene Referenzen hinzufügen. Je nach Eintrag werden unterschiedliche Felder zur Erfassung angeboten, unten als Beispiel der Eintrag für ein Buch.

.. _fig_bibliographie_buch_hinzufuegen_1:

.. figure::
   ./images/bibliographie-buch-hinzufuegen-1.png
   :width: 100%
   :alt: Buch hinzufuegen

   Buch in der Bibliographieordner hinzufügen

Export und Import
-----------------
Sie können die Einträge eines Bibliographieordners zur Nutzung in externen Programmen exportieren. Als Formate stehen Ihnen hierbei Bibtex, Endnote, Ris und XML zur Verfügung.
Sie finden die Links für Export und Import unter "Anzeigen" in der Fußzeile der Seite.

.. _fig_bibliographie_export:

.. figure::
   ./images/bibliographie-exportr.png
   :width: 100%
   :alt: Bibliographie exportieren

   Bibliographie exportieren

Die gleichen Formate stehen Ihnen auch als Import zur Verfügung.

.. _fig_bibliographie_import:

.. figure::
   ./images/bibliographie-import.png
   :width: 100%
   :alt: Bibliographie importieren

   Bibliographie importieren

.. _fig_bibliographie_inhalte:

.. figure::
   ./images/bibliographie-inhalte.png
   :width: 100%
   :alt: Bibliographie-Inhalte

   Bibliographie Inhalte
