.. index:: Bild (Artikeltyp)
.. _sec_bild:

====
Bild
====

Der Artikeltyp »Bild« dient dazu, einzelne Bilder in einer Website zu verwalten. Sie
können in Ordner einsortiert werden und besitzen Eigenschaften und Metadaten.  Will man
in einer Seite eine Illustration verwenden, so muss sie als Artikel vom Typ »Bild« in der
Website liegen. Als einzige Ausnahme werden die Titelbilder von Nachrichten in der
Bearbeitungsansicht der Nachricht direkt hochgeladen. Sie können nicht in anderen
Artikeln verwendet werden.

Die Anzeige eines Bildes besteht aus dem Bild zusammen mit dem Titel, der Beschreibung
und einer Größenangabe (siehe Abbildung :ref:`fig_bild`).

.. _fig_bild:

.. figure::
   ./images/bild.png
   :width: 80%
   :alt: Screenshot der Anzeige eines Bildes

   Anzeige eines Bildes

.. index:: Vollbilddarstellung

Das Bild selbst ist dabei ein Verweis auf seine Vollbilddarstellung, die nur das Bild und
einen Verweis zurück zur Anzeigeansicht enthält. Sie können also zwischen der Anzeige und
der Vollbilddarstellung hin- und herspringen.

Die Bearbeitungsansicht eines Bildes enthält neben den allgemeinen Feldern wie Titel und
Beschreibung ein Formularfeld, mit dem Sie eine Bilddatei von Ihrem Rechner hochladen
können.

.. index:: Bild bearbeiten

Um ein Bild zu verändern, öffnen Sie es im Allgemeinen in einem Bildbearbeitungsprogramm
an Ihrem Arbeitsplatz. Anschließend laden Sie das bearbeitete Bild auf die Website hoch
und ersetzen damit die vorhandene Fassung. Einige einfache Änderungen an einem Bild
können Sie auch direkt auf der Website machen: Bilder besitzen die Ansicht
»Transformieren«, in der Sie ein hochgeladenes Bild direkt spiegeln und drehen können.
Wählen Sie dazu die gewünschte Transformation aus und betätigen Sie die Schaltfläche
»Ausführen« (siehe Abbildung :ref:`fig_bild_transformieren`).

.. _fig_bild_transformieren:

.. figure::
   ./images/bild-transformieren.png
   :width: 80%
   :alt: Transformationsansicht eines Bildes

   Transformationsansicht eines Bildes

Folgende Änderungen kann Plone an Ihrem Bild durchführen:

* horizontal und vertikal spiegeln
* im und gegen den Uhrzeigersinn um 90° drehen
* um 180° drehen


Ansicht zurücksetzen
--------------------

::

  /url/zum/objekt/selectViewTemplate?templateId=TemplateId

(z.B. ``atct_topic_view`` für den default Kollagen View) Das hilft z.B. wenn beim Aufruf
einer Kollage nur noch ``"kupu-captioned_image Error"`` kommt).


.. index:: Animation
.. _sec_animationen:

Animationen
===========

Plone bietet keine Möglichkeit Flash-Elemente in die Seiten zu integrieren, aber das
heißt nicht, dass es gar keine Möglichkeit gibt, die Seite etwas "beweglicher" zu machen.

Es gibt die Möglichkeit, Animationen auf einer Plone Seite einzubinden, und zwar ein
animiertes Bild. Die Animation muss im Voraus mit entsprechender Software vorbereitet
sein. Es gibt eine Menge von Programmen (auch kostenlose) mit deren Hilfe Sie relativ
einfach aus mehreren Einzelbildern ein animiertes Bild im GIF-Format erzeugen können.
Dieses Bild können Sie dann als ganz normales Bild in die Seite integrieren oder als
Kopfgrafik eines Ordners einfügen.

Die Art der Animation lässt sich in Abhängigkeit von Software bestimmen (die Mächtigste
Instrumente: Photoshop, FireWorks). GIF-Animationen haben gleiche Eigenschaften, wie ganz
normale Bilder und können auch verlinkt werden.
