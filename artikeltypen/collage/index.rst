.. index:: Collage
.. _sec_collage:

=======
Collage
=======


Übersicht
=========

Die Collage ist eine Erweiterung, die es ermöglicht, neue oder bereits existierende
Artikel an einer Stelle zusammenzufassen. Zum Beispiel könnte ein Text und daneben dazu
passende Kollektionen angezeigt werden. Um bei Collagen den Überblick zu behalten,
erklären wir hier ihre Funktionalität und Gestaltung. Einige Optionen, die Sie bei der
Arbeit mit Collagen finden, sind nicht auf allen Plone Portalen der Uni Bonn aktiviert.
Für konkrete Fragen und weitere Informationen wenden Sie sich direkt an den Plone Support.

Eine Collage finden Sie nach der Installation der Erweiterung unter »hinzufügen«. An
dieser Stelle werden nur »Titel« und »Beschreibung« der Collage eingegeben. Erst nach dem
Speichern folgt das Hinzufügen der Artikel über den Reiter »manage page«. Eine Collage ist
in erster Linie in Zeilen unterteilt. Diese sind immer von einem dünnen roten Rahmen
eingefasst. Eine neue Zeile enthält immer schon einen blauen Rahmen, den man aber noch in
bis zu drei Spalten unterteilen kann. Das Hinzufügen von Artikeln geschieht immer
innerhalb einer Spalte.

.. _fig_collage_leer:

.. figure:: images/collage-leer.png
   :width: 100%
   :alt: Collage hinzufügen

   Collage hinzufügen.

Allgemein findet sich die Funktion »copy« in jedem Teilbereich einer Collage. Sie fügt
eine Zeile, Spalte oder einen konkreten Artikel in den Zwischenspeicher und bietet einem
dann die Möglichkeit via »paste«, das kopierte Objekt an anderer Stelle der Collage ein
zweites Mal einzusetzen. Dazu stehen jedem Bereich einer Collage die Optionen löschen,
bearbeiten und syndizieren zur Verfügung, die ja schon aus anderen Bereichen von Plone
bekannt sind.

Ansicht von Zeilen
==================

large left / right:
    Diese beiden Optionen bieten die Möglichkeit, die jeweilige Seite etwas breiter
    anzuzeigen als die andere. Die Wahl wirkt sich bei drei Spalten nur auf die äußeren,
    nicht auf die mittlere Spalte aus.

automatic:
    Die Breite aller sichtbaren Spalten ist gleich.

Ansicht von Spalten
===================

portlets top / bottom:
    Mit dieser Option können am oberen bzw. unteren Ende der Spalte Portlets eingefügt
    werden. Hierbei stehen Ihnen alle gewohnten Portlets zur Verfügung.

standard:
    Die Standardansicht listet in der gewählten Spalte alle hinzugefügten Artikel
    untereinander auf.

.. _fig_collage_zusammengesetzt(2):

.. figure:: images/collage-zusammengesetzt(2).png
   :width: 80%
   :alt: Collage. Ansicht

   Ansicht von Zeilen und Spalten.

Aus der Sicht eines Besuchers der Seite ist das Zusammengesetzte später nicht mehr erkennbar:

.. _fig_collage_zusammengesetzt_anzeigen:

.. figure:: images/collage-zusammengesetzt-anzeigen.png
   :width: 80%
   :alt: Collage. Text.

   Benutzeransicht


Vorhandenen Artikel hinzufügen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Diese Option öffnet eine Liste mit allen verfügbaren Artikeln Ihres Plone-Portals, die
sich zu einer Collage hinzufügen lassen. Hilfreich ist hierbei das Suchfeld, da die Liste
standardmäßig unsortiert angezeigt wird und somit meist zu viele Objekte zur Auswahl
stehen. Bei Klick auf das gewünschte Objekt oder den kleinen Pfeil rechts daneben wird es
in der entsprechenden Spalte eingefügt. Je nachdem, wie viele Spalten genutzt werden, kann
es hier zur Verschiebung der Zeilenumbrüche kommen.

Neuen Artikel hinzufügen
~~~~~~~~~~~~~~~~~~~~~~~~~

Diese Option ermöglicht das Erstellen eines neuen Artikels. Hierbei stehen die selben
Objekte zur Auswahl wie sie auch unter »hinzufügen« gelistet sind. Abgelegt werden diese
neu angelegten Artikel dann innerhalb der Collage. Sie finden sie also nicht im
übergeordneten Ordner und können sie nur über die Collage wieder bearbeiten oder
entfernen.

Spalten
~~~~~~~

Teilt die aktuelle Spalte in zwei Teile, die sich wiederum wie eine Spalte verhalten.

Ansicht von Artikeln
--------------------

featured:
    Unterlegt den Inhalt des betroffenen Artikels mit dem Hellgrau,
    das auch als Hintergrundfarbe der Portletspalten dient.

.. _fig_collage_darstellung_hervorheben:

.. figure:: images/collage-darstellung-hervorheben.*
   :width: 80%
   :alt: Collage. Featured.

   Hervorheben-Ansicht.

portlet:
    Hierdurch wird der gewählte Artikel im Layout einer Portletpage angezeigt.

.. _fig_collage_darstellung_portlet:

.. figure:: images/collage-darstellung-portlet.png
   :width: 80%
   :alt: Collage. Portlet.

   Portlet-Ansicht.

text:
    Diese Art der Ansicht zeigt in der Collage nur den Inhalt des Feldes »Haupttext« des
    gewählten Artikels. »Titel« und »Beschreibung« werden ausgeblendet.

.. _fig_collage_darstellung_text:

.. figure:: images/collage-darstellung-text.png
   :width: 80%
   :alt: Collage. Text

   Text-Ansicht.

standard:
    Stellt den Inhalt des eingefügten Artikels so dar, wie er im Original angezeigt wird.
