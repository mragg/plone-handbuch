.. index:: Datei
.. _sec_datei:

=====
Datei
=====

Mit Hilfe eines Artikels vom Typ »Datei« können Sie eine beliebige Datei auf Ihrer
Website veröffentlichen und zum Herunterladen anbieten. Typ und Inhalt, innere Struktur
und Speicherformat der Datei unterliegen keinen Einschränkungen.

Der Nachteil dabei ist, dass Plone nur wenige Dateitypen kennt und in alle anderen
Dateien mangels Wissens über Struktur und Format nicht hineinschauen kann. Aus diesem
Grund funktioniert die Volltextsuche nur für PDF-, Office- und einfache Textdateien, aber
nicht für andere Formate, die möglicherweise auch Textpassagen enthalten können. Um die
Möglichkeiten von Plone ausschöpfen zu können, sollten Sie daher Texte als »Seite« oder
»Nachricht« und Bilder als »Bild« ablegen, wenngleich es technisch möglich ist, sie im
Artikeltyp »Datei« zu speichern.

Die Anzeige einer Datei enthält neben ihrem Titel und der Beschreibung den Dateinamen,
einen Verweis zum Herunterladen der Datei sowie Angaben zu ihrer Größe und der Art
(MIME-Typ) der Daten (siehe Abbildung :ref:`fig_datei`).

.. _fig_datei:

.. figure:: ./images/datei.png
   :width: 100%
   :alt: Anzeige einer Datei

   Anzeige einer Datei

Eine Ausnahme bilden Textdateien, beispielsweise einfacher Text, Quellcode von Programmen
oder HTML-Text. Den Inhalt dieser Dateien kann Plone anzeigen. Es erkennt Textdateien
daran, dass ihr MIME-Typ mit ``text`` beginnt. Normalerweise sorgt Ihr Webbrowser dafür,
dass beim Hochladen einer Datei der richtige MIME-Typ mitgesendet wird.

Je nach Typ der Daten und Konfiguration Ihres Webbrowsers wird beim Herunterladen die
Datei entweder mit einem Hilfsprogramm im Webbrowser selbst dargestellt oder auf Ihrem
Rechner gespeichert. Häufig ist beides möglich; dann fragt der Webbrowser nach, was Sie
mit der Datei tun möchten.

Ähnlich wie bei Bildern laden Sie Dateien in der Bearbeitungsansicht hoch. Wenn bereits
eine hochgeladene Datei vorhanden ist, sehen Sie auch hier den Namen, die Größe und die
Art der Datei. Sie können die vorhandene Datei behalten oder durch eine andere ersetzen.
Um eine Datei erstmalig hochzuladen oder zu ersetzen, wählen Sie mit der Schaltfläche
»Durchsuchen« die gewünschte Datei auf Ihrem Rechner aus und speichern Ihre Veränderungen.

Wenn der Bearbeiten-Knopf nicht mehr mag, können Sie dies auch über die URL probieren: ::

 /url/zum/objekt/base_edit


Dateien austauschen
===================

Folgendes Problem: Sie haben eine Datei zum Download abgelegt und irgendwann soll diese
Datei durch eine neuere Version ersetzt werden.

Das normale Vorgehen sieht so aus:

* Sie löschen den Link aus der Seite
* Sie wechseln auf »inhalte« und löschen den entsprechenden Eintrag aus der Liste
* Wenn Sie Pech haben, wird der Link nicht vollständig aus Ihrer Seite entfernt und der
  Eintrag lässt sich hier nicht löschen. Dann müssen Sie den Quelltext Ihrer Seite
  manuell bearbeiten.
* Sie fügen die neue Datei über »Einfügen« → »Datei« wieder ein und vergeben den
  gleichen Namen
* Sie fügen den Link in Ihre Seite wieder ein

Mit folgendem Trick können Sie sich viel Arbeit sparen:

* Rufen Sie über Ihre Seite die Datei auf, die sie ersetzen möchten
* Ergänzen Sie am Ende des in der Adressleiste angezeigten Links ``/edit`` und drücken
  Sie Enter (Alternativ können Sie auch unter »inhalte« die Datei auswählen, die Sie
  ersetzen möchten)
* Im erscheinenden Formular können Sie nun unter gleichem Titel eine neue Datei auswählen.


.. index: MassLoader, Upload, Import (Ordner)
.. _sec_massloader_nutzen:

MassLoader
==========

Es ist manchmal nötig mehrere PDFs oder Bilddateien auf der Seite hinzufügen. Dabei jede
einzelne Datei hochzuladen ist sehr zeitaufwendig. Sie können es per »WebDAV«_ realisieren,
aber das ist nicht unbedingt die optimale Lösung, weil man dafür zuerst ein WebDAV-Client
auf Ihrem Rechner installieren und konfigurieren muss. Außerdem dient WebDAV mehr für den
Zweck regelmäßigen Uploads, wo Sie z.B. monatlich alle PDFs durch neue ersetzen müssen.
Für einmaligen Upload ist »MassLoader« optimal. »MassLoader« ist ein Zusatzprodukt,
das als eine Alternative zu »WebDAV« gelten kann, um eine Menge von Dateien sehr
schnell und einfach auf den Server hochzuladen.

Die Nutzung des MassLoader ist unvergleichlich einfach. In jedem Plone-Artikel, wo
MassLoader aktiviert ist, erscheint ein Reiter »import«, was automatisch
MassLoader aktiviert/tätigt. Angenommen MassLoader ist unter Konfigurationen für den
Ordner aktiviert. Gehen Sie in einem beliebigen Ordner auf den Reiter »import«, drücken
Sie den Button »Auswählen«, wählen Sie auf Ihrem Rechner das ZIP-Archiv aus und
schließlich drücken Sie den Button »save« fürs Hochladen.

.. _fig_massloader_anzeigen:

.. figure:: ./images/massloader-importformular.png
   :width: 100%
   :alt: massloader

   MassLoader


**Hinweis:**
Vergessen Sie nicht die Einstellungen des neuen Ordners zu prüfen: »von Navigation
ausschließen«, »Status«, »Erstellungsdatum«, »Urheber« usw. Achten Sie auch darauf,
dass MassLoader nur mit den ZIP-Archivdateien funktioniert, kein .rar oder 7z!

Wenn man mit Mac arbeitet wird bei der Verwendung des MassLoader ein zusätzlicher Ordner
erstellt. Dieser sollte wieder gelöscht werden.


.. _»WebDAV«: :ref:`netzlaufwerk`_
