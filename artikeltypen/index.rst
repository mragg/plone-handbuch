.. |seite| image:: ../images/document_icon.png
.. |nachricht| image:: ../images/newsitem_icon.png
.. |datei| image:: ../images/file_icon.png
.. |link| image:: ../images/link_icon.png
.. |ordner| image:: ../images/folder_icon.png
.. |kollektion| image:: ../images/topic_icon.png
.. |bild| image:: ../images/image_icon.png
.. |termin| image:: ../images/event_icon.png
.. |formulare| image:: ../images/plone-images/form_icon.png
.. |collage| image:: ../images/plone-images/collage_icon.png
.. |events| image:: ../images/plone-images/eventfolder_icon.png
.. |bibliographie| image:: ../images/plone-images/bibliography_icon.png
.. |pressrelease| image:: ../images/plone-images/folder_icon.png
.. |youtube| image:: ../images/plone-images/youtube-icon.png


.. index:: Seite, Nachricht, Termin, Datei, Link (Artikeltyp), Ordner, Kollektion
.. _sec_artikeltypen:


============
Artikeltypen
============

In diesem Kapitel werden Ihnen die Eigenschaften aller Artikeltypen von Plone vorgestellt.
Dabei gehen wir zuerst auf die Gemeinsamkeiten ein und widmen uns danach jedem einzelnen
Artikeltyp.  Tabelle :ref:`tab_artikeltypen` gibt einen Überblick über die verfügbaren
Typen und ihre Symbole.

.. _tab_artikeltypen: 

.. table:: Artikeltypen

   +-----------------+---------------------+
   | Symbol          | Artikeltyp          |
   +=================+=====================+
   | |seite|         | Seite               |
   +-----------------+---------------------+
   | |nachricht|     | Nachricht           |
   +-----------------+---------------------+
   | |termin|        | Termin              |
   +-----------------+---------------------+
   | |bild|          | Bild                |
   +-----------------+---------------------+
   | |datei|         | Datei               |
   +-----------------+---------------------+
   | |link|          | Link                |
   +-----------------+---------------------+
   | |ordner|        | Ordner              |
   +-----------------+---------------------+
   | |kollektion|    | Kollektion          |
   +-----------------+---------------------+
   | |formulare|     | Formulare           |
   +-----------------+---------------------+
   | |collage|       | Collage             |
   +-----------------+---------------------+
   | |events|        | Veranstaltungen     |
   +-----------------+---------------------+
   | |bibliographie| | Bibliographie       |
   +-----------------+---------------------+
   | |pressrelease|  | Pressemitteilungen  |
   +-----------------+---------------------+
   | |youtube|       | YouTube Video       |
   +-----------------+---------------------+

.. toctree::
   :maxdepth: 1

   gemeinsamkeiten.rst
   seite.rst
   ordner.rst
   bild.rst
   datei.rst
   nachricht.rst
   termin.rst
   kollektion.rst
   ploneformgen/index.rst
   collage/index.rst
   link.rst
   veanstaltungen/index.rst
   bibliographie/index.rst
   pressemitteilungen/index.rst
   youtube/index.rst
   