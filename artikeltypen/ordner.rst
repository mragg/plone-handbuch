.. index:: Ordner, Darstellung, Inhalte
.. _sec_ordner:

======
Ordner
======

Geben Sie Ihrer Website eine inhaltliche Struktur, indem Sie verwandte Artikel in Ordnern
zusammenfassen. Ihre Leser können dann Zusammenhänge zwischen Artikeln besser erkennen
und gesuchte Inhalte schneller auffinden.

Anders als die meisten Artikeltypen besitzen Ordner keinen eigenen redaktionellen Inhalt,
sondern enthalten eine Reihe anderer Artikel. Auf diese Weise teilen Ordner und
Unterordner den möglicherweise umfangreichen Inhalt der Website in überschaubare
inhaltliche Einheiten auf.

Die Anzeigeansicht eines Ordners zeigt entweder den Inhalt des Ordners an oder verwendet
einen einzelnen Artikel aus dem Ordner gewissermaßen als Titelblatt. Für die Darstellung
ihres Inhalts besitzen Ordner mehrere Vorlagen, aus denen Sie im Menü »Darstellung«
wählen können, das sich in dem grünen Rahmen um die Anzeige befindet:

* Kurzfassung
* Tabelle
* Album
* Liste

.. _fig_ordner:

.. figure::
   ./images/folder-order.*
   :width: 100%
   :alt: Der Inhalt eines Ordners als Liste

   Der Inhalt eines Ordners als Liste

Die Darstellung als Liste (siehe Abbildung :ref:`fig_ordner`) enthält zu jedem Eintrag
den Titel, die Beschreibung, einen Verweis auf das Profil des Erstellers und das Datum der
letzten Änderung. Der Titel ist ein Verweis zum jeweiligen Artikel. Eine Ausnahme bilden
Einträge für Termine: bei ihnen werden anstelle des Änderungsdatums Ort und Zeitraum des
Termins angezeigt.

Artikel im Revisionsstatus »privat« werden in der Regel ausgeblendet. Sie sehen nur die
privaten Artikel, die Ihnen gehören oder sich in Ihrem persönlichen Ordner befinden.

Wollen Sie für die Ordneranzeige einen Artikel aus dem Ordner benutzen, wählen Sie im
Darstellungsmenü den Punkt »Artikel aus dem Ordner.... Sie gelangen so zu einem Formular,
in dem Sie einen Artikel aus dem Ordner markieren können. In der Anzeigeansicht des
Ordners erscheint nun keine Übersicht über seinen Inhalt, sondern der ausgewählte Artikel.

Plone kann für Ordner RSS-Feeds erzeugen. Dieser Vorgang wird Syndizierung genannt. Jeder
Ordner besitzt eine weitere Ansicht, in der Sie das Syndizierungsverhalten steuern können.

In der Bearbeitungsansicht eines Ordners gibt es im Teilformular »Einstellungen« die
Option »Vor- und Zurückblättern einschalten« (siehe Abbildung :ref:`fig_ordner_bearbeiten`).

.. _fig_ordner_bearbeiten:

.. figure:: ./images/ordner-bearbeiten-einstellungen.*
   :width: 100%
   :alt: Teilformular Einstellungen

   Das Teilformular »Einstellungen« bei Ordnern

Wenn diese Option eingeschaltet ist und sich in einem Ordner mehrere Artikel befinden, so
erscheinen in deren Anzeige Verweise zum jeweils vorherigen und nächsten Artikel (siehe
Abbildung :ref:`fig_vor_zurueck_navi`).

.. _fig_vor_zurueck_navi:

.. figure:: ./images/vor-zurueck-navi.*
   :width: 100%
   :alt: Die Navigationselemente, um zum nächsten oder vorherigen
   	 Artikel zu springen

   Vor- und Zurückblättern zwischen Artikeln

Damit lässt sich beispielsweise ein langer Text in kleinere Abschnitte gliedern, durch
die der Leser bequem blättern kann.

.. _sec_inhaltsansicht_ordner:

Inhaltsansicht
==============

Wenn Sie den Inhalt eines Ordners verwalten dürfen, erhalten Sie Zugriff auf seine
Inhaltsansicht (siehe Abbildung :ref:`fig_ordnerinhalt`).

.. _fig_ordnerinhalt:

.. figure:: ./images/ordnerinhalt.*
   :width: 100%
   :alt: Inhaltsansicht eines Ordners

   Inhaltsansicht eines Ordners

Sie erreichen diese Ansicht über den Reiter »Inhalte«.

Die Inhaltsansicht eines Ordners zeigt eine Tabelle aller im Ordner befindlichen Artikel
mit ihren wichtigsten Eigenschaften. In dieser Ansicht können Sie die Artikel unter
anderem kopieren, verschieben und löschen. Haben Sie einen Artikel aus dem Ordner als
Ordneranzeige ausgewählt, so ist er durch Fettschrift hervorgehoben.

Artikel liegen in einem Ordner in der Reihenfolge, in der sie hinzugefügt wurden, und
werden so auch in den Ordneransichten und der Navigation angezeigt. Sie können die
Reihenfolge jedoch verändern, indem Sie einzelne Artikel an dem Symbol »::« in der Spalte
»Reihenfolge« ganz rechts mit der Maus »anfassen« und auf- oder abwärts verschieben. Falls
Sie Javascript ausgeschaltet haben, finden Sie in der Spalte stattdessen Pfeilsymbole vor
(siehe Abbildung :ref:`fig_umordnen`).

.. _fig_umordnen:

.. figure:: ./images/umordnen.*
   :width: 100%
   :alt: So kann man die Reihenfolge von Artikeln verändern

   Artikel in einem Ordner umordnen: mit Javascript (links) und ohne
   (rechts)}


.. index:: Sortieren, Sortierparametern
.. _sec_inhalte_sortieren:

Weitere Darstellungsformen
==========================
* Slideshow

Sie können einen Ordner, der Bilder enthält in einen Slideshow-Modus setzen. Sie finden die Beschreibung im Kapitel :ref:`sec_slideshow`

* Blogview

Sie können einen Ordner auch in einem Blogview darstellen lassen. Sie finden die Beschreibung im Kapitel :ref:`sec_blog`


Navigation Root
===============

Sie können an einem Ordner die Option "Navigation Root" aktivieren. Sie können damit aus einem Unterordner quasi einen nach außen eigenständigen Webauftritt machen.
Ein "Navigation Root" ist in Verbindung mit einer separaten URL ein mächtiges Werkzeug. Gerade im Hinblick auf die Auffindbarkeit durch Suchmaschinen sind einige Dinge zu beachten.
Bitte lesen Sie unbedingt die Hinweise im Kapitel: :ref:`sec_navigation_root`


Ordnerinhalte sortieren
=======================

Gerade in Ordnern mit vielen Objekten möchte man u.U. eine andere Sortierung als im
Standard. In Plone 3.3 sind hierzu leider keine "Knöpfe" verfügbar, es gibt aber
Sortierparameter, die über die URL übergeben werden können und mit denen man auf der
Inhalteansicht eine Umsortierung erzeugen kann.

Verfügbare Parameter
--------------------

* Verändert: ``modified``
* Titel: ``sortable_title``
* Status: ``review_state``
* ID: ``id``
* Freigabe Datum: ``effective``
* Alle Objekte auf einer Seite: ``show_all=true``

**Beispiel**

Die Parameter werden an den URL-Aufruf angehängt: ::

  http://www3.uni-bonn.de/veranstaltungen/@@folder_contents?sort_on=modified

Mit diesem Aufruf werden alle Objekte im Ordner »Veranstaltungen« nach der letzten
Veränderung sortiert.

Mit & können auch mehrere Parameter kombiniert werden: ::

  http://www3.uni-bonn.de/veranstaltungen/@@folder_contents?sort_on=modified&sort_order=ascending

Das können Sie auf jeden beliebigen Plone-Ordner anwenden. Melden Sie sich wie gewohnt in
Plone an, navigieren Sie zu dem Ordner und hängen Sie an die URL den Part ab
``@@folder_contents`` an die URL in Ihrer URL-Leiste im Browser.


Tipp bei häufiger Verwendung
----------------------------

Wenn Sie die Aufrufe häufig benötigen können Sie die Anwendung noch verfeinern und sich
beispielsweise ein Portlet dauerhaft einblenden, das die Links zur Sortierung enthält:

1. Gruppenportlet erzeugen: Damit das Portlet nur von Redakteuren mit den entsprechenden
   Rechten gesehen werden kann und nicht von Besuchern gesehen wird, erzeugen wir uns ein
   »Gruppenportlet«. Nötig sind hierzu Administrationsrechte in Plone.
2. Unter »Konfiguration« → »Benutzer und Gruppe« → »Gruppen« die gewünschte Gruppe
   anklicken
3. Dann auf »Gruppenportlets« Alle Portlets, die Sie hier anlegen sind nur für eingeloggte
   Benutzer der jeweiligen Gruppe sichtbar

   .. _fig_gruppenpotlets:

   .. figure::
      ./images/gruppenportlets.*
      :width: 100%
      :alt: Gruppenportlet

      Gruppenportlet

4. Legen Sie ein Textportlet an
5. Im Fließtext des Textportlets schreiben Sie eine kurze Beschreibung des Links,
   markieren diesen und gehen auf die "Weltkugel", um einen externen Link einzufügen
6. Hier geben Sie nun keinen echten externen Link ein, sondern schlicht die
   Sortierparameter ab dem @@ (s. Screenshot).

   .. _fig_sortieren_im_ornder_link:
   .. figure::
      ./images/sortieren-im-ordner-link.*
      :width: 80%
      :alt: Link mit Sortierparametern

      Link mit Sortierparametern

7. Das Textportlet wird Ihnen nun in allen Ordnern eingeblendet, klicken Sie dort auf den
   Link wird die Sortierung auf den aktuellen Ordner angewendet
