.. index:: Datagrid-Feld (Formular)
.. _sec_datagrid_feld:

=============
Datagrid-Feld
=============

.. note:: ReDi: Text ist noch nicht fertig! Ich schreibe zuerst kleine Abschnitte und Notizen.

Datagrid-Feld ist kein Bestandteil des Plone-FormGens und muss extra einmalig installiert
werden. Die Installation ist der Installation den Zusatzprodukten ähnlich (siehe Kapitel
:ref:`sec_zusatzprodukte`).

Hinzufügen
==========

.. _fig_formular_datagrid_feld_hinzufuegen:

.. figure:: ./images/formular-datagrid-feld-hinzufuegen.*
   :width: 80%
   :alt: Datagrid-Feld hinzufuegen

   Datagrid-Feld hinzufügen

So wird es im Formular aussehen:

.. _fig_formular_mit_datagrid_feld:

.. figure:: ./images/formular-mit-datagrid-feld.*
   :width: 80%
   :alt: Formular mit Datagrid-Feld

   Formular mit Datagrid-Feld

Ansicht ohne weiter Formularelemente:

.. _fig_formular_datagrid_feld_anzeigen:

.. figure:: ./images/formular-datagrid-feld-anzeigen.*
   :width: 80%
   :alt: Datagrid-Feld

   Datagrid-Feld


* ähnlich zu Zeichenkette-Feld (ohne Validator!)
* jede Eingabe ist eine neue Zeile, die entsprechend getränt gespeichert werden
* flexible Eingabe
* unbekannte Anzahl von der einzelnen Eingabe

Im Datenspeicher-Adapter wird folgendes Format verwendet: ::

 [{'feld-name': 'eingegebenen Wert', 'positionsnummer': 'zeilennummer'}
  {'feld-name': 'eingegebenen Wert', 'positionsnummer': 'zeilennummer'}]

Somit kann man nicht nur die Eingaben eines Benutzers voneinander trennen, sondern auch
die Reihenfolge der Eingaben erkennen. Es ist empfehlenswert die Eingabefelder mit gleichen
Namen zu benennen, wie der Datagrid-Feld selbst. So können Sie besseren Überblick in
gespeicherten Daten erschaffen (siehe Abbildung :ref:`fig_formular_datagrid_feld_datenbank`).

.. _fig_formular_datagrid_feld_datenbank:

.. figure:: ./images/formular-datagrid-feld-datenbank.*
   :width: 80%
   :alt: Datagrid-Feld hinzufuegen

   Gespeicherte Eingaben

* Nummerierung mit ungeraden Zahlen
* letzter Eintrag ist immer leer! Bug?!
* Einsatzbeispiele?! (Sieht schön und scheint nutzlos zu sein!)

Formularbeispiel von meiner Projektgruppe: http://sewiki.iai.uni-bonn.de/teaching/projectgroups/ast/2015/start












