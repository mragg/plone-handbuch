.. index:: Datei-Feld (Formular)
.. _sec_datei_feld:

==========
Datei-Feld
==========

Wenn mit Hilfe des Formulars eine Datei, zum Beispiel ein Bild oder ein PDF-Dokument)
hochgeladen werden soll, kommt das Datei-Feld (Abbildung :ref:`fig_datei_feld`) zum
Einsatz.

.. _fig_datei_feld:

.. figure:: ./images/formular-dateifeld-hinzufuegen.*
   :width: 80%
   :alt: Hinzufügeformular für ein Datei-Feld

   Datei-Feld

Maximale Größe für die Datenübertragung zum Server (in Mb)
   Die Größe der Datei kann begrenzt werden. Tragen Sie in dieses Feld die maximale Größe
   in Megabyte ein. 


.. index:: Dateiuploads (Formular)


Dateiupload auf dem Server speichern
====================================

Dateiuploads aus Formularen werden normalerweise nur als Attachment verschickt, werden
serverseitig aber nicht gespeichert - wir zeigen wie Sie dies mit wenig Aufwand ändern
können. Interessant ist die z.B. für ein »Call for Paper«, Sie sammeln zentral in Plone
alle Dokumente ein.

Legen Sie zuerst im Ordner in dem das Formular liegt einen neuen Ordner an und nennen
diesen »uploads«. Wichtig ist, dass Sie diesen Ordner im Status auf »privat« stehen
lassen und nicht veröffentlichen. So ist sichergestellt, dass nur Sie Zugriff auf die
hochgeladenen Dokumente haben.

Legen Sie als nächstes innerhalb des Formulars einen »Adapter für ein eigenes Script« an.

Damit der Dateiupload erfolgreich funktioniert, muss die Proxy-Rolle "Verwalten" ausgewählt werden.

In das Feld Scrip-Rumpf kommen folgende Code-Zeilen, die Zeilen sind kommentiert - bei
Bedarf können Sie hier auch eigene Anpassungen vornehmen. ::

 # * Anpassung nötig
 formFile = request.form['mein-dokument_file']
 # mein-dokument_file -> mein-dokument ist dabei die ID (Kurzname) des Dateifeldes
 # (unbedingt _file nicht löschen oder ändern)

 #"meine-uploads" ist die ID des Ordners der paralel zum FormGen Ordner liegt -
 # hier werden die hochgeladenen Dateien abgelegt
 uploadFolder = getattr(ploneformgen.getParentNode(),
                       'meine-uploads')

 #Hier kann der Dateiname angegeben werden.
 formFileName = 'File-'+ str(DateTime().strftime('%Y-%m-%d-%H-%M'))
 #Hier wird das Plone Objekt erzeugt.
 uploadFolder.invokeFactory('File',id=formFileName, file=formFile)

.. _fig_skriptadapter:

.. figure:: ./images/skriptadapter.*
   :width: 70%
   :alt: Skript-Adapter bei Dateiupload
   
   Skript-Adapter bei Dateiupload
 
Ab jetzt werden alle hochgeladenen Dateien in den Ordner »uploads« gespeichert. Die
Dateien erhalten eindeutige Dateinamen nach dem Muster Uploadname-Zeitstempel - so sind
auch Uploads mit identischen Dateinamen ohne Konflikte möglich.

Wir hatten in einem Institut den Fall, dass als führende ID beim Anlegen der Datei in
Plone das Feld Matrikelnummer verwendet wurde, damit brach der Vorgang ab, wenn das
Formular unter einer Matrikelnummer mehrfach abgeschickt werden sollte. Nachfolgender
Codeblock ergänzt die Funktion um eine Überprüfung ob die Datei im Upload-Ordner schon
existiert: ::

 picture = request.form['foto_file']
 matNummer = request.form['matrikelnummer']
 #"uploads" ist die ID des Ordners der paralel zum FormGen Ordner liegt - hier werden die Dateien  abgelegt
 uploadFolder = getattr(ploneformgen.getParentNode(),'uploads')
 #Hier kann der Dateiname angegeben werden.
 formFileName = matNummer
 #Hier wird das Plone Objekt erzeugt.
 nid = formFileName
 c = 0
 while hasattr(uploadFolder,nid):
 c += 1
 nid = formFileName + '-' + str(c)
 uploadFolder.invokeFactory('File',id=nid,file=picture)

Dateiupload auf Dateiendungen einschränken
------------------------------------------

In unserem Formulartool haben Sie die Möglichkeit mit wenigen Klicks ein »Uploadformular«
hinzuzufügen. Wir zeigen Ihnen, wie Sie z.B. nur PDF-Dateien erlauben. 

Voraussetzung ist, dass Sie ein existierendes Formular haben und in diesem ein
»File-Upload-Feld« (»datei-feld«) eingefügt ist.

Gehen Sie nun innerhalb des Feldes auf »Bearbeiten« und dann auf den mittleren Reiter
»Overrides«. Im mittleren Bereich finden Sie eine Zeile für den »Personalisierten
Validator«, im Standard steht dort ::

 python:false

löschen Sie bitte die Voreinstellung und tragen dort nachfolgende Zeile ein: ::

 python:test(request.form['feldname_file'].filename[-3:]=='pdf', False, 'Nur PDF-Dateien - kein: '+request.form['feldname_file'].filename[-3:])

Jetzt sind nur noch wenige Einstellungen notwendig:

* ändern Sie feldname durch den Namen Ihres Uploadfeldes (die eindeutige ID, d.h. der
  Name des Feldes wie er auch in der URL auftaucht), :guilabel:`_file` bleibt stehen!
* feldname an beiden Stellen entsprechend ändern
* Wenn Sie wollen, können Sie noch den Fehlertext anpassen, der beim Uploadversuch einer
  anderen Datei ausgegeben werden soll. Momentan steht dort: Nur PDF-Dateien, kein
  (hier wird dann die versuchte Dateiendung aufgeführt).


