.. _sec_formular_ordner_elemente:

===========================
Elemente im Formular-Ordner
===========================

Wenn Sie sich in einem Formular-Ordner befinden und das Menü :guilabel:`Hinzufügen`
aufklappen, sehen Sie welche Elemente Sie in einem Ordner hinzufügen können. Zur Verfügung
stehen in der Reihenfolge des Menüs folgende Objekte:

* :ref:`sec_bild_feld`
* :ref:`sec_checkbox_field`
* :ref:`sec_adapter_fuer_eigenes_skript`
* :ref:`sec_datum_und_zeit_feld`
* :ref:`sec_decimal_number_field`
* :ref:`sec_fieldset_ordner`
* :ref:`sec_datei_feld`
* :ref:`sec_titel_feld`
* :ref:`sec_zeilen_feld`
* :ref:`sec_mail_adapter`
* :ref:`sec_mehrfach_auswahl_feld`
* :ref:`sec_passwort_feld`
* :ref:`sec_bewertungs_feld`
* :ref:`sec_formatierter_text`
* :ref:`sec_formatierbarer_text_feld`
* :ref:`sec_daten_speicher_adapter`
* :ref:`sec_pfg_seite`
* :ref:`sec_auswahl_feld`
* :ref:`sec_kurztext_feld`
* :ref:`sec_text_feld`
* :ref:`sec_danke_seite`
* :ref:`sec_whole_number_field`

Bei einigen dieser Objekte handelt es sich um Formularfelder, die zur Aufnahme von
Information dienen, bei anderen um Adapter, die dafür sorgen, dass die Informationen
weiterverarbeitet werden. Außerdem stehen Ihnen Gliederungselemente zur Strukturierung des
Formulars zur Verfügung sowie der Artikeltyp »Seite«.
