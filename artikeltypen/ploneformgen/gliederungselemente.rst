.. index:: Gliederungselemente
.. _sec_formular_gliederung:

===================
Gliederungselemente
===================

Längere Formulare können schnell unübersichtlich werden. Gliederungselemente helfen dabei,
Formulare attraktiv und übersichtlich zu gestalten.

.. _sec_fieldset_ordner:

Fieldset Ordner
===============

Mit Hilfe eines Fieldset-Ordners kann man sachlich zusammengehörige Formularfelder optisch
zusammenfassen (siehe Abbildung :ref:`fig_fieldset_ordner_anzeige`).

.. _fig_fieldset_ordner:

.. figure:: ./images/formularsatz-hinzufuegen.*
   :width: 80%
   :alt: Hinzufügeformular für ein Fieldset

   Fieldset-Ordner

Die Bearbeitungsansicht eines Fieldset-Ordners (siehe Abbildung
:ref:`fig_fieldset_ordner`) ist fast so aufgebaut, wie die Bearbeitungsansicht eines
normalen Ordners. Es gibt jedoch folgende Besonderheiten.

Show Title as Legend
   Der Titel des Ordners kann als Legende im Formular angezeigt werden.

Fieldset Help
   Dieser Text wird als Hilfe-Text im Formularsatz angezeigt.

Im Ordner selbst müssen dann die Felder angelegt werden, die als Teil des Fieldsets
angezeigt werden sollen. 

.. _fig_fieldset_ordner_anzeige:

.. figure:: ./images/formularsatz-anzeigen.*
   :width: 80%
   :alt: Anzeige eines Fieldset-Ordners mit Objekten

   Fieldset-Ordner Anzeige

.. _sec_titel_feld:


Titel-Feld
==========

Bei längeren Formularen ist eine Unterteilung in einzelne Abschnitte sinnvoll, die mit
einer Überschrift versehen werden. Dazu dient das Titel-Feld.

.. _fig_titel_feld:

.. figure:: ./images/formular-bezeichnung-hinzufuegen.*
   :width: 80%
   :alt: Hinzufügeformular eines Titel-Feldes

   Titel-Feld

Mit dem Titelfeld wird lediglich eine Überschrift (Titel) erzeugt, unter der ein
erklärender Text (Hilfe-Feld) stehen kann (siehe Abbildung :ref:`fig_titel_feld`).

.. _sec_formatierter_text:

Formatierter Text
=================

Um in einem Formular formatierten Text zum Beispiel mit Bildern, Links und Tabellen
einzufügen, nutzen Sie dieses Feld (siehe Abbildung :ref:`fig_formatierter_text`). 

.. _fig_formatierter_text:

.. figure:: ./images/formular-bezeichnungsfeld-hinzufuegen.*
   :width: 80%
   :alt: Hinzufügeformular für fomratierten Text

   Formatierter Text

Der Titel des Feldes wird im Formular selbst nicht angezeigt. Er dient lediglich zur
Identifizierung des Feldes im Formular-Ordner. Schreiben Sie Ihren Text einfach in das
Textfeld und speichern Sie das Feld. 

.. _sec_bild_feld:

Bild
====

Wenn für das Verständnis des Formulars eine Grafik oder ein Bild notwendig ist, kann es
mit dem Bild-Feld (Abbildung :ref:`fig_bild_feld`) eingebunden werden.

.. _fig_bild_feld:

.. figure:: ./images/formular-bild-hinzufuegen.*
   :width: 80%
   :alt: Hinzufügeformular für ein Bild-Feld

   Bild-Feld

Betätigen Sie die Schaltfläche :guilabel:`Choose File`, um auf Ihrem Rechner ein Bild
auszusuchen und klicken Sie anschließend auf :guilabel:`Speichern`. 

