.. index:: Import (Formular), Export (Formular)
.. _sec_import_export:

================================
Import und Export von Formularen
================================

Formulare, die mit PloneFormGen erstellt wurden, lassen sich exportieren und an anderer
Stelle importieren. Wer häufig ähnlich strukturierte Formulare erstellen muss, wird diese
Funktion zu schätzen wissen. Man kann viel Zeit sparen, wenn man längere Formulare nicht
komplett neu aufbauen muss, sondern importieren und anpassen kann.

Im Aktionsmenü eines Formular-Ordners finden Sie für den Im- und Export zwei Aktionen vor
(siehe Abbildung :ref:`fig_import_export_aktion`).

.. _fig_import_export_aktion:

.. figure:: ./images/formular-import-export.*
   :width: 80%
   :alt: Aktionsmenü eines Formular-Ordners

   Aktionsmenü eines Formular-Ordners

Mit dem Eintrag :guilabel:`Export` können Sie das gesamte Formular exportieren. Plone
erzeugt eine komprimiertes `Tar`-Archiv, das Sie auf Ihrem lokalen Rechner abspeichern
können (siehe Abbildung :ref:`fig_export_formular`).

.. _fig_export_formular:

.. figure:: ./images/formular-export.*
   :width: 70%
   :alt: Speichern der Export-Datei

   Speichern der Export-Datei

Um ein Formular zu importieren, müssen Sie zunächst einen Formular-Ordner anlegen.
Betätigen Sie dann im Aktionsmenü des neuen Formular-Ordners den Eintrag
:guilabel:`Import`. Sie werden daraufhin aufgefordert, eine Import-Datei zu bestimmen
(siehe Abbildung :ref:`fig_import_formular`) Wählen Sie eine ein vorher exportiertes
Formular als Import-Datei aus und betätigen Sie die Schaltfläche :guilabel:`import`.

.. _fig_import_formular:

.. figure:: ./images/formular-import.*
   :width: 80%
   :alt: Import eines Formular-Ordners

   Import eines Formulars

Der gesamte Inhalt des exportierten Formulars wird in das neue Formular geladen. Wenn Sie
die Option :guilabel:`Remove Existing Form Items` vor dem Importieren ausgewählt haben,
werden die Beispielobjekte des neu erzeugten Formular-Ordners gelöscht.

Sie können nun die importierten Formular-Elemente anpassen.

Probleme mit kopierten Formularen
=================================

Ab und an kommt es vor allem bei Mailadaptern in Formularen zu Problemen.

Die Probleme entstehen, wenn Sie vorhandene Formulare als Vorlage benutzen und davon
Kopien entstehen. Hierbei werden manchmal die Mailadapter »intern« verdoppelt. Dies
resultiert dann darin, dass Mails doppelt/mehrfach verschickt werden, obwohl in der
Formularansicht der Mailadapter korrekt nur einmal angezeigt wird.

Als Workaround reicht es, wenn Sie einmal im Gesamtformular auf »Bearbeiten« gehen und
diese Seite mit »Speichern« beenden. PloneFormGen korrigiert dann intern die Dopplungen.
Sie sollten umkopierte Formulare daher auf jeden Fall auf komplette Funktionalität
überprüfen.
