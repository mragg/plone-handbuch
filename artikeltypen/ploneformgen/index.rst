.. index:: PloneFormGen, Online - Formulare
.. _sec_ploneformgen:

=========
Formulare
=========

PloneFormGen ist ein universaler und leicht bedienbarer Formulargenerator für Plone.

.. toctree::
   :maxdepth: 1
   
   formular-ordner.rst
   felder-adapter.rst
   adapter.rst
   danke-seite.rst
   zahlenfelder.rst
   textfelder.rst
   auswahlfelder.rst
   dateifeld.rst
   datagrid-feld.rst
   gliederungselemente.rst
   seite.rst
   import-export.rst
   tagungsanmeldung.rst
   validator.rst
