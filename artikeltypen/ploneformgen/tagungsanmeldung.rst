.. _sec_tagung_anmeldung:

================================================
Anwendungsbeispiel: Tagungsanmeldung realisieren
================================================

.. _sec_anmeldungsformular:

Mit dem Formularordner können Sie einfach und effektiv eine Tagungsanmeldung
organisieren. Nachfolgender Artikel beschreibt einige sinnvolle Zusatzeinstellungen.

.. index:: Anmeldungsformular

Anmeldungsformular
==================

Im ersten Schritt erstellen Sie wie üblich ein Formular (Hinzufügen → Formularordner).
Nehmen Sie hier alle Felder auf, die Sie für die Tagungsanmeldung benötigen
(E-Mailadresse, Vor- Nachname, Adressdaten, Workshopauswahl etcpp).

.. _sec_anmeldung_in_excel_speichern:

Anmeldungen serverseitig in Excel-Liste speichern
=================================================

Für die spätere Bearbeitung der Anmeldungen (Anschreiben, Teilnahmelisten etc) gibt es
eine einfache Möglichkeit: Sie können die eingehenden Anmeldungen serverseitig sammeln und
speichern lassen. Plone stellt Ihnen alle Daten in einer in Excel/OpenOffice
weiterverarbeitbaren Datei zur Verfügung. Sie benötigen dazu den "Datenspeicher-Adapter"
(Hinzufügen → Datenspeicher-Adapter). Geben Sie diesem einen Namen, setzen Sie den Haken
bei "Spaltennamen einbeziehen"). Sie finden die Speicherung dann unter dem vergebenen
Namen unter "Inhalte" in Ihrem Formular. Die Liste ist für normale Besucher nicht
einsehbar. Sobald Formulardaten eingehen, können Sie diese dort herunterladen (im Standard
als csv-Datei).

Tipp: Einige Excel-Versionen erkennen die Kommata nicht direkt als Feldtrenner. Einfacher
Workaround ist, wenn Sie die Dateiendung von .csv auf .txt ändern und die Datei dann in
Excel öffnen. Sie können dann "Komma" als Spaltenbegrenzer eintragen.

.. _sec_automatisierte_email:

Automatisiert Bestätigungs-Email verschicken
============================================

Gerade bei Tagungsanmeldungen ist es oft gewünscht, dass nach der Anmeldung eine
automatische E-Mailbestätigung verschickt wird. Am einfachsten können Sie dies einrichten,
in dem Sie einen zusätzlichen Mail-Adapter hinzufügen. Legen Sie den Mailadapter an und
tragen Sie im ersten Schritt nur den Namen und keinen Empfänger ein. Gehen Sie dann auf
"Bearbeiten" und dort in das Register "Adressing". Stellen Sie im ersten Punkt "Empfänger
herauslesen aus" die Option ""Ihre E-Mail-Adresse" ein. Unter "Message" sollten Sie noch
die Betreffzeile anpassen (im Standard wird dort "Formularversand" vorgeschlagen, das
sollten Sie anpassen. Sie haben dort auch die Möglichkeit weiteren Text für die Mail
einzustellen, leider gibt es in diesen Feldern keine Möglichkeit,
Formatierungen/Zeilenumbrüche zu verwenden, daher bieten sich die Felder dort nur für sehr
simple Zusatzinformationen an. Größere Formatierungsmöglichkeiten haben Sie im Register
"Template".

Tipp: Bevor Sie Änderungen im "Template" machen, speichern Sie die Feldeinstellungen dort
und sichern Sie am besten in einem externen Texteditor die Zwischenschritte. Kleine
Syntax-Fehler führen unter Umständen zu Fehlkonfigurationen des gesamten Formulars.

.. _sec_mail_template:

Kurze Erklärung zur Nutzung des Mail-Templates
----------------------------------------------

* der <dl> </dl> Block führt die ausgefüllten Felder auf. Diesen Block sollten Sie so
  erhalten
* Sie können folgende Zeilen löschen und dort Ihren eigenen Text unterbringen: ::

  <p tal:content="here/getBody_pre | nothing" />
  <p tal:content="here/getBody_post | nothing" />
  <pre tal:content="here/getBody_footer | nothing" />

Innerhalb des Mailtemplates können Sie mit normalen HTML-Befehlen arbeiten. Achten Sie
bitte unbedingt darauf, alle Umlaute/Sonderzeichen korrekt in HTML aufzuführen!

.. sec_dateiupload:

Dateiupload / Call for Paper
============================

Sie haben die Möglichkeit, bei der Anmeldung einen Dateiupload einzurichten. Die Datei
wird dabei nur an die E-Mail angehängt und nicht serverseitig gespeichert! Klicken Sie im
Formularordner dazu unter Hinzufügen auf "Datei-Feld". Sie können unter den Optionen eine
Größenbeschränkung für Uploads einstellen.