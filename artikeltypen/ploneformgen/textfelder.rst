.. index:: Textfelder (Formular)

==========
Textfelder
==========

.. _sec_kurztext_feld:

Kurztext-Feld
=============

Das Kurztext-Feld (Abbildung :ref:`fig_kurztext_feld`) dient der Eingabe einer einzelnen
Textzeile. Mit Hilfe verschiedener Validatoren kann vor dem Absenden des Formulars
überprüft werden, ob bestimmte Bedingungen eingehalten wurden.

.. _fig_kurztext_feld:

.. figure:: ./images/formular-zeihenkettenfeld-hinzuhuegen.*
   :width: 60%
   :alt: Hinzufügeformular für ein Feld zur Eingabe einer Textzeile

   Feld für Textzeile

Die Einstellungsmöglichkeiten »Titel«, »Hilfe-Feld«, »zwingend benötigt«, »Standard«,
»Maximale Länge« und »Größe« wurden bereits am Beispiel des Whole Number Fields erklärt
(siehe :ref:`sec_whole_number_field`). 

Das Kurztext-Feld kann zudem versteckt werden.

Mit Hilfe des Validators (siehe Abbildung :ref:`fig_validator_kurztext`) kann man
sicherstellen, dass die Eingabe bestimmte Bedingungen erfüllt. 

.. _fig_validator_kurztext:

.. figure:: ./images/formular-kuztextfeld-validator-hinzuhuegen.*
   :width: 50%
   :alt: Optionen für den Validator einer Textzeile

   Validator für Textzeile

Überprüft werden kann:

* ob die Eingabe eine E-Mail-Adresse ist
* ob die Eingabe eine Liste von E-Mail-Adressen ist, die durch Kommas getrennt sind
* ob die Eingabe nur druckbare Zeichen enthält
* ob die Eingabe eine korrekt formatierte Webadresse (URL) ist
* ob es sich bei der Eingabe um eine gültige US-Telefonnummer handelt
* ob es sich bei der Eingabe um eine gültige internationale Telefonnummer handelt
* ob die Eingabe eine gültige Postleitzahl ist
* ob die Eingabe frei von Links ist

Wenn die Bedingung des Validators nicht erfüllt ist, erhält der Benutzer eine Warnung mit
dem Hinweis, wie er eine korrekte Eingabe machen kann.

.. _sec_text_feld:

Text-Feld
=========

Beim Text-Feld (Abbildung :ref:`fig_text_feld`) kann ein mehrzeiliger, nicht formatierter
Text eingegeben werden.

.. _fig_text_feld:

.. figure:: ./images/formular-einfachestextfeld-hinzufuegen.*
   :width: 100%
   :alt: Hinzufügeformular für ein Feld zur Eingabe von Text

   Feld für Text

Die meisten Einstellungsmöglichkeiten wurden bereits in :ref:`sec_whole_number_field`
erklärt. Das Text-Feld kann wie das Kurztext-Feld versteckt werden.

Zusätzlich gibt es folgenden Validator:

Reject Text with Links
   Wenn diese Option ausgewählt wird, darf sich in dem eingegebenen Text kein Link zu
   einer Webseite befinden. Falls dies doch der Fall ist, erhält der Benutzer eine Warnung
   und eine Erläuterung, wie er eine gültige Eingabe vornehmen kann.


.. _sec_formatierbarer_text_feld:

Feld für formatierbaren Text
============================ 

Das Feld für formatierbaren Text dient dazu, dem Benutzer die Möglichkeit zu geben,
längere formatierte Texte einzugeben und dabei den Texteditor Kupu zu nutzen (siehe
Abbildung :ref:`fig_formatierbarer_text_feld` und :ref:`Textfeld mit Texteditor
<fig_formatierbarer_text_feld_anzeige>`).  

.. _fig_formatierbarer_text_feld:

.. figure:: ./images/formular-texteingabefeld-hinzufuegen.*
   :width: 100%
   :alt: Hinzufügeformular für ein Feld für formatierbaren Text

   Feld für formatierbaren Text

Die Konfigurationsmöglichkeiten wurden in :ref:`sec_whole_number_field` erklärt.   

.. _fig_formatierbarer_text_feld_anzeige:

.. figure:: ./images/formular-texteingabefeld-anzeigen.*
   :width: 100%
   :alt: Textfeld mit Bedienelementen des Texteditors

   Textfeld mit Texteditor, wie es sich dem Benutzer darstellt

.. _sec_passwort_feld:

Passwort-Feld
=============

Wenn Passwörter eingegeben werden, müssen sie in der Browseranzeige
maskiert werden. Dies gewährleistet das Passwort-Feld (Abbildung
:ref:`fig_passwort_feld`).

.. _fig_passwort_feld:

.. figure::
   ./images/formular-passwortfeld-hinzufuegen.*
   :width: 100%
   :alt: Hinzufügeformular für ein Passwort-Feld

   Feld für Eingabe eines Passworts

Die Konfigurationsmöglichkeiten entsprechen denen in :ref:`sec_whole_number_field`. 


.. _sec_zeilen_feld:

Zeilen-Feld
===========

Mit dem Zeilen-Feld ist die Eingabe von Textzeilen möglich, die zeilenweise als einzelne
Werte weiterverarbeitet werden. So kann man beispielsweise mit dem Zeilen-Feld eine Liste
von Teilnehmern erzeugen, indem man pro Zeile den Namen eines Teilnehmers eingibt.

.. _fig_zeilen_feld:

.. figure:: ./images/formular-zeilenfeld-hinzufuegen.*
   :width: 100%
   :alt: Hinzufügeformular für ein Zeilen-Feld

   Zeilen-Feld

Die Konfigurationsmöglichkeiten (siehe Abbildung :ref:`fig_zeilen_feld`) wurde bereits
erklärt. Folgendes ist noch zu beachten:

Zeilen
   Hiermit legen Sie fest, wie hoch das Eingabefeld im Formular sein soll. Wenn der
   Benutzer mehr Zeilen einträgt, wird ein Scrollbalken sichtbar.

