.. index:: Validator (Formular)
.. _sec_validator:

=====================================
Formulare: Personalisierter Validator
=====================================

Sie können in Plone Formularfelder in vielfältiger Form validieren. Das fängt an bei der
ganz simplen Option, dass Sie ein Feld zu einem Pflichtfeld erklären.

Gezielter können Sie mit Validatoren einzelne Felder auf z.B. gültige Eingaben
überprüfen. Neben den vorkonfigurierten Validatoren gibt es im Tab »Overrides« das Feld
»Personalisierter Validator«. Wir haben einige Beispiele, wie Sie z.B: auf Datei-Endung
überprüfen können:

Eine weitere Möglichkeit ist es, die Abhängigkeit in Bezug auf andere Felder abzuprüfen.

Beispiel:

Sie haben ein Auswahlfeld in dem Sie den "Status" einer Person erfragen, z.B. Student,
Mitarbeiter, Externer. Je nach Angabe variieren die weiteren Pflichtfelder. So ist es
z.B. nur sinnvoll die Matrikel-Nr bei Studierenden abzufragen.

Sie können den personalisierten Validator via »and« um weitere Optionen erweitern. Ein
Beispiel für einen Valdator bezogen auf den oben skizzierten Fall ist: ::

 python:test(request.form['anfragebereich']!='Student' and
 test(value==''), False, 'Bitte geben Sie Ihre Matrikelnummer an')


Mit "test" leiten Sie die Überprüfung ein, ::

 request.form['anfragebereich']

steuert das Feld »Anfragebereich« an und prüft dort auf die Angabe Student. Mit »and«
wird die nächte Bedingunge angehängt. :guilabel:`test(value="")` prüft die aktuelle
Variable. Im Fehlerfall, d.h. wenn Anfragebereich Student und der aktuelle Wert leer ist,
erfolgt eine Fehlermeldung.