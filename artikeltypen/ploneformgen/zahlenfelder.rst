.. index:: Zahlenfelder (Formular)

============
Zahlenfelder
============

.. _sec_whole_number_field:

Whole Number Field
==================

Das Whole Number Field dient der Eingabe einer ganzen Zahl.

.. _fig_ganzzahl_feld:

.. figure:: ./images/ganzzahl-feld.*
   :width: 80%
   :alt: Hinzufügeformular für ein Feld zur Eingabe einer ganzen Zahl

   Feld für ganze Zahl

Sie können in der Bearbeitungsansicht (siehe Abbildung :ref:`fig_ganzzahl_feld`) folgende
Einstellungen vornehmen:

Titel
   Der Name des Feldes

Hilfe-Feld
   Geben Sie hier falls notwendig einen Text ein, der dem Benutzer erklärt, was er in das
   Feld eintragen soll.

Zwingend benötigt
   Wenn diese Option angewählt wird, muss der Benutzer das Feld ausfüllen. Tut er das
   nicht, erhält er nach dem Absenden des Formulars eine Warnmeldung und wird
   aufgefordert, das Pflichtfeld auszufüllen.

Standard
   Sie können das Formularfeld mit einem Standardwert ausfüllen, den der Benutzer
   überschreiben kann.

Minimaler akzeptierbarer Wert
   Der eingegebene Wert darf nicht kleiner sein als hier angegeben. Bei Unterschreitung
   erfolgt eine Warnmeldung mit Angabe des Minimalwertes.

Maximaler akzeptierbarer Wert
   Der eingegebene Wert darf nicht größer sein als hier angegeben. Bei Überschreitung
   erfolgt eine Warnmeldung mit Angabe des Maximalwertes.

Maximale Länge
   Länge der Zeichenkette, die höchstens eingegeben werden darf. Bei Überschreitung
   erfolgt eine Warnmeldung mit Angabe des Maximalwertes.

Größe
   Größe des Feldes, in das der Benutzer schreiben kann. 


.. _sec_decimal_number_field:

Decimal Number Field
====================

Das Decimal Number Field (Abbildung :ref:`fig_decimal_zahl_feld`) dient der Eingabe einer
Dezimalzahl.

.. _fig_decimal_zahl_feld:

.. figure:: ./images/decimal-zahl-feld.*
   :width: 80%
   :alt: Hinzufügeformular für ein Feld zur Eingabe einer Dezimalzahl

   Feld für Dezimalzahl

Es sind die gleichen Einstellungsmöglichkeiten vorhanden wie bei einem Whole Number Field
(siehe Kapitel :ref:`sec_whole_number_field`). 


