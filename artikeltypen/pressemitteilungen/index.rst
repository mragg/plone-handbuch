.. index:: Pressemitteilungen (Artikeltyp)

====================
"Pressemitteilungen"
====================

Seit das Pressemitteilungs-Tool zum neutralen »Nachrichten-Tool« umgebaut wurde, ist es allen
Einrichtungen möglich, mit dem Pressemitteilungsordner einen Nachrichtenbereich inkl. Archiv aufzubauen wie die
Pressestelle ihn auf der Startseite der Universität aufgebaut hat. Auf jedem Portal kann nur ein Pressemitteilungsordner erstellt werden.

Erstellen und Umbenennen des Ordners "Pressemitteilungen"
=========================================================
Fügen Sie in den Inhalten den "pressrelease folder" hinzu. Wählen Sie einen möglichst eindeutigen Titel und Beschreibung für den Ordner. Beachten Sie:

In der Ansicht des Ordners erscheint bisher standardmäßig der Titel »Pressemitteilungen«.
Der Ordner ist im Standard geschützt, da es ihn nur einmal geben darf. Zum Umbenennen müssen Sie den Ordnerschutz aufheben und danach wieder aktivieren. Folgen Sie bitte nachfolgendem Klickweg:

Diese Einstellungen können nur noch vom Plone-Support vorgenommen werden, bitte wenden Sie sich an plone@uni-bonn.de .

* Einloggen als Administrator
* »Konfiguration«
* »Zope-Management-Oberfläche (ZMI)«
* »portal_types«
* »Press Release Folder« (Achtung: den Eintrag mit »Folder«!)
* Checkbox »implicitly addable« aktivieren + »Save changes«
* Verlassen Sie die »Zope-Management-Oberfläche« und benennen Sie den Ordner um.
* Nehmen Sie nach dem Umbenennen den Haken an der Checkbox »implicitly addable« wieder
  weg (+ save changes)

Hinzufügen und Darstellen von Pressemitteilungen
================================================
Sie können jetzt das Modul nutzen. Hierbei werden die Nachrichten innerhalb
des Ordners erzeugt. Fügen Sie dafür "press release" hinzu.

.. _fig_pressemitteilungen_hinzufuegen_menue:

.. figure:: ./images/pressemitteilungen-hinzufuegen-menue.png
   :width: 35%
   :alt: Hinzufuegen - Menue

   Hinzufuegen - Menü

Sie können hier den Titel (A), die Subüberschrift (B), einen Vorspann (C), das Erscheinungsdatum und den Haupttext (D) festlegen.  In den Inhalten können Sie ein Bild hinzufügen (E).

.. _fig_pressemitteilungen_hinzufuegen:

.. figure:: ./images/pressemitteilungen-hinzufuegen.png
   :width: 100%
   :alt: Pressemitteilungen hinzufuegen

   Pressemitteilungen hinzufuegen

In der speziellen Ansicht kann nach Erscheinungsdatum durch das Archiv gesteuert werden.  Außerdem werden automatisch Portletspalten erzeugt, in denen die Bilder erscheinen.

.. _fig_pressemitteilungen_auflistung:

.. figure:: ./images/pressemitteilungen-auflistung.png
   :width: 100%
   :alt: Auflistung der Pressemitteilungen

   Auflistung der Pressemitteilungen


.. _fig_pressemitteilungen_anzeigen_mit_bild:

.. figure:: ./images/pressemitteilungen-anzeigen-mit-bild.png
   :width: 100%
   :alt: Anzeigen mit Bild

   Anzeigen mit Bild

Auf der Startseite empfiehlt es sich eine »Portlet-Page« anzulegen, dort können Sie dann das »Pressemitteilungs-Portlet« verwenden. Artikel müssen veröffentlicht sein, um angezeigt zu werden.

.. _sec_pressemitteilungen:

Pressemitteilungsportlet
------------------------
Beim Anlegen des Portlets für Pressemitteilungen können Sie die Zahl der Pressemitteilungen bzw. Nachrichten angeben. Diese werden dann in der Portletspalte automatisch gelistet und sind auf die Einzelansicht verlinkt.
