.. index:: Veranstaltungen
.. _sec_veranstaltungen:

===============
Veranstaltungen
===============

Der Veranstaltungsordner kann in den meisten Fällen den Artikeltyp "Termine" ersetzen.
Die Grundidee ist, verschiedene Terminarten in eine sehr einfache und bequeme Umgebung zu
integrieren. Alle Veranstaltungen werden in einem Ordner angelegt/gepflegt und durch das
System automatisch sortiert und dargestellt. Dabei besteht auch die Möglichkeit, dass
Inhaber einer Uni-ID die Veranstaltungen online buchen, lesen Sie dazu weiter unten.


Teilformular Standard
=====================

Nicht alle Teilformulare sind für beide Veranstaltungstypen (normal und buchbar) relevant.

 Unter dem Punkt »Standard« muss der Titel des Veranstaltungsordner eingegeben werden (siehe Abbildung :ref:`fig_veranstaltungsordner_hinzufuegen`).

.. _fig_veranstaltungsordner_hinzufuegen:

.. figure:: ./images/veranstaltungsordner-hinzufuegen.png
   :width: 100%
   :alt: Veranstaltungsordner hinzufuegen

   Veranstaltungsordner hinzufügen


Normale Veranstaltungen hinzufügen und darstellen
=================================================

Fügen Sie im Ordner die Veranstaltungen hinzu. Sie können verschiedene Einstellungen
vornehmen, die vom Administrator voreingestellt werden. Machen Sie
mindestens die Pflichtangaben und veröffentlichen Sie die Veranstaltung. Für die
Darstellung von normalen Veranstaltungen wählen Sie »Darstellung« → »default search
view«. Die Veranstaltungen werden in der Reihenfolge des Startdatums der Veranstaltung
aufgelistet.

.. _fig_veranstaltungen-darstellung-default:

.. figure:: ./images/veranstaltungen-darstellung-default.png
   :width: 100%
   :alt: Darstellung normale Veranstaltung

Jeder Veranstaltungsordner bekommt automatisch einen Standardtext als Beschreibung. Den Beschreibungstext können Sie im Drop-down-Menü unter »Subscribereview« anpassen.

Darstellung normale Veranstaltungen
-----------------------------------
Besucher der Website haben die Möglichkeit einer erweiterten Suchfunktion bei den normalen
Veranstaltungen. Unter "erweiterte Suche einblenden" werden die vier Suchparameter "Suchtext", "Kategorie", "startDate" und "Datumsbereich" aufgerufen (siehe Abbildung :ref:`fig_veranstaltungen-erweiterte-suche`)

.. _fig_veranstaltungen-erweiterte-suche:

.. figure:: ./images/veranstaltungen-erweiterte-suche.png
   :width: 100%
   :alt: Veranstaltungen erweiterte Suche

   Erweiterte Suche

Buchbare Veranstaltungen hinzufügen und darstellen
==================================================

Buchbare Veranstaltungen sind sinnvoll für interne Veranstaltungen wie Workshops. Inhaber einer Uni-ID können sich auf der Website anmelden und die Veranstaltungen buchen. Sie können zwar einen
Kostenbeitrag anzeigen lassen, jedoch nicht abrechnen.

Fügen Sie buchbare Veranstaltungen hinzu und machen Sie sie öffentlich. Beachten Sie, dass
sie mindestens zwei buchbare Veranstaltungen hinzufügen müssen, damit diese buchbar und
reservierbar sind und angezeigt werden. Bearbeiten Sie die Details. Sie müssen eine
Teilnehmerzahl angeben, damit die Veranstaltung buchbar wird. Veranstaltungstyp und
Zielgruppe werden vom Administrator festgelegt. Verweis zum entsprechenden Kapitel (in
Konfiguration muss der Admin Einstellungen zu Zielgruppen und Veranstaltungstyp (Target
audiences und event types) vornehmen.

Wie oben erwähnt, können buchbare Veranstaltung in zwei Kategorien (»am/pm searchview«)
angezeigt werden (siehe Abbildung :ref:`fig_veranstaltungen-darstellung-am-pm`). Ganztägige Veranstaltungen werden von dieser Ansicht ausgeschlossen. Außerdem können Sie die Veranstaltungen als Liste (»Subscribeeventlist«)
darstellen. Sie können nur entweder buchbare oder normale Veranstaltungen anzeigen lassen
aber niemals beide gleichzeitig.

.. _fig_veranstaltungen-darstellung-am-pm:

.. figure:: ./images/veranstaltungen-darstellung-am-pm.png
   :width: 100%
   :alt: Darstellung am/pm

   Darstellung am/pm

Teilformular Subscribeviewtext:
===============================

Mit der Auswahl zwischen »Veranstaltungen am Vormittag« und „Überschrift der Nachmittagstabelle“ können Sie die zwei Kategorien festlegen (siehe Abbildung :ref:`fig_veranstaltungordner-subscribeviewtext`), nach denen die buchbaren Veranstaltungen sortiert und auch angezeigt werden können. Die Aufteilung in Vormittag / Nachmittag ist nur ein Vorschlag und kann geändert werden. In der Darstellung können Sie die buchbaren Veranstaltungen dann als »am/pm searchview« in den zwei Kategorien anzeigen lassen. Dazu müssen Sie bei der Erstellung der Veranstaltung eine Kategorie auswählen. Achtung: Die Anpassung der Bezeichnungen "Vormittag" und "Nachmittag" wird erst in der Darstellung vorgenommen, noch nicht in der Bearbeitung der Veranstaltungen. »Ganztägig« ordnet die Veranstaltung keiner Kategorie zu und taucht in der Darstellung »am/pm searchview« nicht auf.

   .. _fig_veranstaltungordner-subscribeviewtext:

   .. figure:: ./images/veranstaltungordner-subscribeviewtext.png
      :width: 100%
      :alt: Teilformular Subscribeviewtext

      Teilformular Subscribeviewtext

Teilformulare Register und Wording:
===================================

Hier können Sie darüber bestimmen, welche Informationen dem Teilnehmer auf der Buchungsseite angezeigt werden, welche Pflichtangaben die Teilnehmer machen müssen und welche Informationen der Teilnehmer bei erfolgreicher Anmeldung erhält. Die »Warning color« funktioniert wie eine Ampel, die über den Belegungsstatus informiert.

.. _sec_veranstaltung_buchen:

Veranstaltung buchen
====================

Um eine Veranstaltung buchen zu können, benötigen Sie ein Portlet für einen »Warenkorb«. Geben Sie gegebenenfalls die Portletspalte rechts frei und fügen Sie das Portlet »event shopping cart« hinzu. Wenn der angemeldete Interessent die Veranstaltungen anschaut, kann er über den Button »hinzufügen« die Veranstaltungen in den Warenkorb legen und kommt über den Button »Order« zu einem Anmeldeformular, wo er die Pflichtangaben machen muss.

Weitere Hinweise
=================

Sie können einen cvs-export erstellen. Dabei wird eine Excel-Tabelle mit den Anmeldeinformationen erstellt. Sie können csv-Listen pro einzelner Veranstaltung und für den gesamten Ordner erstellen.
