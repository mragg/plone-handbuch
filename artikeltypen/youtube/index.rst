.. index:: YouTube (Artikeltyp)
.. _sec_youtube:

=============
YouTube Video
=============

Mit den beiden Editoren (FCKeditor und Kupu) lassen sich auf einfache Art Videos von
Uni-Bonn.tv in eigene Beiträge integrieren. Darüber hinaus ist es möglich, Videos von
anderen Youtube-Kanälen zum Beispiel Ihres Institutes zu integrieren. Dazu muss der
Administrator der Website eine Anpassung im ZMI vornehmen, die Sie im Kapitel
:ref:`sec_eigene_youtube_kanaele_integrieren` finden.

Editor vs. Artikeltyp
=====================

Wenn Sie ein YouTube-Video als Artikel aus der Auswahlliste unter »hinzufügen« wählen,
wird das Video unter »Inhalte« aufgelistet und seine Metadaten stehen für die Suche und
Kollektionen zur Verfügung. Eine andere Möglichkeit, ein YouTube Video einzubinden ist,
es mithilfe der Editoren direkt in der Seite zu integrieren.

In beiden Editoren finden Sie einen YouTube-Button (siehe Abbildungen
:ref:`fig_standartansicht_kupu` und :ref:`fig_standartansicht_fck`), über den der
Suchen-Dialog für die Integration von Videos aufgerufen wird.

Video und Größe auswählen
=========================

Im erscheinenden Such-Dialogfenster gibt es unterschiedliche Suchen-Möglichkeiten. Sie
können im Dropdown-Menü die generelle Sortierung umschalten (nach Datum, Titel, Relevanz
etc.). Es wird immer nur ein Ausschnitt der Treffermenge angezeigt. Über einen Klick auf
den Link »Load more« am Ende der Treffermenge können Sie jeweils weitere Treffer nachladen.
Außerdem gibt es ein Suchfeld, mit dem Sie direkt nach Videos suchen können. Beachten Sie
bei der Suche, dass nur Videos integriert werden können, die von Uni-Bonn.TV oder dem
entsprechenden Kanal veröffentlicht wurden. Die Suche kann aufgrund von Fehlern auf Seiten
von YouTube eingeschränkt sein. Beachten Sie die exakte Rechtschreibung und Groß- und
Kleinschreibung bei der Suche und probieren Sie unterschiedliche Sortierungen.

.. _fig_youtube_video_auswahl:

.. figure::
   ./images/youtube-video-auswahl.png
   :width: 80%
   :alt: Youtube-Video Auswahlfenster

   Youtube-Video Auswahlfenster

Wenn Sie das Video gefunden haben, das Sie integrieren möchten, klicken Sie auf den Titel
des Videos. Es öffnet sich jetzt ein Dialog, in dem Sie auf unterschiedliche Größen
zugreifen können. Die Größe des Players ist dabei jeweils in Pixeln angegeben. Faustregel
ist, dass Sie die Größen „Thumb“ und „Mini“ sehr klein sind und sich z. B. eher für die
Darstellung in Portletspalten eignen. »Preview« und »Content« eignen sich für einen
größeren Player im Inhaltsbereich einer Seite. »Fullcontent« bzw. »Large« sind sehr große
Darstellungen, hier müssen Sie normalerweise noch die Portletspalten ausschalten.
Andernfalls benötigt der Player zu viel Platz.

Darstellung der Videos
======================

Das Video wird dann als klassischer YouTube-Player auf der Seite dargestellt. Als
Redakteur bekommen Sie im Bearbeitungsmodus nur das Vorschaubild dargestellt - im
Bearbeitungsmodus können Sie das Video nicht abspielen. Speichern Sie das Video.

.. _fig_youtube_video_darstellung1:

.. figure::
   ./images/youtube-video-darstellung1.png
   :width: 80%
   :alt: Ordnerdarstellung - youtube-video

   Ordnerdarstellung - youtube - video


Bei der Verwendung der beiden kleinen Playeransichten werden die Videos in einer
„Overlayeransicht“ geöffnet. Das heißt, das Video wird in einer größeren Darstellung über
der Seite eingeblendet.
