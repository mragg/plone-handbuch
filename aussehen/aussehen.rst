.. index:: Kopf, Inhaltsbereich, Fußzeile, Seitenspalten, Navigationsleiste, Portletspalte
.. _sec_aussehen:

====================
Benutzungsoberfläche
====================

In diesem Kapitel lernen Sie das Aussehen der Plone-Oberfläche kennen. Danach sind Sie in
der Lage, die in den folgenden Tutorien erwähnten Elemente der Oberfläche aufzufinden und
zuzuordnen.

Starten Sie Ihren Webbrowser und rufen Sie darin Ihre Website auf. Sie erfahren die
Adresse Ihrer Website von Ihrem Administrator.

Sie sehen nun die Oberfläche Ihrer Website, wie sie sich einem nicht angemeldeten
Benutzer darstellt (siehe Abbildung :ref:`Plone-Oberfläche für nicht angemeldete
Besucher <fig_plonebase>`).

.. _fig_plonebase:

.. figure:: ./images/plonebase.*
   :width: 100%
   :alt: Die Benutzeroberfläche für nicht angemeldete Besucher

   Plone-Oberfläche für nicht angemeldete Besucher. Die Hauptelemente sind der
   Kopf (1), der Inhaltsbereich (2), die Seitenspalten (3) und der Fuß (4).


Jede Seite der Website folgt dem gleichen Grundaufbau. Die Abbildung benennt die
Hauptelemente einer Plone-Seite:


* Kopf
* Inhaltsbereich
* eine oder zwei Seitenspalten
* Fußzeile

Diese Elemente werden in den einzelnen Abschnitten dieses Kapitels näher beschrieben.


Kopf
====

.. _fig_ploneheader:

.. figure:: ./images/ploneheader.*
   :width: 100%
   :alt: Kopf einer Plone-Seite

   Kopf einer Plone-Seite

..    Kopf einer Plone-Seite. Er enthält ein Logo (A), einige Verweise (B), das
..    Suchfeld (C) und die Navigationsleiste (D).}
  
Abbildung :ref:`fig_ploneheader` stellt die Bestandteile des Kopfes einer jeden Seite der
Website dar. Dabei handelt es sich um folgende vier Elemente:

* A Sekundär-Logo
* B Kopfgrafik
* C Verweise im Seitenkopf
* D Suchfeld
* E Navigationsleiste

Das Sekundärlogo in der linken oberen Ecke der Seite wird in aller Regel vom Administrator 
Ihres Plone-Portals angepasst worden sein. 

Rechts oben finden Sie Verweise zu einer Übersicht über den Inhalt der Website,
Informationen zur Barrierefreiheit und einem Kontaktformular.

Rechts daneben befindet sich das Suchfeld. Wenn Sie hier einen Suchbegriff eingeben und
die Schaltfläche »Suche« betätigen, wird eine Volltextsuche wahlweise in der gesamten
Website oder im aktuellen Bereich durchgeführt. 

Die Navigationsleiste besteht aus zwei Teilen:

* Hauptnavigation
* Verzeichnispfad (»Sie sind hier...«)

In der Hauptnavigation befinden sich Verweise auf wichtige Bereiche der Website, die von
jeder einzelnen Seite aus schnell erreichbar sein sollen. In der Regel sind das Ordner in
der obersten Ebene Ihrer Website.

Am Verzeichnispfad können Sie jederzeit Ihre Position in der Website ablesen. Sie sehen
dort den Pfad durch die Ordnerhierarchie, der Sie von der Startseite aus auf direktem Weg
zum aktuell angezeigten Artikel führt.  Jeder Schritt ist dabei ein Verweis auf einen
dazwischen liegenden Ordner. Diese Arte der Darstellung wird häufig auch als 
»Brotkrumen-Navigation« bezeichnet.


Inhaltsbereich
==============

Plone stellt Ihnen die Artikel Ihrer Website in verschiedenen Ansichten dar. Diese
Artikelansichten nehmen den Inhaltsbereich der Seiten ein. Wenn Sie Ihre Website unter
der Adresse besuchen, die Sie von Ihrem Administrator erhalten haben, sehen Sie eine
Ansicht der Startseite.

.. index:: Seitenspalten

Seitenspalten
=============

Links und rechts des Inhaltsbereichs können Seitenspalten auftauchen. Die Spalten nehmen
zusätzliche Informationen und Bedienelemente auf. Dazu kann jede der beiden Spalten
mehrere Portlets enthalten. Falls eine Spalte kein Portlet enthält oder wenn dieses nichts
anzeigt, wird die Spalte ausgeblendet. Sollten einmal beide Portletspalten ausgeblendet
werden, gelangen Sie ganz unten auf der Seite wieder in die "Portleteinstellungen".

Fuß
===

Der Fuß jeder Webseite enthält einen Vermerk zum Erstellungsdatum und Ersteller.

