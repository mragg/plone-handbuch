.. _sec_ui:

=============================
Aussehen der Plone-Oberfläche
=============================

Die Benutzungsoberfläche von Plone ist das, was Sie als Besucher einer Plone-Website in
Ihrem Webbrowser sehen. Dieses Kapitel beschreibt den Aufbau der Oberfläche. Sie erhalten
einen Überblick über die Anordnung der Informationen und Bedienelemente, die Plone Ihnen
zur Verfügung stellt.

In der Bonner Plone-Installation ist dabei ein sogenanntes "Theme" vorinstalliert. In diesem Theme ist das Corporate Design der Universität schon fertig umgesetzt, so z.B. Logos und Farbgebung.
Innerhalb des Corporate Designs gibt es Einstellmöglichkeiten, um z.B. das Logo der Einrichtung etc. unterzubringen.

Seit August 2016 gibt es als Weiterentwicklung ein *neues Theme*, welches jedoch nicht
"zwangsweise" für bestehende Portale eingeführt wird.
Auf die Veränderungen und Neuerungen gegenüber dem bisherigen Theme gehen wir daher in
einem separaten Abschnitt ein.

Gleichzeitig gehen die nachfolgenden Kapitel auch auf das Thema "Responsive Theme" ein:
Vereinfacht gesagt passt sich das Layout der Bonner Plone-Installation dem jeweiligen
Endgerät an, um den Platz dort optimal auszunutzen.
So werden z. B. die Seitenspalten auf einem Handy automatisch weggeklappt. Für die Gestaltung
Ihrer Webseite ist es wichtig, diese Mechanismen beim Informationsaufbau zu berücksichtigen.

.. note::
   Wenn Ihnen die Darstellung Ihrer Seiten auf mobilen Geräten besonders wichtig ist,
   sollten Sie das neue Theme bevorzugen. Denn dieses setzt responsives Layout wesentlich
   konsequenter und fortschrittlicher um.

.. toctree::
   :maxdepth: 1

   aussehen.rst
   theme2016.rst
   responsive-layout.rst
   ansichten.rst
   navigation.rst
   site-aktionen.rst
