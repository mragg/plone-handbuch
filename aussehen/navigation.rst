.. index:: Navigation
.. _sec_navigation:

==========
Navigation
==========

Es gibt verschiedene Möglichkeiten, sich gezielt durch eine Plone-Website zu bewegen:

* Orientierung an der Ordnerhierarchie: Hauptnavigation und Navigationsportlet, Übersicht
  und Verzeichnispfad
* Suche: Sofortsuche und erweiterte Suche
* Redaktionell gepflegte Mega-Menus (nur im neuen Theme verfügbar)


Ordnerhierarchie
================

Es gibt vier Elemente auf fast jeder Seite einer Plone-Website, mit deren Hilfe Sie sich
durch ihre Ordnerhierarchie bewegen können: die Hauptnavigation, das Navigationsportlet,
die Übersicht und den Verzeichnispfad. Diese Elemente ergänzen einander. Hauptnavigation,
Navigationsportlet und Übersicht geben einen Überblick über den Inhalt der Website, wobei
der Schwerpunkt beim Portlet auf dem gerade besuchten Teil liegt. Der Verzeichnispfad
teilt Ihnen mit, an welcher Stelle in der Ordnerhierarchie der Website Sie sich befinden.

Hauptnavigation
---------------

Die Hauptnavigation ist auf jeder Seite der Website verfügbar. Sie befindet sich im
Seitenkopf (siehe Abbildung :ref:`fig_globale_reiter`) und enthält Verweise auf wichtige
Stellen der Website.

.. _fig_globale_reiter:

.. figure:: ./images/globale-reiter.*
   :width: 100%
   :alt: Hauptnavigation und Verzeichnispfad

   Hauptnavigation und Verzeichnispfad

Plone bringt einige dieser Verweise bereits mit.

Der Reiter für die Gruppenarbeitsplätze wird nur angezeigt, falls Ihre Website
Gruppenarbeitsplätze benutzt.

Zusätzlich zu den genannten Reitern erzeugt Plone in der Grundeinstellung weitere Reiter
für alle Ordner, die sich direkt im Wurzelordner Ihrer Website befinden. Falls die Website
entsprechend konfiguriert ist, nimmt Plone auf diese Weise nicht nur Ordner, sondern alle
Artikel aus dem Wurzelordner, die Sie einsehen dürfen, in die Hauptnavigation auf. Der
Reiter für einen solchen Artikel ist dann mit dem Titel des Artikels beschriftet.

.. index:: Navigationsportlet

.. _sec_navigation_portlet:

Navigationsportlet
------------------

Das Navigationsportlet (siehe Abbildung :ref:`fig_portlet_navigation`) zeigt einen Teil
der Website als Ordnerbaum an. Dabei werden wiederum nur solche Ordner und Artikel
aufgeführt, die Sie auch einsehen dürfen. Viele Eigenschaften des Navigationsportlets
hängen von der Konfiguration Ihrer Website ab. Per Voreinstellung wird das Portlet auf
der Startseite nicht angezeigt, sondern erscheint erst in den einzelnen Ordnern Ihrer
Website.

.. _fig_portlet_navigation:

.. figure:: ../portlets/images/portlet-navigation.*
   :width: 40%
   :alt: Navigationsportlet

   Navigationsportlet

Da die Ordnerhierarchie einer großen Website sehr umfangreich und unübersichtlich werden
kann, wird nie der gesamte Baum gleichzeitig dargestellt. Das Navigationsportlet zeigt
stets nur den Teil Ihrer Website an, der sich innerhalb eines Ordners befindet und den
gerade besuchten Artikel enthält.

Per Voreinstellung beginnt die Anzeige immer mit einem Ordner, der selbst direkt im
Wurzelordner der Website liegt. Damit bildet das Navigationsportlet gerade ein Gegenstück
zur Hauptnavigation. Ob der betreffende Ordner selbst als oberster Eintrag im
Navigationsportlet auftaucht, hängt ebenfalls von der Konfiguration ab.

Der Eintrag für den gerade besuchten Artikel wird blau hinterlegt. Jeder Eintrag im
Navigationsportlet ist ein Verweis zu einem Ordner oder einem anderen Artikel.

Da das Navigationsportlet wie alle Portlets für jeden Ordner, ja sogar für einzelne
Artikel, individuell konfiguriert werden kann, ist es möglich, dass es sich nicht auf
jeder Seite gleichartig verhält.

.. note::

   Für ein Navigationsportlet, das für einen Unterordner konfiguriert wird, empfehlen
   sich folgende von den Standardeinstellungen abweichende Einstellungen:

   * **Obersten Eintrag einbeziehen** anhaken,
   * **Startebene** auf 0 setzen.

.. _sec_sitemap:

.. _fig_sitemap:

.. figure:: ./images/sitemap.*
   :width: 100%
   :alt: Die Sitemap gibt eine Übersicht über den Inhalt der Website

   Übersicht

Übersicht
---------

Unter den Verweisen im Kopf jeder Seite finden Sie die Übersicht. Die Übersicht oder
»Sitemap« ist eine Baumdarstellung aller Artikel der Website, die mit dem Wurzelordner
beginnt und bis zu einer bestimmten Tiefe der Ordnerhierarchie reicht. Per Voreinstellung
werden Ordner bis zur dritten Ebene mit allen enthaltenen Artikeln erfasst (siehe
Abbildung :ref:`fig_sitemap`). Ein weiteres Mal werden hier nur Artikel berücksichtigt,
die Sie auch betrachten dürfen. Jeder Eintrag in der Übersicht ist ein Verweis zur Anzeige
des jeweiligen Artikels.

.. index:: Verzeichnispfad

Verzeichnispfad
---------------

Sie können den Verzeichnispfad, englisch »breadcrumb menu«, als eine Art Brotkrumenspur
verstehen, die den Wurzelordner Ihrer Website durch die Ordnerhierarchie hindurch mit
Ihrem aktuellen Standpunkt verbindet. Das erste Element des Pfads ist der Wurzelordner der
Website (siehe Abbildung :ref:`fig_globale_reiter`). Danach folgen alle Unterordner, in
die Sie nacheinander wechseln müssen, um vom Wurzelordner zum gerade angezeigten Artikel
zu gelangen. Der aktuelle Artikel bildet den letzten Teil des Pfads. Alle Elemente des
Verzeichnispfads sind Verweise zu den jeweiligen Orten auf der Website.

.. _sec_suche:

.. index:: Suche, Mindbreeze Search Appliance

Suche, Mindbreeze Search Appliance
==================================

In Plone-Portalen verwenden wir standardmäßig seit Oktober 2016 eine Mindbreeze Search Appliance
(MSA), um Webseiten der Universität Bonn zu durchsuchen.
Die MSA indiziert alle Webseiten der Universität Bonn, unabhängig vom darunter verwendeten
Content-Management-System, d. h. es werden auch "Nicht-Plones" durchsucht. Die Search Appliance
ist ein eigener Hardware-Server, der vom HRZ der Universität Bonn betrieben wird, d. h. wir
haben administrative Hoheit auf dem Gerät. Der Server selbst ist vom Anbieter gemietet, wird
aber von uns in unseren Netzen betrieben. Der Anbieter hat keinen Zugriff auf den Server oder
z. B. indizierte Webseiten - dies war ein Grund für den Einsatz einer MSA innerhalb der
Universität Bonn, dass wir echte Such-Hardware bekommen, die wir kontrollieren können.

Die Ergebnisse kommen im Look&Feel der klassischen Google-Suchergebnisse daher, gleichzeitig
erfolgt die Sortierung und Bewertung von Seiten&Treffern durch Mindbreeze-Algorithmen.
Hier vertrauen wir auf das KnowHow der Suchmaschinen-Experten.

MSA-Integration in Plone
------------------------

Die Standardsuche in Plone durchsucht jeweils nur das lokale Plone. Das ist zwar nicht
schlecht, führt in unserer verteilten Plone-Installation aber immer wieder zu Nachfragen
und Einschränkungen, die eine gute Suche schlicht nicht ermöglichen.

Wenn z. B. der Lehrstuhl ein eigenes Plone hat, gibt es keine Treffer auf Institutsseiten.
Wenn es zu dem gesuchten Thema auch Treffer auf anderen Uni-Bonn Seiten gibt, werden
diese nicht angezeigt.
Mit der MSA haben wir nun einen Index über (hoffentlich alle) Uni-Bonn Webseiten erstellt,
und dieser Index ist aus jedem Plone heraus durchsuchbar. Standardmäßig ist Plone jetzt
so konfiguriert, dass der Gesamt-Index der Uni-Bonn Seiten durchsucht wird.

MSA-Konfiguration für Ihre Webseite
-----------------------------------
Der Standard "Alle Uni Bonn Seiten durchsuchen" ergibt vermutlich nicht auf allen
Webseiten Sinn. Sie haben aber die Möglichkeit, lokal in Ihrem Plone Einschränkungen
vorzunehmen. Hier ist es in erster Linie ein dynamischer Filter, der das Suchergebnis auf
bestimmte URLs einschränkt.

Komplexer und umfassender geht die Konfiguration, wenn Sie mit uns zusammen ein Set an
Parametern für Ihre Suche auf der MSA konfigurieren. In erster Linie definieren wir hier mit
Ihnen zusammen ein sogenanntes "Frontend" - hier haben wir die Möglichkeit direkt auf der
MSA z. B.: Teilmengen zu definieren, z. B. "Alle URLs, die etwas mit Mathematik in Bonn zu tun
haben." Darüber hinaus stehen uns die klassischen von Google bekannten Funktionen zur
Verfügung, um z. B. auf Suchbegriffe gezielt ein Banner mit z. B. weiteren Begriffen aber
auch direkten URLs als Hinweis über dem Suchergebnis anzuzeigen.

Der Plone-Support übernimmt hierbei mit Ihnen zusammen die initiale Konfiguration,
danach können Detailänderungen jeweils in Absprache mit dem Plone-Support vorgenommen
werden.

Wie sieht die MSA unsere Webseiten? - MSA-extern
------------------------------------------------

Damit die MSA einen "echten Blick von außen" hat und z. B. nicht fälschlicherweise Webseiten
indiziert, die nur innerhalb des BONNETs sichtbar sind, betreiben wir die MSA außerhalb des
BONNETS. Damit ist sichergestellt, dass die MSA von unseren Webservern wie ein "Besucher
von außen" betrachtet wird.

MSA für Portale mit IP-Schutz/Intranets - MSA-intern
----------------------------------------------------
Es gibt einige Portale, die mittels eines IP-Schutzes als Intranet betrieben werden, d. h.
Webseiten, die nur aus dem BONNET oder dem Institutsnetz erreichbar sind.
Die MSA-extern kann in diese Portale nicht hinein sehen. Wir haben daher eine zweite MSA
im Einsatz, die explizit für die Indizierung von internen Seiten gedacht ist. Diese ist
nur innerhalb des BONNETS zu erreichen.

Sprechen Sie uns an, wenn Sie die interne MSA für Ihre Suche verwenden möchten.

MSA pro Navigation-Root einstellbar
-----------------------------------

In Plone können die zentralen Einstellungen in jedem Navigation-Root überschrieben
werden. Sie finden die :ref:`Einstellungen <sec_navigation_root>` unter "navroot editieren" in der
Inhaltsansicht des entsprechenden Ordners.

.. _sec_suchportlet:

Such-Portlet
------------
Neben der MSA-Suche gibt es auch eine Plone-interne Suche. Sie finden diese als Portlet unter
"Portlet hinzufügen". Die Plone-interne Suche können Sie im Portlet auf einen Ordner
begrenzen und Treffer nur aus diesem Ordner anzeigen lassen. Gleichzeitig können Sie eine
Live-Vorschau aktivieren, damit bekommen Besucher schon Treffer beim Tippen in das
Suchfeld angezeigt.

.. _sec_mega_menu:

Neues Theme: Mega-Menu / Flyoutmenu
===================================

Ein neues Element im neuen :ref:`sec_unibonn_theme2016` sind
die sogenannten Mega-Menus (auch als "Flyoutmenu" bezeichnet).
Diese werden bei Hover/Klick auf einen
Menüpunkt in der Hauptnavigation eingeblendet.

.. _fig_mega_menu:

.. figure:: ./images/Fly_Out.*
   :width: 100%
   :alt: Mega-Menu ausgeklappt

   Mega-Menu ausgeklappt

Die Struktur und Elemente des Mega-Menus sind redaktionell gestaltet. Sie finden die
Einrichtung in den Eigenschaften des jeweiligen Ordners. Wechseln Sie auf den
Navigationspunkt, zu dem Sie ein Mega-Menu einrichten wollen, dort auf :guilabel:`„Inhalte“` und
:guilabel:`„Bearbeiten“`. Im Register :guilabel:`„Einstellungen“` finden
Sie unten den Eintrag :guilabel:`„Folder Menu“`.

.. _fig_mega_menu_edit:

.. figure:: ./images/Fly_Out_bearbeiten.*
   :width: 60%
   :alt: Mega-Menu bearbeiten

   Mega-Menu bearbeiten

Um die Struktur und Optik der Ausgabe hinzubekommen, sollte dieser Eintrag nur mit
dem FcK-Editor bearbeitet werden. Im Fck-Editor finden Sie in der oberen
Buttonzeile den Button :guilabel:`„Vorlagen“` |templates|.

.. |templates| image:: ./images/Vorlagen_Icon.png

.. _fig_mega_menu_template:

.. figure:: ./images/Fly_Out_Vorlagen.*
   :width: 40%
   :alt: Mega-Menu Vorlage im FCK-Editor

   Mega-Menu Vorlage im FCK-Editor

Klicken Sie auf diesen Button. In dem nachfolgenden
Popup finden Sie den Eintrag :guilabel:`„Flyout Column“`. Klicken
Sie auf den Eintrag. Sie erhalten dann einen Beispieleintrag in das Editorfeld.
Ein Abstand (Umbruch) zwischen zwei „Flyout Columns“ erzeugt in der Ausgabe eine
neue Spalte. Innerhalb einer Spalte können Sie Zwischenüberschriften erzeugen,
indem Sie einem Eintrag das Überschriftenformat „h3“ zuweisen.

Wir empfehlen Ihnen, zuerst die komplette Struktur über das Anlegen mehrer „Flyout
Columns“ zu erzeugen. Kontrollieren Sie die Ausgabe, und fangen Sie erst danach
mit dem Zwischenstrukturieren und den einzelnen Links an.

.. note::
   Es empfiehlt sich, die Bearbeitung dieser Einträge auf möglichst wenige Redakteure zu reduzieren.
