.. index:: Layout, Responsive Layout
.. _sec_responsive_layout:

=================
Responsive Layout
=================

Seit Mitte Oktober 2014 ist auf allen Uni Bonn Seiten ein sogenanntes »responsive theme« aktiv, wodurch
sich die Uni Bonn Seiten an das jeweilige Endgerät des Seitenbesuchers anpassen.

Die Neuerung dient dazu, die Uni Bonn Webseiten für die verschiedenen Geräte der Benutzer
anzupassen - egal ob man einen Computer mit großem Bildschirm, ein Tablet oder sein
Smartphone benutzt, die Uni Seite erkennt das Gerät und passt sich diesem in der Benutzbarkeit an.

Das »responsive theme« ist direkt auf allen Uni Bonn Seiten verfügbar und aktiv, es ist
für den Administrator nicht nötig, dieses freizuschalten.

Die Idee dahinter ist, dass Sie für Ihre Nutzer keine zusätzlichen Smartphone-Apps oder ähnliches
anfertigen müssen, sondern Ihre Website automatisch auch für kleine Bildschirme optimiert wird.

Hauptziel bei der Entwicklung des Responsive Layouts war den Fokus bei kleineren Bildschirmen
auf den Inhaltsbereich zu lenken.

Auf welche Weise passen sich die Seiten an?
===========================================

Damit die Webseite auch auf einem kleinen Smartphonebildschirm gut lesbar ist, wird nur
der Contentbereich (die mittlere Spalte) auf Smartphones dargestellt. Die Portletspalten
links und rechts (linke und rechte Spalte neben dem Contentbereich) klappen weg und
können - um deren Inhalt zu betrachten - durch Buttons am unteren Seitenrand jeweils
ausgeklappt werden.

.. _fig_responsive_layout_ohne_portlets:

.. figure:: ./images/responsive-layout-ohne-portlets.*
   :width: 80%
   :alt: e-mailadresse

   Ansicht der Responsive Layout ohne Portlets

Zusätzlich klappt sich die Hauptnavigation auf einem Smartphone ein, und kann am oberen
Seitenende zur Interaktion ausgeklappt werden. Dies betrifft auch die Navigation, die
klassischerweise als Navigationsportlet in der linken Spalte untergebracht ist. Da im
mobilen Layout die Portletspalte standardmäßig eingeklappt ist und das die Navigation
erschweren würde, wird das Navigationsportlet automatisch in den Menüpunkt »Navigation«
integriert und damit im mobilen Layout aus der Portletspalte entfernt.

Zusätzlich wird der obere Bereich (Kopfzeile und Kopfgrafiken) angepasst, so dass die
Elemente auf kleinen Bildschirmen Platz finden. Dazu werden die Grafiken entsprechend
verkleinert. Aus der Kopfzeile werden alle Statuslinks (z.B. Impressum, Sitemap etc.)
in die Fußzeile verschoben. So bleiben in der Kopfzeile nur »Navigation«, die
Sprachenflaggen und das Suchfeld. Diese passen auf Handybildschirmen auch nebeneinander.

.. _fig_responsive_layout_mit_einem_portlet:

.. figure:: ./images/responsive-layout-mit-einem-portlet.*
   :width: 80%
   :alt: e-mailadresse

   Ansicht der Responsive Layout ohne Portlets

Weiterentwicklung
-----------------

Das Responsive Theme wird laufend weiterentwickelt und stetig angepasst. Die mobile Welt
ist inzwischen sehr vielfältig mit einer Vielzahl an unterschiedlichen Geräten und
Systemen. Wir versuchen, einen Großteil der Geräte zu erreichen und auf möglichst vielen
Plattformen zu testen, sind uns aber bewusst, dass das mobile Layout nicht auf allen
Geräten funktionieren wird. Wir haben die Lösung trotzdem ausgerollt, weil wir davon
ausgehen, dass die geschätzten 80% der Geräte, die wir aktuell erreichen, von der
Verbesserung profitieren sollten.

Treten Fehler oder unerwünschte Seiteneffekte auf, die Sie mit dem Responsive Theme in
Verbindung bringen, melden Sie dies bitte an den `Plone Support`_.


.. _`Plone Support`: http://www.plone.uni-bonn.de
