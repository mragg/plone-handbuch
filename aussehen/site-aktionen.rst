.. index:: Seitenkopf
.. _sec_gui_siteaktionen:

======================
Verweise im Seitenkopf
======================

Ganz rechts oben auf jeder Plone-Seite befinden sich einige Verweise, die von jeder Seite
aus erreichbar sein sollen, aber nicht zur Hauptnavigation gehören.

* Übersicht
* Barrierefreiheit
* Kontakt

Die Übersicht wird in Abschnitt :ref:`sec_sitemap` beschrieben.

.. _fig_verweise_seitenkopf:

.. figure:: ./images/verweise-seitenkopf.*
   :width: 100%
   :alt: Verweise im Seitenkopf

   Verweise im Seitenkopf

Unter »Barrierefreiheit« gelangen Sie zu einer Erläuterung von Plones
Funktionen für barrierefreie Webseiten. Barrierefreiheit bedeutet, dass
Webseiten auch von Menschen mit körperlichen Behinderungen sowie mit
verschiedenartigen Geräten gelesen und benutzt werden können. Dadurch ergeben
sich Anforderungen wie veränderbare Schriftgrade, Navigation ohne Benutzung
der Maus oder Darstellung auf kleinen Bildschirmen.

Plone trägt auf vielerlei Weise dazu bei, barrierefreie Webseiten zu
erstellen. Die Seite »Barrierefreiheit« befasst sich mit zwei Aspekten:

Textgröße
  Unter der Überschrift »Textgröße« finden Sie drei Schalter, mit denen Sie die
  Grundschriftgröße der Website groß, normal und klein machen können.

Tastaturkürzel
  Plone hat zehn Tastaturkürzel, mit denen Sie wichtige Stellen auf
  der Website sofort aufrufen können. Die Kürzel sind von 0 bis 9
  durchnummeriert und werden mit Tastenkombinationen aufgerufen.  Auf
  der Seite »Barrierefreiheit« wird die Bedeutung aller Tastaturkürzel
  beschrieben und erklärt, wie man sie in verschiedenen Webbrowsern
  benutzt. Diese Seite erreichen Sie beispielsweise immer mit dem
  Kürzel 0.

Über den Verweis »Kontakt« gelangen Sie zu einem Kontaktformular, mit dem
Sie eine Nachricht an den Betreiber oder Verantwortlichen der Website
versenden können. Falls Sie angemeldet sind, müssen Sie hier lediglich einen
Betreff und die Nachricht selbst eingeben. Als nicht angemeldeter Besucher
können Sie zusätzlich Ihren Namen und Ihre E-Mail-Adresse angeben.
