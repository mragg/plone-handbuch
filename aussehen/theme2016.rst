.. index:: Uni Bonn Theme 2016
.. _sec_new_theme:

==================
Neues Theme (2016)
==================

Seit Mitte August 2016 erscheinen die zentralen Universitätswebseiten in einem neuen Theme.
Basierend auf "Bootstrap" verfügt es über ein nochmals verbessertes responsives Layout
und ist somit optimiert für mobile Endgeräte.
Zudem wurde auf eine ansprechende, dem Zeitgeschmack entsprechende grafische Gestaltung Wert
gelegt und einige interessante neue Elemente hinzugefügt.

.. _fig_new_theme:

.. figure:: ./images/Neues_Theme.*
   :width: 100%
   :alt: Neues Theme mit Portalstartseite

   Neues Theme mit Portalstartseite

Die Neuerungen im Überblick
===========================

Kopfbereich
-----------

Sprachenumschalter
   Die Sprachenumschaltung erfolgt jetzt links oben. Die Sprachen werden nicht mehr
   durch Flaggen, sondern durch die Iso-Sprachcodes (DE, EN, FR etc.) dargestellt.
Logo-Bereich
   Der Logobereich wurde in der Höhe verkleinert. Der Schriftzug unter dem Uni-Bonn-
   Logo entfällt. Damit reduziert sich auch der Platz für das Einrichtungslogo nach unten.
Suchfeld
   Das Suchfeld wird prominenter im rechten Bereich des Seitenkopfes dargestellt.
Social Media Links
   Links neben dem Suchfeld können Social Media Icons (inkl. Verlinkung) angezeigt
   werden. Sie können selbst definieren, welche Icons inkl. Links dort angezeigt werden sollen.
Kopfgrafiken entfallen
   Die aus dem altem Theme bekannten Kopfgrafiken entfallen im neuen Theme ersatzlos.
Mega-Menus
   Bei Hover/Klick auf einen Menüpunkt in der Hauptnavigation können - redaktionell
   gepflegte - Menus eingeblendet werden.

Inhaltsbereich
--------------

Für den Inhaltsbereich bietet das neue Theme die Möglichkeit, sogenannte Portalseiten
anzulegen und als Startseiten für den Hauptauftritt, aber auch beliebige Unterordner
festzulegen. Portalseiten bestehen aus mehreren vorgegebenen Elementen:

Slider
   Hier können ein oder mehrere Bilder mit einem kurzen Text überlagert angezeigt werden.
Feature Buttons
   Unter dem Slider befinden sich fünf Buttons, bestehend aus Beschriftung, Symbol und einem
   Link, um Besucher auf wichtige Seiten des Webauftritts hinzuweisen.
Newsbereich + Portletspalte
   Im mittleren Teil der Portalseite ist standardmäßig der Newsbereich untergebracht.
   Dieser zeigt automatisch News (vormals Pressemitteilungen) an. In einer rechts
   daneben liegenden Portletspalte können beliebige Portlets eingeblendet werden.
Kampagnen
   Darunter befinden sich vier Kampagnenkästen. Diese funktionieren ähnlich wie die
   Feature Buttons, bieten jedoch Raum für ein Bild und etwas mehr Text.

Fußbereich
----------

Ein weiteres Element des neuen Themes, das sich global über alle Seiten erstreckt,
ist eine redaktionell gepflegte Sitemap - "Doormat" - am Ende jeder Seite.
Die aus dem alten Theme bekannte Fußzeile mit dem Vermerk zum Erstellungsdatum und
Ersteller gibt es jedoch nicht mehr.

Weiterführende Informationen
============================

Wer mit seinem Plone-Portal auf das neue Layout umsteigen möchte, findet weitere
Informationen und Hinweise auf folgenden Seiten:

* :ref:`Installation, Portalseiten, Doormat <sec_unibonn_theme2016>`
* :ref:`Mega-Menu <sec_mega_menu>`
* :ref:`Social Media Links <sec_navigation_root>`
