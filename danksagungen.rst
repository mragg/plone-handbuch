Danksagungen
============

Das Hochschulrechenzentrum der Universität Bonn pflegt für seine Plone-Nutzer diese
Version des Plone-Handbuchs. Es basiert auf den Vorarbeiten des Plone-Handbuches, an dem
vor dem HRZ viele Personen und Firmen aktiv mitgearbeitet haben bzw. die Entwicklung
unterstützt haben. Dazu gehören Christian Theune und Thomas Lotze von der Firma
`gocept gmbh`, die dankenswerterweise das Handbuch unter einer Creative Commons-Lizenz
herausgebracht haben. Daneben gilt der Dank vor allem Jan Ulrich Hasecke, der die
Dokumentation vorangetrieben hat und auch viele der Bonner Plone-Tools dokumentiert hat!


* `Charlie Clark Consulting, Düsseldorf`_

* `Fafalter GmbH, Düsseldorf`_

* `iqplusplus, Erfurt`_

* `Jan Ulrich Hasecke, Solingen`_

* `Plone-Team Hochschulrechenzentrum Universität Bonn`_


.. _`Charlie Clark Consulting, Düsseldorf`: mailto:charlie.clark@clark-consulting.eu

.. _`Fafalter GmbH, Düsseldorf`: http://www.fafalter.de

.. _`iqplusplus, Erfurt`: http://www.iqpp.de

.. _`gocept gmbh`: http://www.gocept.com

.. _`Jan Ulrich Hasecke, Solingen`: http://www.hasecke.com

.. _`Hochschulrechenzentrum Universität Bonn`: http://www.hrz.uni-bonn.de
