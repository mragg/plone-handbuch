.. index:: Benutzer
 
========
Benutzer
========

Verschiedene Benutzer interessieren sich für ganz unterschiedliche Dinge, wenn
sie ein Content-Management-System benutzen.
Unangemeldete Besucher wollen den öffentlichen Inhalt der Website
einsehen und ihre öffentlich zugänglichen Funktionen nutzen. Autoren tragen
zum Inhalt bei, während Redakteure für die Veröffentlichung von Artikeln
verantwortlich sind.

Plone kann Ihnen für Sie persönlich bestimmte Informationen und
Bedienmöglichkeiten anbieten. Dazu müssen Sie an der Website registriert sein
und sich zu Beginn jeder Sitzung mit Ihrem Benutzernamen und persönlichen
Passwort ausweisen. Solange Sie das nicht tun, bekommen Sie stets nur die
öffentliche Ansicht der Website zu sehen.

Als angemeldeter Benutzer erhalten Sie auf bestimmte Artikel mehr
Zugriffsrechte, sodass Sie beispielsweise weitere Artikelansichten zu sehen
bekommen.

Weiterhin gibt es eine Reihe von Portlets, die angemeldeten Benutzern
vorbehalten sind und für sie personalisiert werden.

Auf der anderen Seite wird es durch die Unterscheidung der Benutzer
und die Beachtung von Zugriffsrechten möglich, private Daten zu
schützen und den Zugang der Benutzer zu bestimmten Bereichen
einzuschränken.

Ist Ihre Website entsprechend konfiguriert, wird Ihnen darüber
hinaus ein persönlicher Ordner zur Verfügung gestellt. Dort können Sie
nach eigenem Ermessen Unterordner und andere Artikel anlegen,
bearbeiten und löschen.

