.. _sec_grundlagen_von_plone:

====================
Grundlagen von Plone
====================

Plone ist ein webbasiertes Content-Management-System. Sie können damit den Webauftritt Ihrer Einrichtung / Ihres Instituts ohne Programmierkenntnisse bearbeiten.
Sämtliche Bearbeitungen werden in Ihrem normalen Webbrowser gemacht, Sie können weltweit von jedem internetfähigen Rechner Ihre Webseiten bearbeiten.

Die ersten Kapitel legen die nötigen Grundlagen und definieren einige Begriffe und Herangehensweisen, auf die in den nachfolgenden Kaptieln immer wieder Bezug genommen wird.
Sie finden im Handbuch dann immer wieder praktische Übungen, die Sie in Ihrem Plone selbst nachvollziehen können.

Die Plone-Installation der Universität Bonn ist ein großer Cluster, auf dem derzeit ca. 300 Plone-Installationen parallel betrieben werden. Die Plone-Grundfunktionalität steht dabei in allen Plones zur Verfügung.
Die Administration erfolgt dabei aufgeteilt: Die Administration der technischen Gesamtumgebung erfolgt durch das Hochschulrechenzentrum, auf der Ebene des Webauftritts erfolgt die Administation durch die jeweilige Einrichtung.
Es gibt eine Reihe an Funktionalitäten, die in allen Plones direkt freigeschaltet und nutzbar sind. Einige Funktionen müssen erst durch den dezentralen Administrator freigeschaltet werden.

.. toctree::
   :maxdepth: 1

   artikel.rst
   ansichten.rst
   benutzer.rst
