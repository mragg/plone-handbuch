.. index:: Support
.. _sec_plone_support:

=============
Plone-Support
=============

Wir beraten Sie gerne im Vorfeld, richten Ihnen "Ihr Plone" ein, unterstützen Sie und
Ihre Kolleginnen und Kollegen mit Schulungen und helfen via E-Mail oder telefonisch auch
bei allen Plone-Fragen und Problemen im alltäglichen Betrieb.

Damit wir Ihnen gezielt und zeitnah helfen können, nutzen Sie je nach Frage- oder
Problemstellung unsere Anfrageformulare unter `www.plone.uni-bonn.de`_

| Hochschulrechenzentrum der Universität Bonn
| Wegelerstraße 6
| 53115 Bonn
| Raum 0.012

| **Telefon:** 0228-733157
| **E-Mail:** plone@uni-bonn.de

.. _`www.plone.uni-bonn.de`: http://www.plone.uni-bonn.de
