.. index:: Bilingual Portlet
.. _sec_bilingual_portlet:

=================
Bilingual Portlet
=================
Von der Funktionalität ist ein Bilinguales Portlet identisch mit dem Statischen Textportlet (:ref:`sec_statisches_text`)
Auf der obersten Ebene eines Plone-Auftritts/Navigationroots werden Portlets in allen Sprachen angezeigt. In darunterliegenden Ordnern werden Portlets nur in der jeweiligen Sprache angezeigt. Wenn Sie gezielt auf der Startseite eines mehrsprachigen Auftritts ein Portlet nur in einer Sprache anlegen möchten, nutzen Sie das Bilingual-Portlet. Sie können dort in den Optionen auswählen in welchem Sprachbereich das Portlet angezeigt werden soll.
Dieses Portlet wird in der Regel nicht benötigt, wenn Sie in Ihrem Ploneportal :ref:`sec_linguaplone` verwenden.
