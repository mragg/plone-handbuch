.. index:: Bonnus-Portlet
.. _sec_bonnus_portlet:

==============================================
Bonnus-Portlet. Literatursuche im Hauptkatalog
==============================================

Es steht ein neues Plone-Produkt »Bonnus-Portlet« zur Verfügung, das die Literatursuche im
Hauptkatalog und den Zugang zu Ihrem Benutzerkonto, direkt von Ihrer Institutsseite
ermöglicht.

Das »Bonnus-Portlet« bietet die Möglichkeit direkt von Ihrer Institutsseite den Hauptkatalog
der Universitäts- und Landesbibliothek Bonn (ULB) nach Allgemein- und Fachliteratur zu
durchsuchen, Ihr Konto aufzurufen oder ggf. Bestellungen und Vormerkungen aufzugeben.
In bonnus finden Sie den gesamten Bestand der Bibliotheken der Universität Bonn.
Darüberhinaus durchsuchen Sie mit einer Recherche aber auch weitere Quellen, wie
lizenzierte und freie Zeitschriften und Bücher sowie Inhalte verschiedener Datenbankanbieter.
Die Zahl der in bonnus enthaltenen Quellen wächst hierbei stetig an.

Voraussetzung / Installation
============================

Wie alle anderen Zusatzprodukte muss das »Bonnus-Portlet« einmalig auf Ihrer Instanz
installiert werden. Die Installation muss vom Administrator der Plone-Instanz gemacht
werden.

**Klickweg:** »Konfiguration« → »Zusatzprodukte« → »Installierbare Produkte« →
»unibonn.bonnusportlet« → »installieren«

.. _fig_bonnus_portlet_01:

.. figure:: ./images/bonnus-portlet-01.*
   :width: 75%
   :alt: Installation von Bonnus-Portlet

   Installation von Bonnus-Portlet

**Hinweis:** Entfernen Sie bitte keine installierten Produkte, weil dies zu instabiler
Funktionalität der ganzen Instanz führen kann!


Nutzung
=======

Nachdem das Bonnus-Portlet installiert ist, können Sie es wie alle anderen Portlets in Ihrer
Seite hinzufügen und entweder in der rechten oder linken Spalte anzeigen lassen
(»Portleteinstellungen« → »Portlet hinzufügen« → »Bonnus Portlet«) oder innerhalb einer
PortletPage einbinden (z. B. Homepage des `Staatswissenschaftliches Seminar`_).

.. _`Staatswissenschaftliches Seminar`: http://www.sws.uni-bonn.de/

Das Portlet enthält das Eingabefeld für eine direkte Suche, ein Auswahlfeld, um
die Suche auf ein bestimmtes Feld einzuschränken, den Link zur `Erweiterten
Suche`_ und den Link zum Benutzerkonto `Mein Konto`_.

.. _`Erweiterten Suche`: https://merry.ulb.uni-bonn.de/ulb/bonnus.php?referrerBonnus=portlet&destination=advanced
.. _`Mein Konto`: https://merry.ulb.uni-bonn.de/ulb/bonnus.php?referrerBonnus=portlet&destination=account

.. _fig_bonnus_portlet_02:

.. figure:: ./images/bonnus-portlet-02.*
   :width: 50%
   :alt: Nutzung von Bonnus-Portlet

   Nutzung von Bonnus-Portlet


Einstellungen
=============
In dem Fenster »Portlet bearbeiten« können Sie einige wenige Einstellungen vornehmen.

.. _fig_bonnus_portlet_03:

.. figure:: ./images/bonnus-portlet-03.*
   :width: 85%
   :alt: Einstellungen des Bonnus-Portlets

   Einstellungen des Bonnus-Portlets

Die Überschrift des Portlets lautet immer »bonnus - das Suchportal der Universität«.

Im Eingabefeld »Text« können Sie einen kurzen Text eingeben, der unter dem Suchbutton
angezeigt wird. Dies kann ein Tipp, Hinweis oder auch ein Link
sein. Im Falle einer Wartung des Hauptkatalogs kann hier z. B. auf die `Aktuellmeldung der
ULB`_ verwiesen werden.

.. _`Aktuellmeldung der ULB`: http://www.ulb.uni-bonn.de/
