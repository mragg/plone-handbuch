.. index:: Portlets
.. _sec_portlets:


========
Portlets
========

Zum Inhalt einer Plone-Seite trägt nicht nur der aktuelle Artikel bei. Jede Seite
enthält auch eine Anzahl von Portlets, die links und rechts neben der Artikelansicht
angeordnet sind. Portlets fassen Inhalte der Website zusammen, bieten Zugriff auf ihre
Funktionen oder stellen andere Informationen bereit.

Dieses Buch beschreibt die Details der einzelnen Portlets dort, wo ihre Funktion
besprochen wird (siehe Tabelle :ref:`tab_portlets`).


.. toctree::
   :maxdepth: 1
   :caption: Portlets

   standardportlets.rst
   bilingual.rst
   bonnus-suche.rst
   revisionsliste.rst
   statisches-text.rst
