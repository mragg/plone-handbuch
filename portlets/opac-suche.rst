.. index:: OPAC-Portlet
.. _sec_opac_portlet:

============================================
OPAC-Portlet. Literatursuche im Hauptkatalog
============================================

**Achtung:** Dieses Portlet ist veraltet. Bitte nicht mehr verwenden!

Es steht ein neues Plone-Produkt »OPAC-Portlet« zur Verfügung, das die Literatursuche im
Hauptkatalog und den Zugang zu Ihrem Benutzerkonto, direkt von Ihrer Institutsseite
ermöglicht.

Das »OPAC-Portlet« bietet die Möglichkeit direkt von Ihrer Institutsseite den Hauptkatalog
der Universitäts- und Landesbibliothek Bonn (ULB) nach Allgemein- und Fachliteratur zu
durchsuchen, Ihr Konto aufzurufen oder ggf. Bestellungen und Vormerkungen aufzugeben.
Über den Hauptkatalog sind alle elektronisch erfassten Bestände der ULB, einer Großzahl
der Universitätsinstitute, der ehemaligen Deutschen Zentralbibliothek für
Landbauwissenschaften (ZBL) und der Bereichsbibliothek für Ernährung, Umwelt und
Agrarwissenschaften (ZB MED Köln) recherchierbar.

Voraussetzung / Installation
============================

Wie alle anderen Zusatzprodukte muss das »OPAC-Portlet« einmalig auf Ihrer Instanz
installiert werden. Die Installation muss vom Administrator der Plone-Instanz gemacht
werden.

**Klickweg:** »Konfiguration« → »Zusatzprodukte« → »Installierbare Produkte« →
»unibonn.opacportlet 1000« → »installieren«

.. _fig_opac_portlet_01:

.. figure:: ./images/opac-portlet-01.*
   :width: 75%
   :alt: Installation von OPAC-Portlet

   Installation von OPAC-Portlet

**Hinweis:** Entfernen Sie bitte keine installierte Produkte, weil dies zu instabiler
Funktionalität der ganzen Instanz führen kann!


Nutzung
=======

Nachdem das OPAC-Portlet installiert ist, können Sie es wie alle andere Portlets in Ihrer
Seite hinzufügen und entweder in der rechten oder linken Spalte anzeigen lassen
(»Portleteinstellungen« → »Portlet hinzufügen« → »OPAC Portlet«) oder innerhalb einer
PortletPage einbinden (z. B. Homepage des `Staatswissenschaftliches Seminar`_).

.. _`Staatswissenschaftliches Seminar`: http://www.sws.uni-bonn.de/

Das Portlet enthält das Eingabefeld für eine direkte Suche, den Link zur `Erweiterten
Suche`_, dem Benutzerkonto `Ihr Konto`_ und weiteren Informationen zur Anmeldung und Inhalt
des Hauptkatalogs. Je nach Einstellung des Portlets wird noch die Option für die
Auswahl einer Zweigstelle angezeigt.

.. _`Erweiterten Suche`: http://opac.ulb.uni-bonn.de:8080/webOPACClient/start.do
.. _`Ihr Konto`: http://opac.ulb.uni-bonn.de:8080/webOPACClient/start.do?StartPage=userAccount

.. _fig_opac_portlet_02:

.. figure:: ./images/opac-portlet-02.*
   :width: 50%
   :alt: Nutzung von OPAC-Portlet

   Nutzung von OPAC-Portlet


Einstellungen
=============
In dem Fenster »Portlet bearbeiten« müssen Sie einige wenige Einstellungen vornehmen.

.. _fig_opac_portlet_03:

.. figure:: ./images/opac-portlet-03.*
   :width: 90%
   :alt: Einstellungen des OPAC-Portlets

   Einstellungen des OPAC-Portlets

Die Überschrift des Portlets lautet immer »Hauptkatalog«.

Die Eigenschaft »Zweigstellen« MUSS unbedingt angepasst werden, da die Suche ansonsten
scheitert und nicht an den Hauptkatalog übergeben werden kann. Verschieben Sie einfach,
die für Ihr Institut maßgebende Zweigstelle nach rechts. Wird mehr als eine Zweigstelle
ausgewählt erscheint unterhalb des Suchfelds die Option »Zweigstelle auswählen«. Die
erste Zweigstelle im rechten Bereich ist standardmäßig voreingestellt.

Im Eingabefeld »Text« können Sie einen kurzen Text eingeben, der unter dem Suchbutton
angezeigt wird. Dies kann ein Tipp, Hinweis oder auch ein Link (HTML-Kenntnisse nötig)
sein. Im Falle einer Wartung des Hauptkatalogs kann hier z. B. auf die `Aktuellmeldung der
ULB`_ verwiesen werden.

.. _`Aktuellmeldung der ULB`: http://www.ulb.uni-bonn.de/


Option "Zweigstelle auswählen"
==============================

Die Option "Zweigstelle auswählen" bestimmt die Statusanzeige eines Exemplars im
Hauptkatalog. Hierdurch wird bestimmt, ob ein Exemplar als :guilabel:`"ausleihbar"` oder
:guilabel:`"in anderer Zweigstelle frei"` angezeigt wird.

* :guilabel:`ausleihbar` = in der gewählten Zeigstelle, z. B. Hauptbibliothek oder
* :guilabel:`in anderer Zweigstelle frei` = in der nicht gewählten Zweigstellen, z. B.
  Abteilungsbibliothek MNL. Das Suchergebnis bleibt dabei immer gleich.

**Beispiele:**

* Für Institute und/oder Institutsbibliotheken, die in der Regel ausleihbare Literatur
  ausschließlich in der :guilabel:`Hauptbibliothek` suchen, empfiehlt es sich diese
  Zweigstelle auszuwählen. (z. B. Fachgebiet: Germanistik, Anglistik oder Philosophie)
* Für Institute und/oder Institutsbibliotheken, die in der Regel ausleihbare Literatur
  ausschließlich in der :guilabel:`Abteilungsbibliothek MNL` suchen, empfiehlt es sich
  diese Zweigstelle auszuwählen. (z. B. Fachgebiet: Biologie, Landbauwissenschaften oder
  Medizin)
* Für Institute und/oder Institutsbibliotheken, die ausleihbare Literatur sowohl in der
  :guilabel:`Hauptbibliothek` als auch in der :guilabel:`Abteilungsbibliothek MNL`
  suchen, empfiehlt es sich beide Zweigstelle auszuwählen und diese als Option unterhalb
  des Suchfelds anzeigen zu lassen. (z. B. Fachgebiet: Geographie oder Psychologie)

Der Suchende kann dann selber entscheiden welche Voreinstellung er für seine Recherche
benötigt.
