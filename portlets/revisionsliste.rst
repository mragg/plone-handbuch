.. index:: Revisionsliste
.. _sec_revisionsliste:

Revisionsliste
==============

Die Revisionsliste ist ein Portlet, das Redakteuren eine Liste aller zur
Veröffentlichung eingereichten Artikel anzeigt (siehe
Abbildung :ref:`fig_portlet_revlist`).

.. _fig_portlet_revlist:

.. figure:: ./images/portlet-revlist.*
   :width: 40%
   :alt: Das Portlet zeigt Artikel, die eingereicht wurden

   Portlet »Revisionsliste«


So haben Redakteure einen Überblick über die anstehende Arbeit und können die
zu prüfenden Artikeln direkt aufrufen.

Jeder zu prüfende Artikel ist mit Titel und Datum der letzten Änderung
aufgeführt. Der Titel ist ein Verweis zum Artikel selbst, und ein
Symbol zeigt den Artikeltyp an. Wenn Sie den Mauszeiger über den Titel
halten, sehen Sie zusätzlich die Beschreibung des Artikels.

Die Liste ist nach dem Einreichungsdatum sortiert und beginnt mit dem
Artikel, der bereits am längsten auf die Prüfung wartet.