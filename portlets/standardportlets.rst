========================
Übersicht aller Portlets
========================

.. _tab_portlets:

.. table:: Portlets in der linken und rechten Spalte

   +-----------------------+----------------------------------------+
   | Titel                 | siehe                                  |
   +=======================+========================================+
   | Anmelden              | :ref:`sec_tut_anmelden`                |
   +-----------------------+----------------------------------------+
   | Navigation            | :ref:`sec_navigation_portlet`          |
   +-----------------------+----------------------------------------+
   | Aktuelle              | :ref:`sec_nutz_von_metad_1`            |
   | Änderungen            |                                        |
   +-----------------------+----------------------------------------+
   | Suche                 | :ref:`sec_suchportlet`                 |
   +-----------------------+----------------------------------------+
   | Kollektionsportlet    | :ref:`sec_kollektionsportlet`          |
   +-----------------------+----------------------------------------+
   | Revisionsliste        | :ref:`sec_revisionsliste`              |
   +-----------------------+----------------------------------------+
   | Nachrichten           | :ref:`sec_nachrichten_portlet`         |
   +-----------------------+----------------------------------------+
   | Termine               | :ref:`sec_termin`                      |
   +-----------------------+----------------------------------------+
   | Kalender              | :ref:`sec_termin`                      |
   +-----------------------+----------------------------------------+
   | Pressemitteilungen    | :ref:`sec_pressemitteilungen`          |
   +-----------------------+----------------------------------------+
   | RSS-Feed              | :ref:`sec_rss`                         |
   +-----------------------+----------------------------------------+
   | BONNUS                | :ref:`sec_bonnus_portlet`              |
   +-----------------------+----------------------------------------+
   | Statisches            | :ref:`sec_statisches_text`             |
   | Portlet               |                                        |
   +-----------------------+----------------------------------------+
   | Sprachspezifisches    | :ref:`sec_bilingual_portlet`           |
   | Portlet               |                                        |
   +-----------------------+----------------------------------------+
   | True Gallery          | :ref:`sec_slideshow`                   |
   | Portlet               |                                        |
   +-----------------------+----------------------------------------+
   | Event Shopping Cart   | :ref:`sec_veranstaltung_buchen`        |
   +-----------------------+----------------------------------------+

Portlets werden ausgeblendet, wenn sie leer sind. Falls es beispielsweise keine
Nachrichten anzuzeigen gibt, hat das Nachrichtenportlet keinen Inhalt und wird daher gar
nicht eingeblendet. Ferner erscheint per Voreinstellung auf der Startseite kein
Navigationsportlet, da sein Inhalt erst mit Unterordnern beginnt (siehe Abschnitt
:ref:`sec_navigation_portlet`).

Viele der Portlets enthalten eine Liste bestimmter Artikel der Website, beispielsweise
der neuesten Nachrichten. Solche Listen umfassen aus Platzgründen nur wenige Einträge.
Sie enden jedoch mit einem Verweis, der Sie zu einer vollständigen Übersicht der zum
Portlet passenden Artikel führt. Das könnte etwa eine Liste aller Nachrichten auf der
Website sein.

Portlets werden grundsätzlich innerhalb eines Ordners vererbt. Sie können übergeordnete Portlets, die vererbt werden nicht anzeigen lassen - aber leider nicht "mischen", d.h. entweder übernehmen Sie alle übergeordneten Portlets komplett oder Sie deaktivieren alle und müssen dann händisch die einzeln benötigten erneut anlegen.

Artikelspezifische Portlets (z.B. das Terminportlet) sind portalübergreifend und lassen sich nicht auf einzelne Ordner einschränken. Wenn Sie Einschränkungen benötigen, müssen Sie über Kollektionen und Kollektions-Portlets gehen.
