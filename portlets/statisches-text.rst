.. index:: Statisches Text Portlet
.. _sec_statisches_text:

=======================
Statisches Text Portlet
=======================

Im Gegensatz zu den meisten anderen Portlets wird der Inhalt eines statischen Portlets
nicht bei jeder Anzeige neu bestimmt. Stattdessen handelt es sich um einen frei
formatierbaren Text, der bei der Konfiguration des Portlets bearbeitet und bei der
Anzeige unverändert wiedergegeben wird (siehe Abbildung :ref:`fig_statisches_portlet`).

.. fig_statisches_portlet_hinzufuegen:

.. figure:: ./images/portlet-statisches-text-hinzufuegen.*
   :width: 80%
   :alt: Das statische Portlet

   Statisches Portlet erstellen

.. _fig_statisches_portlet:

.. figure:: ./images/portlet-statisches-text-anzeigen.*
   :width: 40%
   :alt: Das statische Portlet

   Statisches Portlet

Außerdem kann jedes statische Portlet einen Verweis auf weiterführende Informationen
anzeigen.

Nicht nur der Inhalt statischer Portlets ist frei wählbar, sondern auch ihre Gestaltung:
Jedes statische Portlet hat einen eigenen Text in Kopf- und Fußzeile, wobei die Fußzeile
gar nicht angezeigt wird, wenn sie leer gelassen wurde. Weiterhin müssen statische
Portlets nicht unbedingt mit einem Rahmen dargestellt werden.