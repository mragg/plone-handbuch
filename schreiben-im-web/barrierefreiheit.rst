.. _sec_barrierefreiheit:

================
Barrierefreiheit
================

Begriffsdefinition
==================

Barrierefreiheit bezeichnet im deutschen Sprachgebrauch eine Gestaltung der …
Umwelt in der Weise, dass sie von Menschen mit Behinderung … in derselben Weise
genutzt werden kann wie von Menschen ohne Behinderung. Im außerdeutschen
Sprachgebrauch wird dieser Zustand eher als „Zugänglichkeit“ (engl.: Accessibility)
bezeichnet. (Wikipedia)

Barrierefreies Internet sind Web-Angebote, die von allen Nutzern unabhängig von
körperlichen oder technischen Möglichkeiten uneingeschränkt (barrierefrei) genutzt
werden können.
Barrierefreiheit schließt also sowohl Menschen mit und ohne Behinderungen als auch
Benutzer mit technischen oder altersbedingten Einschränkungen sowie Webcrawler ein,
mit denen Suchmaschinen den Inhalt einer Seite erfassen.
Die Informationen, die die Universität im Internet bereitstellt müssen barrierfrei zugänglich sein. Viele Internetseiten sind nicht barrierefrei, da sie

* Elemente verwenden, die nicht dem HTML-Standard entsprechen,
* multimediale Inhalte ohne Ersatzvariante für Behinderte enthalten,
* schlecht strukturiert sind, – …

Web Content Accessibility Guidelines (WCAG) 2.0
===============================================

Die Richtlinien für barrierefreie Webinhalte (WCAG) 2.0 decken einen großen Bereich von Empfehlungen ab, um Webinhalte barrierefreier zu machen. Wenn Sie diesen Richtlinien folgen, dann werden Inhalte für eine größere Gruppe von Menschen mit Behinderungen barrierefrei sein. Dies beinhaltet Blindheit und Sehbehinderung, Gehörlosigkeit und nachlassendes Hörvermögen, Lernbehinderungen, kognitive Einschränkungen, eingeschränkte Bewegungsfähigkeit, Sprachbehinderungen, Photosensibilität und Kombinationen aus diesen Behinderungen. Darüber hinaus wird das Befolgen dieser Richtlinien Ihre Webinhalte in vielen Fällen für Nutzer im Allgemeinen benutzbarer machen.

Wahrnehmbar
-----------
* Stellen Sie Textalternativen für alle Nicht-Text-Inhalte zur Verfügung, so dass diese in andere vom Benutzer benötigte Formen geändert werden können, wie zum Beispiel Großschrift, Braille, Symbole oder einfachere Sprache.
* Stellen Sie Alternativen für zeitbasierte Medien zur Verfügung.
* Erstellen Sie Inhalte, die auf verschiedene Arten dargestellt werden können (zum Beispiel mit einfacherem Layout), ohne dass Informationen oder Strukturen verloren gehen.
* Machen Sie es für den Benutzer leichter, Inhalte zu sehen und zu hören, einschließlich der Trennung zwischen Vordergrund und Hintergrund.

Bedienbar
---------

* Sorgen Sie dafür, dass alle Funktionalitäten von der Tastatur aus verfügbar sind.
* Geben Sie den Benutzern ausreichend Zeit, Inhalte zu lesen und zu benutzen.
* estalten Sie Inhalte nicht auf Arten, von denen bekannt ist, dass sie zu Anfällen führen.
* Stellen Sie Mittel zur Verfügung, um Benutzer dabei zu unterstützen zu navigieren, Inhalte zu finden und zu bestimmen, wo sie sich befinden.

Verständlich
------------

* Machen Sie Textinhalte lesbar und verständlich.
* Sorgen Sie dafür, dass Webseiten vorhersehbar aussehen und funktionieren.
* Helfen Sie den Benutzern dabei, Fehler zu vermeiden und zu korrigieren.

Robust
------
* Maximieren Sie die Kompatibilität mit aktuellen und zukünftigen Benutzeragenten, einschließlich assistierender Techniken.

http://www.w3.org/Translations/WCAG20-de/WCAG20-de-20091029/

Irrtümer
========

Bei der Umstellung von Webseiten auf richtige Schwerpunktsetzung achten!

* Ich muss ab sofort zu jedem Bild einen Alternativtext erfinden. Falsch! Zu jedem Bild muss es ein Alt-Tag geben, der Inhalt dessen kann jedoch durchaus leer gelassen werden: alt=""
* Ich muss jedes Video durch einen langen Text ersetzbar machen. Falsch! Selten ist es möglich, ein Video tatsächlich durch Text erschließbar zu machen. Sinnvoller ist es, den Link zu einem Movie auf der Seite als solchen zu kennzeichnen.
* Ich darf keine Tabellen mehr verwenden.
  Falsch! Für tabellarische Inhalte dürfen auf jeden Fall weiterhin Tabellen verwendet werden. Außerdem sind sie zur Darstellung in manchen Browsern sinnvoll, da sonst unschöne Effekte entstehen (IE). Kriterium: Linearisierbarkeit
* Lange Listen sind in Ordnung, denn sie bestehen ja nur aus Text. Falsch! Inhalte gut strukturieren!
* Die Funktion "Schriftgröße ändern" behindert das Layout meiner Seite. Inhalt geht vor Layout
* Das Wort "Prüfungsänderungsordnung / ÄO" kennt doch jeder. Sprachwechsel deutlich machen! Abkürzungen vorher immer erläutern.
