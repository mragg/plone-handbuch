.. _sec_schreiben-im-web:

=================================
Schreiben im Web/Barrierefreiheit
=================================

Wenn Sie Texte und Informationen für das Internet aufbereiten, ist dies eine andere Form des Publizierens als im klassischen Print. Die folgenden Tipps sollen Anregungen sein, Texte für Internetseiten aufzubereiten.
Die Tipps in diesem Kapitel gelten weitgehend unabhängig vom verwendeten Content-Management-System. Auch dieser Teil des Handbuchs steht unter CC-Lizenz und kann gerne weiterverwendet werden - wir freuen uns über Ergänzungen, Anregungen!
Ein großer Dank geht an die HU-Berlin für den Input zum Kapitel Barrierefreiheit.

.. toctree::
   :maxdepth: 1

   tipps-zum-schreiben.rst
   technische-tipps.rst
   barrierefreiheit.rst
