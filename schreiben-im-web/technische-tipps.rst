.. _sec_technische-tipps-zum-schreiben:

================
Technische Tipps
================

Ein Großteil der Besucher eine Webseite gelangt über Suchmaschinen (Google & Co) zu Ihren Seiten. Suchmaschinen erfassen automatisiert Ihre Inhalte. Hier ist es wichtig, diesen Prozess zu unterstützen und Google & Co dabei zu helfen, relevante Inhalte auch als solche zu identifizieren.
Generell gilt: Sie schreiben für Ihre Leser und nicht für die Suchmaschine – trotzdem kommen viele Tipps sowohl der Suchmaschine wie auch den Lesern entgegen.

.. figure:: ./images/google-suche.*
   :width: 80%
   :alt: Google Suche



Seiten in Plone
===============

Überschrift
-----------

Das häufigste Objekt in Plone ist die Seite. Aus dem Feld „Titel“ wird die URL abgeleitet unter der die Seite später zu erreichen ist. Gleichzeitig ist das der „wichtigste“ Inhalt für eine Suchmaschine. Der Titel besteht idealerweise aus 2-5 Wörtern (kein aktives Prädikat) und dient als „Eyecatcher“.

Hier gilt:
So kurz wie möglich – so lang wie nötig.
Der Titel kann auch alleine, ohne den weiteren Kontext auftauchen, z.B. in der Ergebnisliste einer Suchmaschine.

Untertitel: Überschrift 2
-------------------------

Kurze, näherer Umschreibung der Überschrift. Hier können auch aktive Prädikate verwendet werden.

Beispiel: Gefährliche Sicherheitslücke (Überschrift) - Microsofts Outlook App schleust E-Mails über Fremd-Server (Untertitel)

Beschreibung
------------

In das Beschreibungsfeld kommt eine Zusammenfassung der Seite/des Ordners. Maximal zwei Sätze / 140 Zeichen. Ist das Beschreibungsfeld redundant zur Überschrift, kann es leer bleiben.

.. figure:: ./images/fckeditor.*
   :width: 60%
   :alt: Seite in Plone

Keywords
========
Überlegen Sie sich Keywords, d.h. wichtige Stichworte für Ihre Webseite. Das sind z.B: Stichwörter, die jemand bei Google eingibt und unter der Ihre Webseite gefunden werden soll.
Platzieren Sie diese Keywords richtig.

Das wichtigste Keyword sollte

* in der URL vorkommen
* in der Überschrift vorkommen (möglichst weit vorne)
* im ersten Absatz der Seite vorkommen

Alle Keywords sollten (mehrfach) im Text verwendet werden.

Mindest- und Maximallängen
==========================

Suchmaschinen verarbeiten vor allem Titel und Überschriften automatisiert und generieren daraus auch die Trefferansichten.
Berücksichtigen Sie dies auch mit der Länge von Titel und Überschriften.
Wie immer gilt: Das sind nur Empfehlungen.

* Titel: Minimum 40 Zeichen, Maximum 70 Zeichen (mehr zeigt der Treffer bei Google nicht an)
* 300 Wörter Minimum des Gesamt-Textes, Maximum?
* Mind. 1-2 ausgehende Links
* Mindestens eine Unterüberschriftenebene verwenden

Bilder im Web
=============

* Wenn Sie Bilder einfach nur neben dem Text anzeigen möchten, empfehlen sich Werte um die 300px größte Seitenlänge.
* Wenn Sie Bilder mit Großansicht (auch in der Galerie) anzeigen möchten , empfehlen sich Werte um die 1000px größte Seitenlänge.
* Denken Sie an Mobilgeräte/Kleine Bildschirme, Auflösungen: Geben Sie bei Bildern, die z.B. die gesamte Inhaltsbreite benutzen Prozentwerte an, statt Pixel. Sie skalieren die Bilder auf die vorhandene Breite.
Dies sind grobe Daumenwerte. Letztendlich hängt es von Bild selbst ab und wieviel Platz Ihnen zur Verfügung steht oder Sie dem Bild einräumen möchten.

Weitere Tipps
=============

* Strukturieren Sie die Seite mit Zwischenüberschriften, verwenden Sie hier die vorgesehenen Überschriftenformate – auf keinen Fall „händische Formatierungen“!
* keine Überschriftenformate in Text-Portlets verwenden
* keine Baustellenseiten
* kein „Herzlich Willkommen auf unserer Internetseite“, im Internet müssen Sie niemanden begrüßen
* Haben Sie an allen verwendeten Bilder die notwendigen Rechte?
* "Tabellenartige Inhalte" wie z.B. Stundenpläne, Statistiken etc. werden auch im Web mit Tabellen dargestellt. Achten Sie bei Tabellen darauf Spaltenüberschriften zu verwenden.
* Optische Strukturierung/Formatierung z.B. von Bildern darf nicht mit Tabellen umgesetzt werden
* wenn Sie eine Seite kopieren, Kurznamen bearbeiten – sollte die Seite in der URL nicht „copy of ...“ heißen
* nur einzelne Links im Fließtext, lagern Sie viele Links in ein Textportlet in die rechte Portletspalte aus
* Setzen Sie Zwischenüberschriften und Anker bei langen Seiten (mit der Option „Inhaltsverzeichnis“). Alternativ teilen Sie lange Seiten auf mehrere kurze Seiten auf.
* Nutzen Sie unser Statistik-Tool „Piwik“, um das Besucherverhalten auszuwerten. Sie erhalten hier auch Hinweise zu Suchbegriffen unter denen Sie bisher gefunden werden.
* Vermeiden Sie „Duplicate Content“, d.h. nicht denselben Inhalt auf mehreren Webseiten verwenden. Jeder Inhalt sollte eine einzige Stelle in Ihrem Webauftritt haben, an weiteren Stellen verlinken Sie den Inhalt entsprechend.
* sprechende Linkinhalte: Legen Sie einen Link immer auf einen kurze Beschreibung, was sich hinter dem Link verbirgt (keinesfalls: Weitere Informationen finden Sie >>hier<<)
* keine Textinhalte in Grafiken darstellen
