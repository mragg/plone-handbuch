.. _sec_tipps-zu-schreiben:

===================
Tipps zum Schreiben
===================

Verfolgen Sie eher das Ziel Informationen schnell erreichbar zu machen und weniger „optisch schön zu gestalten“. Inhaltlich gut aufbereitete Internetseiten führen meist zu einer besseren Bewertung durch Suchmaschinen und sind gleichzeitig fast immer barrierefrei.

Internetseiten werden "gescannt"
================================

Im Unterschied zu Print-Texten sind wir im Internet viel eher gewohnt Text zu „scannen“. Dieser wird kurz überflogen und nicht linear gelesen. Daher ist es wichtig, kurze und präzise Informationen zu liefern.

Erleichtern Sie den Lesern die Entscheidung mit wenigen Blicken festzustellen (z.B. durch Zwischenüberschriften), ob die Seite die für sie relevanten Informationen enthält und sich damit die intensivere Lektüre lohnt. Die Lesegeschwindigkeit am Bildschirm ist in der Regel auch um 1/3 langsamer als bei gedrucktem Papier.
Die wichtigsten Kurztipps für Ihre Texte im Web:

   Gliedern
      Das Wichtige kommt zuerst.

   Teasertext
      Bei längeren Texten kann ein kurzer, sogenannter Teasertext, vorangestellt werden, damit der Inhalt der Seite zusammengefasst wird und damit besser erfasst werden kann. Der Teasertext sollte idealerweise ca. 130-150 Zeichen umfassen (2-3 Sätze).

   Kürzen / Vereinfachen
      Kurze Sätze, klare Sprache, keine Abkürzungen, aktiv formulieren, geläufige Wörter verwenden (nicht zu technisch), keine überflüssigen Wörter/ Phrasen benutzen. Sollten Sie wichtige Texte nicht kürzen können, nutzen Sie bitte die „Einklappfunktion“ von Texten in Plone. So können bei Bedarf Informationstexte „ausgefahren“ werden und nehmen trotzdem wenig Platz ein (Formatierung in Plone: „Accordion Paragraph“).

   Formatieren
      Überschriften als solche kenntlich machen. Vermeiden Sie die Kursivschrift (unleserlich am Bildschirm) und zu viele Fettungen. Diese führen dazu, dass nichts mehr gelesen wird – auch die wichtigen Absätze nicht. Vermeiden Sie auch GROSSBUCHSTABEN und Wörter wie „ACHTUNG“. Im Web werden in der Regel keine Unterstreichungen vorgenommen!


      .. figure:: ./images/inhalts-dreieck.*
         :width: 80%
         :alt: Inhalte gliedern

         Inhalte gliedern

Formulierungsbeispiele
======================

.. |br| raw:: html

   <br />

.. list-table:: Formulierungsbeispiele I
   :widths: 15 30
   :header-rows: 1

   * - Besser
     - Nicht so schön
   * - Wie schreibe ich mich ein?
     - Modalitäten und Hinweise für die Einschreibung  |br|\ an unserer Universität
   * - Terminänderung: Antrittsvorlesung Prof. Schneider
     - Die Antrittsvorlesung von Prof. Schneider |br|\ "Frauenbilder in der Literatur des Mittelalters" |br|\ muss leider verschoben werden
   * - Studienberater: Ansprechpartner A-Z
     - Alphabetische Auflistung aller Studienberater
   * - Umzug, Adressänderung: Was muss ich tun?
     - Aufgaben der Universitätsverwaltung
   * - Ehemalige Teilprojekte |br|\ (Sonderforschungsbereich 651)
     - SFB 651: Ehemalige TPs
   * - Einschreibung: Vorschriften und Termine
     - Modalitäten und terminliche Vorgaben |br|\ zur Einschreibung an unserer Universität
   * - Bitte geben Sie Ihren Antrag im |br|\ Studentensekretariat ab.
     - Um sich einzuschreiben, müssen die |br|\ Studieninteressenten ihren Antrag im |br|\ Studentensekretariat abgeben

.. list-table:: Formulierungsbeispiele II
   :widths: 15 30
   :header-rows: 1

   * - Besser
     - Nicht so schön
   * - Er leitet das Institut
     - Er ist mit der Leitung des Instituts beschäftigt
   * - Sie vertritt ihn
     - Sie übernimmt seine Vertretung
   * - erwägen
     - in Erwägung ziehen
   * - überlegen
     - eine Überlegung anstellen
   * - Die Kunst, verständlich zu schreiben
     - Die Kunst der Verständlichkeit des Schreibens

*	Aktiv ist meistens besser als passiv!
*	Modalverben (dürfen, können, mögen, müssen, wollen, sollen) sollten ;-) vermieden werden
*	Ein Satz sollte nicht mehr als 20 Wörter haben (ideal sind im Durchschnitt 14-17 Wörter, „Atemlänge“)
*	3-Sekunden-Regel: Alles, was zusammengehört, steht weniger als 3 Sekunden voneinander entfernt = Zeit, in der der Durchschnittsleser 12 Silben bzw. 6-9 Wörter erfassen kann
* Ein Satz sollte nur eine Information enthalten
*	Ein Satz sollte keine nicht zuvor erläuterten Abkürzungen (z. B. „IKT“, „OEM“) enthalten
* Ein Satz sollte ein ausgewogenes Verhältnis von Verben und Substantiven aufweisen (1 Verb bei 16 Substantiven ist eindeutig zu wenig!)
*	Ideales Verhältnis Substantive:Verben = 3:1. Nicht mehr als 3 Substantive auf 1 Verb pro Satz
*	Der Nominalstil wirkt häufig unschön
*	Überflüssige Adjektive (z. B. „gesonderte“ vor „hochschulspezifische“) vermeiden
*	Hauptsätze sollten immer erste Wahl sein
*	Hauptaussage in den Hauptsatz – Nebensache in den Nebensatz
*	Schachtelsätze nach Möglichkeit entwirren

Meldungen schreiben
===================

.. figure:: ./images/meldung-schreiben.*
   :width: 80%
   :alt: Meldungen schreiben

   Meldungen schreiben

*	Ideale Länge: 1.000 – 1.500 Zeichen
*	Sie ist sachlich, kurz, informativ (keine Wertungen!)
*	Zeitformen: 	        > Perfekt oder Präsens: hat vorgestellt oder stellt vor

Berichte/ Fachartikel schreiben
===============================

.. figure:: ./images/berichte_schreiben.*
   :width: 80%
   :alt: Berichte/Fachartikel schreiben

   Berichte/Fachartikel schreiben

*	Länge: 2.000 – 5.000 Zeichen
*	Mögliche Themen: Z.B. laufende Projekte, Pläne, Ideen, Anwendungspotenziale, Empfehlungen, Projektergebnisse (aber nicht zwingend)
*	Ziel: Aufklären und erklären statt nur berichten (wie in Meldung)
*	Sachlich wie Meldung
*	Stilmittel wie Zitate, indirekte Rede erlaubt (bzw. erwünscht), um bloße Fakten zu bewerten/ einzuordnen (weg von rein sachlicher Ebene)

Insgesamt gilt
--------------

Zahlen als Wörter: > Zahlen 1 bis 12
  Zehner, Hunderter, Tausender: zwanzig, hundert, tausend
Zahlen als Ziffern:  > Zahlen ab 13
  Bruchzahlen: 5,75 statt fünfdreiviertel |br|\
    Wenn kleine und große Zahlen zusammentreffen (Vergleiche): |br|\
    Der Anteil stieg von 8 auf 23 Prozent statt |br|\
    Der Anteil stieg von acht auf 23 Prozent |br|\
    Vor Einheiten und ähnlichen Angaben: 12 Prozent; 1. Januar 2016 |br|\
    Telefonnummern, Hausnummern, Kontonummern etc.
Einheiten einheitlich gebrauchen:
  abkürzen: %, EUR, km oder ausschreiben: Prozent, Euro, Kilometer
Englische Begriffe einheitlich verwenden:
  mit Bindestrich: E-Mail-Adresse, Identity-Management-System ohne Bindestrich: Knowhow, Backup

Der Texteinstieg: Aufmerksamkeit erzeugen - Mut zur Kreativität!
================================================================

Mögliche Varianten:

Szenische Beschreibung:
  Stylisches Layout, einfacher Wechsel zwischen den Dokumenten und viele weitere neue Funktionen: Office 2016 für Windows ist da! ...
Schlagzeile:
  Office 2016 für Windows ist da: Und nicht nur das! Sie können es jetzt sogar über unseren MS Rahmenvertrag im Softwareshop kaufen. Aber ist der Einsatz wirklich ratsam? …
Zitat (von anwesender oder historischer Person):
  „Ob wir den flächendeckenden Einsatz von Office 2016 an der Universität Bonn demnächst empfehlen können, hängt noch von vielen Faktoren ab“, erklärt Andreas Beutgen, Mitarbeiter des Hochschulrechenzentrums der Universität Bonn. …
Sprichwort, Reim:
  Gut Ding will Weile haben: So ist es auch mit der Software. Obwohl Office 2016 frisch auf dem Markt ist und viele Versprechen mit sich bringt, können wir den Einsatz an der Universität Bonn noch nicht empfehlen! …
Wortspiel:
  Im Office doch lieber Writer statt Word? Noch immer können wir den Einsatz von Office 2016 für Windows nicht guten Gewissens empfehlen. …
Philosophisches, Weisheiten, Literarisches oder Anlehnungen daran:
  Zwar kann man, laut Watzlawick, nicht nicht kommunizieren - aber immerhin ohne Office 2016.  Von einem Gebrauch des neuen Microsoft-Produktes raten wir aufgrund folgender Risiken noch immer ab: ...
Vergleiche/Assoziationen quer durch alle Themenwelten (Natur/Tiere, Mathe, Sport, Menschen, Literatur, Architektur, Musik etc.) oder Kontraste:
  Nach rasantem Start doch erstmal Boxenstopp! Mit Office 2016 verspricht Microsoft seinen Nutzern viele tolle neue Funktionen. Wie funktionstüchtig und sicher das neue Office wirklich ist, erklären wir Ihnen anhand einiger Beispiele: …
Provokation/Frage:
  Sollten deutsche Hochschulen das neue Office 2016 jetzt schon nutzen? Diese Frage wird derzeit auch in Bonn kontrovers diskutiert. …
Rhetorische Frage:
  Wer kennt das Dilemma nicht? Ein neues Produkt des Lieblingsherstellers ist auf dem Markt, aber über den geschäftlichen Einsatz wird hitzig diskutiert. …
Lautmalerei (Comic-Sprache; Klangwörter):
  „Tja, so einfach wie anfangs angenommen, ist das mit Office 2016 wohl doch nicht“, erklärt Andreas Beutgen, Mitarbeiter des Hochschulrechenzentrums der Universität Bonn, während er sich mit der rechten Hand seufzend durch‘s Haar fährt.
Alliteration/Dreiklang (gleicher Anfangsbuchstabe oder –laut von Wörtern):
  Innovativ, intuitiv, informativ: Das neue Office 2016 ist da und auch wenn es so manch einem unter den Nägeln juckt, können wir den Einsatz noch nicht empfehlen.

Synonyme sorgen für Abwechslung:
  Erläutern, mitteilen, versichern, bestätigen, äußern, erzählen, betonen, verraten, feststellen, berichten etc. statt ausschließlich sagen.
