.. _sec_umgang:

=======================
Umgang mit Artikeltypen
=======================


In Kapitel :ref:`sec_artikeltypen` haben Sie erfahren, wie Sie den Inhalt
einzelner Artikel bearbeiten. Dieses Kapitel befasst sich damit, wie Sie mit
Artikeln als Teil Ihrer Website umgehen:


* Webinhalte erstellen, löschen, kopieren und verschieben
* frühere Versionen von Artikeln konsultieren
* Arbeitsabläufe nutzen
* Inhalte kommentieren und diskutieren
* RSS-Feeds von Ordnern und Kollektionen anbieten

.. toctree::
   :maxdepth: 1

   logo.rst
   artikelaktionen.rst
   versionierung.rst
   syndizierung.rst
   arbeitskopie.rst
   navigation-root.rst
   rechteuebersicht.rst
   webdav.rst
