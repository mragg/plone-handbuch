.. index:: Logo, Kopfgrafik
.. _sec_kopfgrafik_bearbeiten:

==============================
Logo und Kopfgrafik bearbeiten
==============================

Da die Ordner die Struktur Ihrer Webseite bilden, ist es logisch, dass sie die ganze
Seite in unterschiedliche thematische Bereiche aufteilen. In manchen Fällen können sie
sogar für verschiedene Abteilungen des Institutes stehen. Von daher ist es manchmal nötig
auch unterschiedliche Abbildungen im Kopfbereich haben so, dass in jedem Bereich bzw.
Ordner ein anderes Kopfbild und vielleicht sogar auch einen anderen Logo steht. Sie
können den Kopfbereich Ihres Ordners ganz einfach anpassen.


Auf der obersten Ebene
~~~~~~~~~~~~~~~~~~~~~~

Im Verzeichnis »Startseite« auf der obersten Seite müssen das Logo mit dem (Kurz-)Namen
»institutelogo« und die Kopfgrafik mit dem (Kurz-)Namen »folderimage« (ohne Dateiendung)
abgelegt werden. Das Logo der Universität kann nicht geändert werden. So abgelegte Bilder
sind durch das Prinzip der Vererbung auf Ihrem gesamten Portal sichtbar.


Innerhalb eines Ordners
~~~~~~~~~~~~~~~~~~~~~~~

In einem Ordner innerhalb Ihres Portals werden Logo und Kopfgrafik im Dialog
»Kopfgrafiken« unter »Bearbeiten« eingetragen. Wenn Sie die Bilder auf diese Weise
einbinden, wird die oben erwähnte Vererbung unterbrochen. Somit lassen sich die
unterschiedlichen Bereiche Ihres Internetauftritts auch optisch voneinander trennen.

Auf der Seite Plone-Support_ finden Sie viele Vorlagen der Pressestelle.

.. _Plone-Support: http://www.plone.uni-bonn.de/dokumentation/Kopfgrafiken


**Tipps & Tricks:**

In einigen Fällen stellt Ihr Portal als :ref:`sec_navigation_root` ein Unterverzeichnis
eines anderen Portals dar. Wenn das zutrifft, verhalten sich die Kopfgrafiken Ihrer
Startseite wie oben unter "Innerhalb eines Ordners" beschrieben. Ob Sie auf einer
Navigation Root arbeiten oder nicht, stellen Sie am einfachsten durch folgenden Klickweg
fest: »Startseite« → »Inhalte« → »bearbeiten«. Sollte dort der blaue Reiter
»Kopfgrafiken« auftauchen, können Sie die Grafiken dort bearbeiten. Falls nicht,
verfahren Sie wie bei »Auf oberster Ebene« beschrieben.

Das Entfernen der Dateiendung aus dem Kurznamen können Sie auch nach dem Upload der
Grafik vornehmen. Wählen Sie die Grafik aus und bei Aktionen »umbenennen« - dort kann
der Kurzname angepasst werden.