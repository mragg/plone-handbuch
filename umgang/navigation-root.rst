.. index:: Navigation Root
.. _sec_navigation_root:

===============
Navigation Root
===============

Eine Plone-Installation lässt sich mehrfach verwenden. Das spart Ressourcen und
administrativen Aufwand. Das Hilfmittel dazu ist die »Navigation Root«.

In jedem Unterordner Ihres Plone-Auftritts lässt sich ein »Navigation Root« definieren.
Ein Navigation Root kann nur vom Administrator gesetzt werden.



.. _fig_navigation_root:

.. figure:: ./images/navigation_root_update.*
   :width: 90%
   :alt: Navigation Root

   Navigation Root

Intern behandelt Plone den Ordner und alle seine Unterodner aber jetzt als eigenständigen
Webauftritt! Sie können eine vollständig neue Navigations- und Ordnerstruktur aufbauen,
die Standardsprache ändern und sogar eigene Kopfgrafiken verwenden.

Zusammen mit den Möglichkeiten, die wir mit :ref:`sec_rewrite` haben, lässt sich ein
Portal administrativ hervorragend untergliedern, gleichzeitig können massiv Ressourcen
auf den Servern eingespart werden.

Die einzige Einschränkung der »Navigation Root« ist, dass Sie die Einstellungen, die unter
»Konfiguration« vorgenommen werden, übernehmen müssen. Was sich in den meisten Fällen der
Anwendung allerdings eher als Arbeitserleichterung erwiesen hat.

Optionen des Navigation Root
============================
Innerhalb eines Navigation Root können Sie einige - nachfolgend erläuterte - Optionen neu setzen, die ansonsten global innerhalb eines Portals gelten.
Wenn Sie diese Optionen für ein Navigation Root neu definieren, gelten diese automatisch für alle Ordner und Objekte innerhalb des Navigation Roots.

.. _fig_navigation_root:

.. figure:: ./images/Root_Objekt_bearbeiten.*
   :width: 90%
   :alt: Navigation Root Optionen

   Navigation Root Optionen

Öffentliche URL
===============
Wenn für die Navigation Root eine eigene, von der Portal-URL abweichende URL geschaltet
wurde, sollten Sie diese hier (ohne Angabe des Protokolls, also www.xyz.uni-bonn.de) eintragen. Hierdurch werden Doppelindexierungen durch Suchmaschinen
wie Google vermieden.

Externer Such-Dienst
====================
Standardmäßig ist in allen Plone-Portalen als Suchfunktion die Suche über den externen Such-Dienst
(Mindbreeze Search Appliance - MSA) eingestellt. Dieser wird mit gewissen Vorgaben angesprochen.
Wenn Sie hier eigene Definitionen innerhalb eines Navigation Roots benötigen, sind diese hier einstellbar.
Bspw. kann der Namensraum der zu durchsuchenden Seiten eingeschränkt werden.
Wenn Sie hier Änderungen vornehmen möchten, müssen diese vorab mit dem Plone-Support abgesprochen sein,
da dies parallel auf Seiten der MSA auch konfiguriert werden muss.

Webstat Javascript Snippet
==========================
Zur Erstellung von Besucherstatistiken betreibt das HRZ einen eigenen Server auf Basis der Software Piwik.
Wenn Sie die Besucher innerhalb eines Navigation Roots separat zählen möchten, können Sie hier ein eigenes Piwik-Snippet eintragen.
Dies bekommen Sie durch den Plone-Betrieb eingerichtet und dann mitgeteilt.
Bitte beachten Sie, dass mit einer separaten Zählung der Besucher in einem Navigation Root diese dann *nicht* mehr im Gesamtportal mitgezählt werden.

.. _sec_sprachflagen_navigationroot:

Sprachumschalter
================

In einigen Fällen kann es sein, dass in einem Navigation Root die Sprachumschalter
(Flaggen im alten Theme, Kürzel im neuen Theme)
stören, da z. B. dieser Ordner nur in einer Sprache gepflegt wird.
Aus diesem Grund finden Sie in den Optionen des Navigation Root nun einen Schalter
zum Aktivieren bzw. Deaktivieren der Sprachumschalter.

Der früher vorhandene entsprechende Schalter in den Einstellungen der Ordner wurde entfernt.
Weiterhin gilt jedoch, dass auf der Root-Ebene eines Portals die Option derzeit wirkungslos ist.
Wenn Sie diese benötigen, wenden Sie sich bitte an uns (plone@uni-bonn.de).
Wir werden dann eine Lösung in Ihrem Portal einrichten.

Social Media Buttons
====================
Zu jedem Navigation Root können Sie einen eigenen Satz von Social Media Buttons definieren.
Zu beachten ist dabei jedoch, dass diese nur im *neuen Theme* der Uni Bonn auch angezeigt werden.

Geben Sie zu jedem Button einen geeigneten Namen, den Link zu Ihrem Social Media Angebot
sowie das passende Icon an. Mit den seitlichen Bearbeitungsschaltflächen können Sie Buttons hinzufügen,
entfernen sowie die Reihenfolge verändern.
