.. index:: Rechteübersicht, Cassandra
.. _sec_rechte_uebersicht:

================
Rechteübersicht
================

Das Zusatzprodukt Cassandra richtet sich nur an Administratoren der Plone-Webseiten.

Durch viele Benutzer und Gruppen auf einem Portal wird die Übersicht über eingeräumte
Rechte sehr schnell undurchschaubar. Cassandra bietet die einfache Möglichkeit auf der
Startseite und in Ordnern die Zugriffsrechte einzusehen. Dabei werden sowohl global
vergebene Rechte als auch Rechte von Benutzern in Ordnern (rekursiv) dargestellt.

Cassandra benötigt einigen Rechenaufwand - rufen Sie es nur zur Kontrolle der Rechte
gezielt auf.

Das Zusatzprodukt kann nicht aus einem Navigationroot heraus aufgerufen werden - als
Administrator rufen Sie es bitte oberhalb des Navigation-Roots auf.


Installation
============

Cassandra muss einmalig installiert werden über den Weg: Konfiguration → Zusatzprodukte → Installierbare Produkte

Aufruf von Cassandra
--------------------

Die Anwendung startet indem an eine beliebige URL ::

 https://www....de/.../@@cassandra

angehängt wird.
