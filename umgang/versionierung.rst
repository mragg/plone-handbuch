.. index:: Versionierung, History
.. _sec_undo:

=========================
Versionierung und History
=========================

Bitte beachten Sie, dass Versionierung und Mehrsprachigkeit nicht gleichzeitig verwendet werden können.

Es wird bei Ihrer Arbeit an der Website gelegentlich vorkommen, dass Sie
Änderungen zurücknehmen und zu einer früheren Version eines Artikels
zurückkehren möchten. Das ist in Plone mit Hilfe der Versionsverwaltung
möglich. Das CMS erstellt jedes Mal, wenn Sie einen Artikel verändern und
speichern, eine neue Version des Inhalts. Die alten Versionen des Artikels
werden dabei nicht überschrieben, sondern stehen weiterhin zur Verfügung.

Sie erreichen die älteren Versionen eines Artikels über die Ansicht
»Versionen« (siehe Abbildung :ref:`fig_ansicht_versionen`).

.. _fig_ansicht_versionen:

.. figure:: ./images/ansicht-versionen.*
   :width: 100%
   :alt: Die Ansicht, die ältere Versionen eines Artikels auflistet

   Die Ansicht »Versionen«

Dort finden Sie eine Tabelle vor, in der die aktuelle Arbeitskopie und die
vorherigen Versionen aufgelistet sind. Die Tabelle stellt Ihnen alle
Informationen zu den einzelnen Versionen übersichtlich in einzelnen Spalten
zur Verfügung.
