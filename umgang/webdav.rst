.. index:: WebDAV, Netzlaufwerk
.. _sec_netzlaufwerk:

=====================================
WebDAV - Webauftritt als Netzlaufwerk
=====================================

Sie können sich Ihren Webauftritt als Laufwerk unter Windows/Linux/MacOS einbinden, um
darüber komfortabel beispielsweise größere Uploads vorzunehmen.

**Bitte beachten Sie**, dass Plone nicht Ihren lokalen Fileserver ersetzen soll. Auch wenn
Speicherplatz heute in großer Menge verfügbar ist, nutzen Sie diesen »mit Augenmaß«.

»WebDAV« ist ein Protokoll, das den Upload von Dateien ermöglicht. Dabei wird die
Kommunikation über die normalen ``http``-Ports abgewickelt (s. auch: Wikipedia:
WebDAV_)

.. _WebDAV: http://de.wikipedia.org/wiki/WebDAV

Sie können sich Ihren Plone-Webauftritt via »WebDAV« als Laufwerk einbinden. Unter
Windows empfehlen wir Ihnen das Zusatzprogramm "Anyclient", das unter verschiedenen
Plattformen läuft und bisher einen sehr guten Eindruck macht. Vor allem läuft es direkt
unter Windows 7.

Den »Anyclient« gibt es für Windows, MacOS und Linux, Download direkt unter:
www.jscape.com/products/file-transfer-clients/anyclient/

Kurze Bedienungsanleitung:

* Nach dem Start klicken Sie auf »File« → »Sitemanager«
* Legen Sie dort mit »New« einen neuen Eintrag an
* Schalten Sie zuerst in dem neuen Eintrag unten den Connection Type auf »Webdav«
* Host ist Ihre Portal-URL, z. B.: ``https://www.portalname.uni-bonn.de/``
* Username & Password wie gewohnt Ihre Zugangsdaten zu HRZ-Diensten
* Abspeichern & mit Connect ausprobieren

Unter MacOS gibt es gute Erfahrungen auch mit dem freien »Client Cyberduck«:
http://cyberduck.ch/


Zugangsmöglichkeit
==================

Der Zugang ist nur via ``https`` möglich, die Webdav-URL ist identisch mit Ihrer Portal-URL.
Login und Passwort sind Ihre Uni-ID.

Sie haben über »WebDAV« dann die Möglichkeit sehr einfach im Dateibrowser Dateien bzw.
ganze Ordnerstrukturen zu übertragen.

Sprechen Sie sich auf jeden Fall mit uns ab, ehe Sie sehr große Datenmenge (mehrere
Gigabyte) in Ihr Plone hochladen.


Einschränkung
-------------

Wenn Sie Bilder via »WebDAV« hochladen verlieren Sie die Funktionalität, dass Plone Ihnen
mehrere Kopien in unterschiedlichen Auflösungen anlegt. Wenn Sie dies nutzen möchten,
müssen Sie die Bilder über die Webdialoge hochladen.


Support
-------

WebDAV-Anbindungen werden vom Plone-Support der Uni Bonn nicht in vollem Umfang
unterstützt. Wir können nicht sicherstellen, dass die derzeit funktionsfähigen
Anwendungen auch in zukünftigen Plone-Versionen in vollem Umfang zur Verfügung stehen.
