.. index:: Gruppen (automatisch)
.. _sec_autogroup:


==================
Dynamische Gruppen
==================

Mit diesem Werkzeug könnnen Administratoren auf Basis der Eigenschaften eines Benutzers automatisch Gruppen erzeugen.
Dies bietet sich vor allem als praktische Hilfe für den Aufbaus eines (institutsinternen) Intranets an. Eine Gruppe kann beispielsweise auf Basis der Institutsnummer der Mitarbeiter erzeugt werden.
Im Unterschied zu den normalen Plone-Gruppen wird mit diesem Tool erst beim Login des Benutzers entschieden ob er Mitglied dieser (dynamischen) Gruppe wird. Zur Verfügung stehen grundsätzlich die Eigenschaften auf Basis der ldap-Felder.
Das Tool bietet sich vor allem für den Aufbau von Intranets bzw. generell geschützten Bereichen zu. Mit dem Tool müssen Sie nicht mehr per Hand Listen von Uni-IDs für den Zugriff pflegen, sondern Sie können auf Basis von Eigenschafter der Benutzer (z.B. bestimmte Institutsnummer, oder Status Student, Status Mitarbeiter) automatisiert Rechte erteilen.
Ein weiterer Unterschied zur normalen Benutzergruppe in Plone ist, dass Sie nicht nachschauen können, wer Mitglied der Gruppe ist - dies wird dynamisch erst beim Login eines Benutzers immer wieder neu ausgewertet. Ein Triggern über alle Benutzer auf dem ldap ist aus Lastgründen explizit nicht implementiert.

Konfiguration
=============
Das Tool ist standardmäßig aktiviert. Sie finden die Konfiguration im Bereich der "Konfiguration von Zusatzprodukten". Der Eintrag dort lautet "Member Property To Group".

.. figure:: ./images/konfiguration-1.png
   :width: 60%
   :alt: Konfiguration
   :align: right

   Member Property To Group: Konfiguration

Jeder Eintrag besteht aus zwei Konfigurationsparamtern. In das Feld "Users Group Property Field" tragen Sie das ldap-Feld ein, das Sie abfragen möchten.
In das Feld "Mapped Groups" tragen Sie den Wert ein auf den hin geprüft werden soll und die Gruppe in die bei einem "Treffer" der Benutzer eingetragen werden soll.

Abfragbare ldap-Felder für "Users Group Property Field"
=======================================================
Folgende Felder können Sie adressieren

* departmentNumber = Institutsnummer
* usertype = Statusgruppe (Wertebereich: student, employee)
* eduPersonAffiliation = "Mitglied der Universität"
* radiusgroupname = (noch nicht freigegeben)

Syntax für "Mapped Groups"
==========================

Hier sind mehrere Einträge möglich, die sich dann alle auf das darüber abgefragte ldap-Feld beziehen. Jeder Eintrag kommt in eine eigene Zeile.
Bitte beachten Sie, dass das Tool selbst die Plone-Gruppe anlegt, sofern diese noch nicht vorhanden ist.
Die formale Syntax eines Eintrages ist: user-property-value|group-id|title|description|email

* user-property-value = Wertebereich, der geprüft wird; hier sind Ersetzungen mit dem Operator * möglich;
  Beispiel: 072* "matcht" auf alle Werte, die mit 072 beginnen
* group-id = Gruppen-ID, keine Sonderzeichen, keine Leerzeichen
* title = Name der Gruppe (taucht später in den Auswahldialogen auf)
* description = Beschreibung der Gruppe
* email = Mailadresse der Gruppe

.. figure:: ./images/konfiguration-2.png
   :width: 50%
   :alt: Konfigurationdetails
   :align: right

   Member Property To Group: Konfiguration Detail

Über "add property" können Sie weitere Einträge anlegen.
Aktuell sind nur Ersetzungen auf Basis "*" möglich, zudem sind derzeit keine Verknüpfungen von mehreren Paramtern möglich. Prinzipiell kann das Tool in dieser Richtung ausgebaut werden. Wenn Sie hier Bedarf haben, wenden Sie sich bitte an den Plone-Support.

Beispiel
========
"Users Group Property Field" = departmentNumber
"Mapped Groups" = 072*\|hrz-mitarbeiter\|hrz-mitarbeiter\|HRZ Mitarbeiter\|\ plone@uni-bonn.de

Dieser Eintrag wertet beim Login eine Benutzers die Institutsnummer (departmentNumber) aus. Beginnt diese mit 072 wird der Benutzer in die Gruppe "HRZ Mitarbeiter" eingetragen.

Ein simples Szenario für ein Intranet könnte ein Privater Ordner sein, der damit für Anonyme Benutzer nicht sichtbar ist. Dort würden Sie unter Zugriff der Gruppe "HRZ-Mitarbeiter" das Recht "Ansehen" zuweisen.
Der große Vorteil ist, dass Sie keine Liste in Plone über Mitarbeiter-Uni-IDs mehr pflegen müssen. Der Intranet-Ordner ist automatisch für alle Uni-IDs, die der Institutsnummer 072* zugeordnet sind zugreifbar.

.. note::
   Entfernen Sie nicht den Haken bei "Berechtigungen von übergeordneten Ordnern übernehmen".
   Die Mitglieder der dynamischen Gruppe erhalten sonst keine ausreichenden Berechtigungen
   für den Ordner.
   
