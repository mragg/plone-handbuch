.. index:: Bookingtool
.. _sec_bookingtool:


===========================
Einrichten des Bookingtools
===========================

Das Booking-Tool ist ein Werkzeug, um ganz allgemein gesprochen eine Reservierung/Buchung von Objekten vorzunehmen.
Die Reservierungen werden dabei in einem Kalender dargestellt.

.. figure:: ./images/kalender.png
   :width: 60%
   :alt: Accordions Beispiele
   :align: right

   Booking-Tool: Kalender-Übersicht

Das Booking-Tool verhält sich dabei wie ein Ordner. Sie können beliebig viele "Booking-Ordner" innerhalb Ihres Auftritts anlegen.
Innerhalb eines Booking-Ordners können dabei auch mehrere auszuleihende Elemente gruppiert werden. So können Sie z.B. auch einen Gerätepool abbilden.
Es gibt innerhalb des Booking-Tools dann diverse Parameter über die Sie z.B. unterschiedliche Workflows steuern können, Ausleihzeiten, Urlaubszeiten und auch die Standardansichten bearbeiten können.

Das Bookingtool ist explizit für kleinere Ausleihen gedacht - im Standard ist eine vorherige Anmeldung in Plone Voraussetzung, d.h. Sie sollten das Tool normalerweise nur für interne "Booking-Vorgänge" einsetzen.

.. figure:: ./images/hinzufuegen.png
   :width: 100%
   :alt: Accordions Beispiele
   :align: right

   Booking-Tool: Hinzufügen

**Installation**
Das Booking-Tool muss zuerst vom Administrator des jeweiligen Plones installiert werden.
Nach der Installation können Sie in jedem Ordner einen Buchungsordner erzeugen.
Über den Punkt „hinzufügen“ aus der Bearbeitungsleiste wählen Sie "reservierungen" aus.

Standardkonfiguration
=====================

Zu Beginn bleibt man im „Standard“ und legt fest:

Titel:
 Name des Buchungskalenders (ist in der Menüleiste erkennbar)
Beschreibung:
 kurze Erläuterung des Namens bzw. Zweck des Kalenders
Typen:
 bezeichnet die Hauptelemente die verliehen werden z.B. Mediensets, KFZ-Verleih,
 Besprechungsräume, untergliedern sich dann in die Kategorien
Kategorien:
 teilen die Typen auf, zum Beispiel:
Mediensets:
 Set A, B, C
KFZ-Verleih:
 Wagen X, Y, Z
Besprechungsräume:
 Raum 504, 608, 703

Die Einstellung zum Aktualisierungsmodus (automatische..) und Reservierungsmodus
(überprüfen) sollte man stehen lassen

Displaykonfiguration
====================

Im Anschluss wechselt man vom „Standard“ zum „Display“, hier werden Einstellungen zu den Kalenderansichten geregelt.

* Unterschieden wird zwischen einer Listenansicht und einer Kalenderansicht, hier kann mit
  dem Vorgabe-Ansichtsmodus festgelegt werden welche als Standard zuerst gezeigt wird
* Innerhalb dieser Listen- bzw. Kalenderansicht kann die Vorgabe für den angezeigten
  Zeitraum (Tag/Woche/Monat/Jahr) angepasst werden

Time Einstellungen
==================

Anschließend wird „time“ gewählt

* Tagesbeginn betitelt den Beginn ab wann Buchungen erstellt werden können (die
  Reservierungen können online zu jeder Uhrzeit erfolgen)
* Tagesende reguliert den spätesten Zeitpunkt einer Buchung
* Der Punkt minimaler Buchungszeitraum gibt die Zeitspanne an, die mindestens gebucht wird

Holiday, „Öffnungszeiten“ festlegen
===================================

Mit dem Punkt „holiday“ können Sie Zeiten festlegen , an dem keine Buchungen möglich sind

* Wochenenden
* Feiertage je nach Bundesland
* Individuelle Schließzeiten

E-Mail Einstellungen
====================

Die Ebene „Email“ dient der Einstellung der unterschiedlichen automatisierten Email-Texte
und den dazugehörigen Empfängern, hierfür stehen Ihnen unterschiedliche Variablen zur
Verfügung, die Sie innerhalb der E-Mails individuell einfügen können

Administrative E-Mail
---------------------

Zu Beginn muss eine administrative E-Mail Adresse festgelegt werden und eine zweite, die
eine Kopie aller administrativen E-Mails erhält. Im Namen der administrativen E-Mail
Adresse werden alle ausgehenden E-Mails Ihres Bookingtools / Kalenders versandt

Weitergehend werden die unterschiedlichen E-Mail Texte und Betreffzeilen definiert

1. administrativer E-Mail Text für eine zurückgezogene Buchung

2. administrativer E-Mail Betreff für eine zurückgezogene Buchung
3. administrativer E-Mail Text für eine neu eingestellte Buchung
4. Kunden E-Mail Betreff für eine neu eingestellte Buchung
5. Kunden E-Mail Text für eine neu eingestellte Buchung
6. Kunden E-Mail Betreff für eine akzeptierte Buchung
7. Kunden E-Mail Text für eine akzeptierte Buchung
8. Kunden E-Mail Betreff für eine abgelehnte Buchung
9. Kunden E-Mail Text für eine abgelehnte Buchung
10. Kunden E-Mail Betreff für eine zurückgezogene Buchung
11. Kunden E-Mail Text für eine zurückgezogene Buchung
12. administrativer E-Mail Betreff für eine veränderte Buchung
13. administrativer E-Mail Text für eine veränderte Buchung

Für die Betreffzeilen stehen Ihnen folgende Variablen zur Verfügung:

%(name)s
 Der Name der von der Buchenden Person angegeben wurden ist
%(email_customer)s
 E-Mail des Kunden
%(comment)s
 Kommentare die der Buchende im Kommentarfeld hinterlassen hat
%(start)s
 Beginn der Buchung (Datum und Uhrzeit)
%(end)s
 Ende der Buchung (Datum und Uhrzeit)
%(modified)s
 Veränderungen
%(phone)s
 Telefonnummer des Kunden
%(bookable_object_title)s
 Angegebene Buchungstitel
%(booking_url)s
 URL der Buchung
%(created)s
 Erstelldatum der Buchung

Für E-Mail Texte stehen Ihnen folgende Variablen zur Verfügung:

%(name)s
 Der Name der von der Buchenden Person angegeben wurden ist
%(email_customer)s,
 E-Mail des Kunden
%(comment)s
 Kommentare die der Buchende im Kommentarfeld hinterlassen hat
%(start)s
 Beginn der Buchung (Datum und Uhrzeit)
%(end)s
 Ende der Buchung (Datum und Uhrzeit)
%(modified)s
 Veränderungen
%(phone)s
 Telefonnummer des Kunden
%(bookable_object_title)s
 Angegebene Buchungstitel
%(booking_url)s
 URL der Buchung
%(created)s
 Erstelldatum der Buchung

Im Anschluss speichern. Ihr Bookingtool / Kalender wurde erstellt.

Reservierbare Objekte hinzufügen
================================

Hierfür klicken Sie auf der Menüleiste auf „hinzufügen“ und dort auf „reservierbares
Objekt“

Es öffnet sich folgende Maske, auszufüllen ist:

* Titel
* Ggfs. Beschreibung
* Reservierungstyp (ist im Punkt 2 „Standardkonfigurationen“ erstellt worden) auswählen
* Ggfs. Kategorie auswählen (ist im Punkt 2 „Standardkonfigurationen“ erstellt worden)
* Evtl. Text (weitere Beschreibung / Inhaltsangabe)

Diese einzelnen Objekte sind derzeit noch nicht reservierbar und müssen öffentlich gesetzt
werden um eine Buchung zu ermöglichen. Hierfür klicken Sie bitte auf Status und verändern
in mittels Mausklick auf veröffentlichen. Nachdem Sie dies für alle Objekte getan haben
hat sich die Ansicht Ihres Bookingtools wie folgt geändert.
