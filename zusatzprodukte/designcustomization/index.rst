.. index:: Design Customization, CSS, Aussehen
.. _sec_designcustomizaition:

====================
Design Customization
====================

[Zusatzprodukt ausgespielt am 08.Juni 2016.]

.. note::
   Bitte beachten Sie die Nutzungsbedingung des Ploneservice, dass alle 
   Webseiten dem von der Pressestelle der Uni Bonn
   beschlossenen :doc:`Corporated Design </aussehen>`
   folgen müssen.
   
   Als Faustregel können Sie sich merken: Optische Änderungen oder 
   Hinzufügungen außerhalb des Content-Bereichs sind nicht gestattet.

Dieses Zusatzprodukt erlaubt Administratoren portalweit eigene CSS-Regeln hinzuzufügen. Bitte beachten Sie, dass umfangreiche Kenntnisse in CSS vorausgesetzt werden. Dieses Handbuch bringt Ihnen das nicht bei.

Installation
============

Das Produkt finden Sie unter Konfiguration->Zusatzprodukte den Eintrag::

   unibonn.designcustomization [Versionsnummer]

(Hinter den jeweiligen Produktnamen ist die jeweils aktuelle Version aufgeführt.) Haken Sie dieses Produkt an, und klicken Sie unten auf :guilabel:`Installieren`. Anschließend finden Sie unter Konfiguration in der unteren Hälfte die Option `Design customization`.

.. note::
   Auch wenn es für Admins klickbar ist, ist es in unserem Setup *nicht
   vorgesehen*, Produkte zu deinstallieren. Lassen Sie die Deinstallation von
   Produkten bitte nur über den Support vornehmen!

Aufbau der CustomCSS
====================

Wählen Sie als Administrator unter Konfiguration die Option *Design customization*, werden Sie auf ein schlichtes Formular weitergeleitet. Hier haben Sie einmal den aktuellen Inhalt der CustomCSS und darunter einen Button zum Speichern.

Die CustomCSS erfordert eine spezielle Syntax, damit Plone diese richtig in seine Seiten einbauen kann. ::

  /* <dtml-with base_properties> */
  /* <dtml-call "REQUEST.set('portal_url', portal_url())"> */
  
  /* Hier ist Platz für Ihre zusätzlichen CSS-Regeln */
  
  /* </dtml-with> */

Dies ist die minimale Anforderung, was in der Custom-CSS stehen muss. Entfernen Sie einzelne Zeilen mit ``dtml``, kann es zu Fehlern kommen. Bitte geben Sie acht.

Darüber hinaus finden Sie unterhalb dieser Pflichtzeilen ein Kommentar zu vordefinierten Werten, die Sie für Ihre eigenen CSS-Regeln verwenden können aber nicht müssen.

Eine eigene Regel einfügen
==========================

Als Beispiel nehmen wir an, wir möchten einen neuen Stil für unsere Tabellen verfügbar haben. Dieser soll grün hinterlegte Spaltenüberschriften haben und in der ersten Spalte eine leicht kleinere Schriftgröße. Damit möchten wir auf unserer Seite dann eine Timetable für ein Event darstellen.

Wir fügen unsere neuen CSS-Regeln zwischen die dtml-Zeilen ein::

  /* <dtml-with base_properties> */
  /* <dtml-call "REQUEST.set('portal_url', portal_url())"> */
  
  table.bonn-table{
    width:100%;
  }
  table.bonn-table th{
    background-color:#009E49;
    color:           #ffffff;
  }
  table.bonn-table td:first-child{
    font-size: &dtml-fontSmallSize; ;
  }
  
  /* </dtml-with> */

Anschließend kann man die Änderung speichern und die neue Klasse `bonn-table` verwenden:

* Verwenden Sie für das Bearbeiten von Seiten den
  :doc:`Kupu-Editor </arbeiten-mit-plone/kupu.rst>`, 
  so können Sie in der Konfiguration unter der Option `Visueller Editor` die neue Tabellenklasse einrichten.
* Verwenden Sie für das Bearbeiten von Seiten den 
  :doc:`FCK-Editor </arbeiten-mit-plone/fck.rst>`, 
  so ist Ihre einzige Möglichkeit beim Bearbeiten in die Quellcodeansicht zu wechseln und dort händisch bei der Tabelle
  ``class=“bonn-table“``
  einzutragen.
