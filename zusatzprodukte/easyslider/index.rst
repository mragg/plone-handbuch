.. index:: EasySlider, Bildergalerie
.. _sec_easyslider:

============================
Slideshow mit EasySlider
============================


Übersicht
=========

Wenn Sie oder Ihr Administrator EasySlider unter Konfiguration/Zusatzprodukte installiert haben, steht Ihnen für jeden Artikeltyp - zum Beispiel Ordner und Seiten - unter Aktionen die Option "add slider" zur Verfügung. Im dann folgenden Dialog kann der Sliderbereich selbst konfiguriert und in diesen mehrere "Slides" eingefügt werden.

Benutzung
==========

Artikeln Slider hinzufügen
++++++++++++++++++++++++++
Suchen Sie sich einen Artikeltyp, zum Beispiel eine Seite, aus und wählen Sie im Redaktionsmenü den Dropdown "Aktionen" und dort die Option "add slider". Jeder Artikel kann entweder keinen oder genau einen Slider haben.

.. _fig_add_easyslider:

.. figure::
   ./images/slider-hinzufuegen.png
   :width: 100%
   :alt: Ein Klick auf Optionen im Redaktionsmenü öffnet das Dropdown.

   Unter Aktionen können Sie den EasySlider einfach hinzufügen.

Slidersettings
+++++++++++++++++
Ihnen stehen für den Slider eine Vielzahl von Optionen zur Verfügung.

Width + Height
---------------

Ein Slider sitzt immer oberhalb der Artikelbeschreibung und unterhalb des Artikeltitels. In den Slidersettings kann die Breite und die Höhe des Feldes eingestellt werden, indem sich die Slides bewegen sollen.
Es empfielt sich bei der Höhe sich an der Höhe der längsten Slide zu orientieren. Beachten Sie, dass der EasySlider als Zusatzprodukt nicht an das "responsive theme" der Uni Bonn gekoppelt ist und nur feste Breiten unterstützt. Beachten Sie auch die Option "Centered?" weiter unten.

Show it?
----------
Entfernen Sie diesen Haken, um EasySlider auszublenden. Dabei wird er nicht gelöscht, sondern kann zu einem späteren Zeitpunkt wieder eingeblendet werden.

Effect Type
-------------
Hier kann der Übergangseffekt zwischen zwei Slides eingestellt werden, Sie haben zwei Optionen.

Slide
*****
Der Standard. Die Slides wechseln horrizontal.

Fade
*****
Die Slides blenden ineinander über.

Vertical?
-----------
Ist der Haken gesetzt wechseln die Slides nichtmehr horrizontal, sonder vertical durch.

Speed
------
Die Dauer des Überganges in Millisekunden.

Odd Slide Speed
----------------
Die Dauer des Überganges für jede zweite Slide. Lassen Sie diesen Wert auf Null, um die normale Dauer zu verwenden.

Auto?
------
Setzen Sie hier den Haken für eine automatische Slideshow, die nach einer gewissen Zeit zur nächsten Slide wechselt.

Pause
------
Wie lange die Slides angezeigt werden sollen, bevor automatisch zur nächsten Slide gewechselt werden soll. Die Angabe ist wieder in Millisekunden.

Odd Slide Pause
----------------
Wie lange jede zweite Slide angezeigt werden soll. Die Option entspricht der Option "Odd Slide Speed".

Continuous?
-----------
Soll am Ende der Slideshow wieder von vorne begonnen werden?

Centered?
-----------
Legt fest, ob der Easy Slider zentriert werden soll, wenn die eingestellte Breite kleiner ist, als die zur Verfügung stehende Breite.

Type of Navigation
------------------
Es gibt vier mögliche Optionen, um festzulegen inwiefern der Benutzer die Slideshow steuern kann.

Big Arrows
**********
Zeigt links und rechts zwei große Buttons.

Small Arrows
************
Zeigt in der unteren linken Ecke zwei Buttons.

Navigationbuttons
*****************
Zeigt am unteren Rand zentriert zwei Buttons um vorwärts und rückwärts zu schalten und dazwischen shortcuts zu den einzelnen Slides.

No Buttons
**********
Zeigt keine Navigation für den Benutzer an.


Navigation Buttons Rendering Type
----------------------------------
Legen Sie hier fest, ob für manche Slides die Navigation ausgeblendet werden soll.

Fade Navigation?
----------------
Ist der Haken gesetzt erscheint die Navigation nur, wenn der Benutzer die Maus über die Slides bewegt.

Resume Play
------------
Soll die Slideshow nach kurzem Warten weiter gehen, wenn der Benutzer manuell eine Slide ausgewählt hat? Im Normalfall wird die Slideshow pausiert.

Easy Templates Enabled?
------------------------
EasyTemplates sind in unserer Installation nicht vorhanden und stehen nicht zur Verfügung,

Slides
+++++++
ToDo
