.. index:: Zusatzprodukte
.. _sec_zusatzprodukte:

==============
Zusatzprodukte
==============

Viele Zusatzprodukte stehen Ihnen bei Plone zur Verfügung, doch nicht jedes ist schon
installiert. Hier erfahren Sie, wie sie vorgehen.

Als Administrator eingeloggt klicken Sie auf Konfiguration.

Unter »Website-Konfiguration« finden Sie den Punkt »Zusatzprodukte«.

.. _fig_zusatzprodukte_instalieren:

.. figure:: ./zusatz_1.*
   :width: 80%
   :alt: Zusatzprodukte installieren

   Zusatzprodukte installieren

Auf der folgenden Seite erhalten Sie dann eine Übersicht, über alle Produkte, die zur
Verfügung stehen oder bereits installiert sind. Setzen Sie einen Haken beim gewünschten
Produkt und klicken Sie auf Installieren. Dieser Vorgang kann einige Zeit in Anspruch
nehmen.

Nun können Sie unter »Konfiguration von Zusatzprodukten« die gewünschten Einstellungen an
Ihrem Produkt vornehmen.

.. _fig_zusatzprodukte_uebersicht:

.. figure:: ./zusatz_2.*
   :width: 80%
   :alt: Zusatzprodukte konfigurieren

   Zusatzprodukte konfigurieren

.. toctree::
   :maxdepth: 1

   linguaplone/index.rst
   soaplsf/index.rst
   portlet-page/index.rst
   video/index.rst
   easyslider/index.rst
   massloader/index.rst
   qComments/index.rst
   bookingtool/index.rst
   window/index.rst
   autogroup/index.rst
   theme2016/index.rst
   designcustomization/index.rst
