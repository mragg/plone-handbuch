.. index:: Mehrsprachigkeit, Sprache
.. _sec_linguaplone:

================================
Mehrsprachigkeit mit LinguaPlone
================================

Sprachenordner
==============
Auf neueren Plone-Auftritten werden unterschiedliche Sprachen in sprachenspezifische Ordner abgelegt (/de, /en). Dieses Verhalten ist inzwischen üblich, dadurch ist der URL einer Webseite die Sprache in der das Dokument verfasst wurde anzusehen.
Ältere Plone-Auftritte haben diese Ablage noch nicht, wenn Sie einen älteren Auftritt auf die neue Methodik umstellen wollen, wenden Sie sich bitte an den Plone-Support.

Mehrsprachige Inhalte lesen
===========================

Wenn auf der Plone-Website die Erweiterung LinguaPlone installiert
wurde, ist es möglich, Inhalte in verschiedenen Sprachen anzubieten.
Sie finden zur Auswahl der gewünschten Sprachversion oben rechts
direkt unterhalb des Benutzermenüs Verweise mit den möglichen Sprachen
(siehe Abbildung :ref:`fig_sprache_auswaehlen`), in denen Inhalte
vorliegen können. Dabei kann es sich je nach Konfiguration der Website
um reine Textverweise oder um kleine Fähnchen handeln.

.. _fig_sprache_auswaehlen:

.. figure:: ./images/lingua-uebersetzen-in-menue.png
   :width: 50%
   :alt: Links zur Auswahl der Sprache

   Auswahl der Sprachversion

Wenn Sie einem dieser Verweise folgen, erscheint die gesamte Website
in der ausgewählten Sprache: die Bedienungselemente der
Benutzeroberfläche ebenso wie der Inhalt des Artikels, auf dem Sie sich
befunden haben, als Sie die Sprache wechselten.

Falls der Artikel nicht in der Sprache vorliegt, die Sie gewählt
haben, werden Sie stattdessen zur Startseite der Website oder zum
nächst höher gelegenen Ordner in der gewünschten Sprache
weitergeleitet. Von dort aus können Sie sich anhand der Navigation neu
orientieren.

Mehrsprachige Inhalte erstellen
===============================

Beim Aufbau einer mehrsprachigen Website sind folgende Punkte zu beachten.
Bei neueren Plone-Portalen werden jeweils eigene Ordner für die Sprachen angelegt, d.h.
alle Dokumenten einer Sprachen sind in einem gemeinsamen Unterordner untergebracht:

# www.plone.uni-bonn.de/de für deutsche Seiten des Plone-Supports
# www.plone.uni-bonn.de/en für englische Seiten des Plone-Supports

#. Die Benutzeroberfläche der Website muss in alle Zielsprachen
   übersetzt werden. Diese Arbeit hat Ihnen die Plone-Community bereits
   abgenommen. Das Content-Management-System Plone unterstützt mehr
   als 50 Sprachen. Falls Sie Veränderungen an der Übersetzung der
   Benutzeroberfläche vornehmen möchten, informieren Sie sich auf
   http://plone.org über das Thema Internationalization_.

#. Es muss für jede Sprachversion ein entsprechender Navigationsbaum,
   also eine eigene Ordner-Hierarchie aufgebaut werden, damit sich die
   Besucher in ihrer Muttersprache auf der Website zurechtfinden. Wie
   Sie dies machen, wird in diesem Kapitel beschrieben.

#. Und schließlich müssen die Inhalte selbst in den gewünschten
   Sprachen vorliegen. Die Arbeitsschritte, die hierfür notwendig
   sind, werden ebenfalls in diesem Kapitel beschrieben.

Aufbau einer mehrsprachigen Navigation
======================================

Um eine mehrsprachige Navigation aufzubauen, müssen die Ordner,
mit denen eine Ordnerhierarchie aufgebaut wurde, übersetzt
werden. Soll die gesamte Website mehrsprachig aufgebaut werden, müssen
bereits die Ordner im Wurzelverzeichnis von Plone dabei einbezogen
werden. Der Aufbau einer mehrsprachigen Navigation muss daher in der
Regel von einem Administrator durchgeführt werden.

Sobald eine Website mehrsprachige Inhalte verwaltet, bekommt die
Zuordnung von Artikeln zu einer bestimmten Sprache eine wichtige
Bedeutung. Es werden nämlich nur solche Artikel von der Erweiterung
LinguaPlone als Sprachversion erkannt, die nicht
`sprachunabhängig` sind. Wenn Sie beispielsweise die englische
Sprachversion der Website betrachten, werden alle Inhalte, die einer
anderen Sprache angehören unterdrückt und nicht angezeigt. Im
Umkehrschluss bedeutet dies, dass nur sprachunabhängige Inhalte in
allen Sprachen zur Verfügung stehen.

Wenn Sie einen Artikel übersetzen möchten, gehen Sie daher zunächst
ins Teilformular »Kategorisierung« (siehe Kapitel
:ref:`sec_teilf_kateg`) des Artikels. Kontrollieren Sie dort die
Stellung des Auswahlmenüs :guilabel:`Sprache`. Wenn
»Sprachunabhängig (voreinstellt)« auswählt ist, wird der Artikel nicht
als eine bestimmte Sprachversion erkannt. Bei Bildern mag dies
sinnvoll sein, bei Artikeln, die übersetzt werden, sollte immer eine
Sprache gesetzt werden. Ordnen Sie also den Artikel vor der
Übersetzung der Ausgangssprache zu. In unserem Beispiel ist dies »Deutsch«.

Sobald Sie LinguaPlone installiert haben, erfolgt beim Anlegen neuer
Inhalte automatisch eine Zuordnung des Artikels zu der aktuell
ausgewählten Sprache, in der die Website betrachtet wird. Wenn Sie
sprachunabhängige Inhalte erstellen wollen, müssen Sie die
Voreinstellung entsprechend ändern.

.. index:: Übersetzung

Übersetzung eines Ordners
-------------------------

Wenn LinguaPlone installiert ist und Sie Artikel in der Website
bearbeiten dürfen, finden Sie im grünen Rahmen das Menü
:guilabel:`Übersetzen in...`. Wenn Sie das Menü aufklappen, sehen Sie
die Sprachen, in die Artikel der Website übersetzt werden können. In
unserem Beispiel in Abbildung :ref:`fig_ordner_uebersetzen` ist dies
Englisch und Französisch.

.. _fig_ordner_uebersetzen:

.. figure::
   ./images/ordner-uebersetzen.png
   :width: 100%
   :alt: Anzeige eines Ordners mit geöffnetem Auswahlmenü

   Auswahlmenü zur Übersetzung

Wenn Sie eine der verfügbaren Sprachen auswählen, gelangen Sie zu
einem Bearbeitungsformular (siehe Abbildung
:ref:`fig_uebersetzen_ordner`), das auf der linken Seite den Ordner in
der Originalsprache (Deutsch) zeigt und auf der rechten Seiten die
entsprechenden Bearbeitungsfelder für die Übersetzung anbietet. Im
Übrigen ist das Formular genauso aufgebaut wie die Bearbeitungsansicht
eines Ordners. Sie haben wie in der normalen Bearbeitungsansicht in
verschiedenen Teilformularen die Möglichkeiten den Ordner mit Metadaten zu
kategorisieren. Geöffnet ist das Teilformular »Standard«, auf dem Sie den Titel
und die Beschreibung des Ordners eingeben können. Wenn Sie ins Englische
übersetzen, befinden Sie sich in der englischen Sprachversion eines Artikels.
Die Benutzeroberfläche von Plone erscheint entsprechend in Englisch.

.. _fig_uebersetzen_ordner:

.. figure::
   ./images/lingua-uebersetzen-standard.png
   :width: 100%
   :alt: Zweigeteilte Bearbeitungsansicht des Ordners Kochseminar: links in
         Deutsch, rechts sind Formularfelder zur Eingabe der Übersetzung.

   Zweigeteilte Bearbeitungsansicht des Ordners »Kochseminar«

Geben Sie die Übersetzung für Titel und Beschreibung ein und betätigen
Sie die Schaltfläche :guilabel:`Save`. Sie gelangen danach zur Anzeige
des Ordners.

Der Ordner ist zunächst leer. LinguaPlone merkt sich, vom welchem
deutschsprachigen Ordner der Ordner eine Übersetzung darstellt. Wenn
Sie Artikel in dem Ursprungsordner übersetzen, werden die
Übersetzungen automatisch in dem richtigen Ordner der Zielsprache
gespeichert.

Wechseln Sie daher zurück in den deutschsprachigen Originalordner,
indem Sie in der Sprachauswahl den Verweis (Textlink oder Fähnchen)
zur deutschen Sprachversion anklicken. Sie befinden sich dann wieder
im deutschen Ordner.

Sie werden feststellen, dass der Kurzname des Ordners von LinguaPlone
automatische geändert wurde, indem der Original-ID ein »-en« angehängt
wurde. Aus :file:`kochseminar` wurde :file:`kochseminar-en`. Dies ist
nicht immer wünschenswert. Schalten Sie also gegebenenfalls die
Bearbeitung von Kurznamen ein, sodass Sie gleich bei der Übersetzung
uch einen passenden Kurznamen festlegen können oder ändern Sie nach
der Übersetzung die Kurznamen der übersetzten Artikel in sinnvoller
Weise über das Menü :guilabel:`Aktionen` oder in der Inhaltsansicht
der Ordner.

Der Status des übersetzten Ordners entspricht dem allgemeinen Anfangsstatus von
Ordnern auf der Website. In der Voreinstellung ist dies der Status »privat«.
Die Arbeitsabläufe der einzelnen Übersetzungen sind voneinander unabhängig.

Ebenso unabhängig vom Originalartikel ist die Historie des übersetzten
Artikels.

Übersetzung einer Kollektion
----------------------------

Neben Ordnern sind Kollektionen wichtige Gliederungselemente einer
Plone-Website. Auch sie lassen sich mit LinguaPlone übersetzen. Die
Übersetzung erfolgt – wie bei Ordnern und allen anderen Artikeln – in
einer zweigeteilten Bearbeitungsansicht (siehe Abbildung
:ref:`fig_uebersetzen_kollektion`).

.. _fig_uebersetzen_kollektion:

.. figure:: ./images/uebersetzen-kollektion.png
   :width: 80%
   :alt: Zweigeteilte Bearbeitungsansicht einer Kollektion

   Zweigeteilte Bearbeitungsansicht einer Kollektion

Auf der linken Seite des Bearbeitungsformulars sehen Sie die Version
der Ausgangssprache, auf der rechten Seite können Sie die Übersetzung
eingeben. Dies gilt für alle Teilformulare der Bearbeitungsansicht.

Bei der Einstellung der Kriterien gibt es keine zweigeteilte
Ansicht. Bedenken Sie, dass es in der Voreinstellung kein
Suchkriterium »Sprache« bei Kollektionen gibt. Ein solches Kriterium
kann jedoch im Konfigurationsmenü für Kollektionen (siehe Kapitel
:ref:`sec_konfiguration_kollektionen`) hinzugefügt werden.

Der Status der übersetzten Kollektion entspricht dem Anfangsstatus von
Kollektionen auf der Website, in diesem Falle »privat«.

.. index:: Übersetzung

Übersetzung von Artikeln
========================

Die Übersetzung der anderen Artikeltypen erfolgt in gleicher Weise. Um einen
Artikel zu übersetzen, rufen Sie ihn zunächst in der Originalsprache auf.
Wählen Sie im Auswahlmenü :guilabel:`Übersetzen in...` die gewünschte Sprache
aus und füllen Sie die Felder in der zweigeteilten Bearbeitungsansicht
entsprechend aus.

Einige Formularfelder stehen Ihnen in der Bearbeitungsansicht einer
Übersetzung nicht zur Verfügung. So lässt sich das Freigabe- und
Ablaufdatum einer Übersetzung nicht verändern (siehe Abbildung
:ref:`fig_uebersetzen_teilf_dates`)

.. _fig_uebersetzen_teilf_dates:

.. figure::
   ./images/uebersetzen-teilf-dates.png
   :width: 100%
   :alt: Das Teilformular »Datum« in einer Übersetzung

   Das Teilformular »Datum« in einer Übersetzung

Man kann Übersetzungen auch nicht unabhängig vom Original von der
Navigation ausschließen (siehe :ref:`fig_uebersetzen_teilf_settings`).

.. _fig_uebersetzen_teilf_settings:

.. figure::
   ./images/uebersetzen-teilf-settings.png
   :width: 100%
   :alt: Das Teilformular »Einstellungen in einer Übersetzung

   Das Teilformular »Einstellungen« in einer Übersetzung

Folgende weitere Einschränkungen sollten Sie bei der Übersetzung
beachten:

Seite
   Beim Artikeltyp »Seite« werden im Teilformular »Einstellungen« die
   Vorgaben des Originals beim »Präsentationsmodus« und dem
   »Inhaltsverzeichnis« übernommen und können in Übersetzungen nicht
   geändert werden.

Nachricht
   In der Übersetzung einer Nachricht wird das Titelbild des Originals
   übernommen und kann nicht ausgetauscht werden.

Termin
   Datum und Uhrzeit eines Termins, die Teilnehmer und der
   Veranstaltungstyp werden vom Original unveränderbar übernommen.

Datei
   Beim Artikeltyp »Datei« können nur Titel, Beschreibung und die
   übrigen Metadaten verändert werden, die Datei selbst ist nicht austauschbar.

Bild
   Das Bild im Artikeltyp »Bild« kann ebenfalls in Übersetzungen nicht
   ausgetauscht werden.

Verwaltung mehrsprachiger Inhalte
=================================

Das Menü :guilabel:`Übersetzen in...` besitzt unterhalb der Liste der
verfügbaren Sprachen den Eintrag :guilabel:`Übersetzungen
verwalten`. Sie gelangen über diesen Verweis zu einem Formular (siehe
Abbildung :ref:`lingua_uebersetzungen_verwalten`) mit dem Sie

* die Sprachzuordnung einer Übersetzung verändern können

* beliebige Artikel in der Website als Übersetzung des aktuellen
  Artikels auswählen können und

* die angezeigte Übersetzungen löschen können.

.. _lingua_uebersetzungen_verwalten:

.. figure::
   ./images/uebersetzungen-verwalten.png
   :width: 100%
   :alt: Formular zur Verwaltung von Übersetzungen

   Verwaltung von Übersetzungen

Sprache ändern
--------------

Wenn der angezeigte Artikel irrtümlicherweise als englische
Übersetzung deklariert wurde, in Wirklichkeit aber die französische
Übersetzung ist, können Sie hier die Sprachzuordnung ändern.

Wählen Sie die gewünschte Sprache aus und betätigen Sie die
Schaltfläche :guilabel:`Sprache ändern`.

Übersetzung verknüpfen
----------------------

Wenn Sie bei der Planung Ihrer Website die einzelnen Sprachversionen
anders verwalten möchten als LinguaPlone dies vorgibt oder wenn
Inhalte ohne die Zuhilfenahme von LinguaPlone bereits übersetzt worden
sind, können Sie unter dieser Überschrift den Original-Artikeln die
entsprechenden Übersetzungen zuordnen.

Wählen Sie zunächst die Zielsprache aus und betätigen Sie die
Schaltfläche :guilabel:`Durchsuchen`, um den Artikel, der als
Übersetzung dienen soll, in der Website zu suchen. Es öffnet sich ein
Auswahlformular (siehe Abbildung :ref:`fig_uebersetzung_verknuepfen`),
das genau so zu bedienen ist, wie das im Kapitel
:ref:`sec_teilf_kateg` beschriebene Formular zur Auswahl eines
Verweises.

.. _fig_uebersetzung_verknuepfen:

.. figure::
   ./images/uebersetzung-verknuepfen.png
   :width: 100%
   :alt: Auswahlformular

   Formular zur Auswahl und Suche eines Artikels

Übersetzungen entfernen
-----------------------

Wenn Sie Übersetzungen entfernen möchten, markieren Sie unter der Überschrift
»Übersetzungen entfernen« die Sprachversion, die entfernt werden soll.
Anschließend haben Sie die Möglichkeit den Artikel, der die Übersetzung
enthält, endgültig zu löschen oder nur die Verknüpfung als Übersetzung zu
lösen. Zum Löschen betätigen Sie die Schaltfläche :guilabel:`Löschen`, um die
Verknüpfung als Übersetzung zu lösen :guilabel:`Unlink`.

.. attention:: Wenn Sie auf :guilabel:`Löschen` klicken, wird der
   Artikel, der als Übersetzung fungiert, in der Website tatsächlich
   gelöscht.

Das Formular zur Verwaltung von Übersetzungen steht nur dann
vollständig zur Verfügung, wenn es von dem Artikel in der
Ursprungssprache aus aufgerufen wird. Wenn Sie das Formular von einer
Übersetzung aus aufrufen, stehen Ihnen aus Sicherheitsgründen nicht
alle Verwaltungsmöglichkeiten zur Verfügung. Die Ursprungssprache wird
auch als `kanonische Sprache` bezeichnet, da nur in ihr
sämtliche Bearbeitungs- und Verwaltungsmöglichkeiten zur Verfügung
stehen, bei den abgeleiteten Übersetzungen jedoch nicht.

.. _fig_nicht_kanonische_sprache:

.. figure::
   ./images/nicht-kanonische-sprache.png
   :width: 80%
   :alt: Eingeschränktes Formular zur Verwaltung von Übersetzungen

   Eingeschränktes Formular

Wenn Sie alle Verwaltungsmöglichkeiten zur Verfügung haben wollen,
müssen Sie zur kanonischen Sprache wechseln. Ein entsprechender
Verweis befindet sich unten auf dem Formular.

.. _Internationalization: http://plone.org/documentation/manual/plone-community-developer-documentation/i18n/

Verhalten bei "Keine Übersetzung vorhanden"
-------------------------------------------
Es gibt zwei unterschiedliche Möglichkeiten, wie sich der Plone-Auftritt verhalten soll, wenn es für eine Seite keine Übersetzung gibt.
Über den Klick auf den Sprachumschalter wird die Übersetzung der jeweiligen Zielsprache angefordert.

* im Standard wird - falls keine Übersetzung vorhanden ist - eine Fehlerseite ausgegeben. Auf dieser wird ausgegeben, dass es keine Übersetzung für diese Seite gibt.

* Alternativ kann eine "Traversierung" angeschaltet werden. Dann wird in der Baumstruktur des Webauftritts weiter nach oben gegangen, bis eine übersetzte Seite gefunden wird und diese wird dann ausgegeben. Das kann in einigen Fällen ein sinnvolles Vorgehen sein - hängt aber sehr von der Struktur und den Inhalten des Webauftritts ab. Bitte überlegen Sie vorher genau ob Ihre Datenstruktur dafür geeignet ist und gehen exemplarisch Fälle durch. Das Verhalten kann nur durch den Plone-Support angepasst werden - die Konfiguration ist im ZMI erforderlich: "use_translated_parent_lookup" aktivieren unter portal_properties / linguaplone_properties
Bitte wenden Sie sich an den Plone-Supprt, wenn Sie das Verhalten aktivieren möchten.
