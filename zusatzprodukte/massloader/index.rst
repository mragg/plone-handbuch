.. index: MassLoader
.. _sec_massloader_konfiguration:

==========
MassLoader
==========

Nach der Installation können Sie das Produkt für die Redakteure optimieren. Ohne die
Seite mit Zusatzprodukten zu verlassen, finden Sie im linken Portlet die Auflistung
alle installierte Produkte. Gehen Sie auf :guilabel:`MassLoader`, damit das Produkt
konfigurieren.

.. _fig_massloader_konfiguration:

.. figure:: ./images/massloader-zusatzprodukt.*
   :width: 50%
   :alt: Zusatzprodukt MassLoader
   
   Zusatzprodukt :guilabel:`MassLoader`



* Erste Einstellung ist die maximale ZIP-Archivgröße. Die Archivdateien, die diesen Wert
  überschreiten, werden nicht hochgeladen. Achten Sie auch darauf, dass außer diesem Wert
  der Server noch eigene Einstellungen hat, wo die Upload-Zeit begrenzt ist. Wenn Sie die
  maximale Größe zu hoch setzen, kann es nach etwa 20 sek. Uploadzeit zu einer
  Fehlermeldung kommen.

* Die nächste Einstellung lässt die Plone-Artkeltypen bestimmen, wo :guilabel:`MassLoader`
  verwendet wird. Wenn Sie nur den Artikeltyp »Ordner« wählen, wird
  :guilabel:`MassLoader` nur für die Plone-Ordner aktiviert.

* Treat Images like Files: Lässt die Bilddateien aus ZIP-Archiv im Plone nicht als Bilder,
  sonder als ganz normale Dateien speichern. Im Endeffekt hat diese Option keine Wirkung
  aufs Verhalten und Funktionalität der Bilddateien.

* Weitere zwei Einstellungen lassen die Archivdatei und ihre Inhalte zu bestimmte
  Plone-Artikeltypen konvertieren. »Portal Type for file« = ``"Datei"`` und
  »Portal Type for folder« = ``"Ordner"`` heißt, dass das Archiv als ein
  normaler Plone-Ordner gespeichert wird, dabei bekommt der Ordner automatisch den Namen
  des ZIP-Archives und seine Inhalte werden als Dateien gespeichert.


.. figure:: ./images/massloader-einstellungen.*
   :width: 90%
   :alt: Konfiguration von MassLoader
   
   Konfiguration von MassLoader
   
