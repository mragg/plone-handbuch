.. index:: Portlet-Page
.. _sec_portlet_page:

==============
Portlet - Page
==============

.. note::
   Das Produkt "Portlet-Page" wird unter dem neuen :ref:`Uni Bonn Theme 2016 <sec_new_theme>` nicht mehr
   unterstützt. Bitte verwenden Sie stattdessen den Artikeltyp :ref:`Collage <sec_collage>`,
   welcher Portlet-Pages komplett ersetzen kann.

   Auch wenn Sie noch mit dem alten Theme arbeiten, empfiehlt es sich, das Produkt nicht
   mehr einzusetzen. Dadurch wird ein späterer Wechsel auf das neue Theme erleichtert.

Eine Portlet Page ermöglicht die Kombination verschiedener Plone-Elemente auf einer Seite mit dunkelblau hinterlegten Überschriften. Wenn Sie eine Portlet Page hinzufügen, können Sie den Titel, die Beschreibung und den Haupttext hinzufügen und später noch bearbeiten.

.. _fig_portletpage-hinzufuegen:

.. figure:: ./images/portletpage-hinzufuegen.*
   :width: 80%
   :alt: PortletPage hinzufügen

   PortletPage hinzufügen

.. _fig_portletpage-hinzugefuegt:

.. figure:: ./images/portletpage-hinzugefuegt.*
   :width: 80%
   :alt: Leeres Portletpage

   Portletpage

Rechts neben "bearbeiten" im Redaktionsmenü finden Sie "manage portlets". Nun können Sie verschiedene Portlets hinzufügen. Wählen Sie dazu im dropdown-Menü das gewünscht Portlet aus.

Ein "Statisches Text Portlet" eignet sich, um neue Redaktionsinhalte hinzuzufügen und mit dem Editor zu bearbeiten. Die übrigen Portlets erhalten ihre Informationen aus bestehenden Inhalten. Die Möglichkeiten entsprechen den Portlets in den Portletspalten rechts und links und ähneln sich auch in der Darstellung. Lesen sie hierfür VERWEIS. Sie können die Position der Portlets mit den kleinen Pfeilen am rechten Rand verändern.

.. figure:: ./images/portletpage-manage-portlets.*
   :width: 80%
   :alt: Portlets hinzufügen

   Portlets in Portletpage hinzufügen


.. figure:: ./images/portletpage-anzeigen.*
   :width: 80%
   :alt: PortletPage anzeigen

   Portletpage anzeigen
