.. index:: Soap-LSF, BASIS, Vorlesungsverzeichnis
.. _sec_soaplsf:

=========================================
BASIS - Vorlesungsverzeichnis mit SoapLSF
=========================================


Mit SOAP-LSF gibt es die Möglichkeit, Veranstaltungsdaten aus dem Vorlesungsverzeichnis
BASIS in Plone darzustellen. (siehe Abbildung :ref:`fig_soaplsf`) 

.. _fig_soaplsf:

.. figure:: 
   ./images/soap-lsf-uebersicht.*
   :width: 90%
   :alt: BASIS-Daten in Plone via Soap-LSF eingebunden

   BASIS-Daten in Plone darstellen


Die Installation des Zusatzproduktes erfolgt wie in Kapitel
:ref:`sec_konfiguration_erweiterungen` beschrieben.


Hinzufügen von Soap-LSF
=======================

Um eine Verknüpfung zum Vorlesungsverzeichnis BASIS herzustellen, muss Ihr
Plone-Administrator einmalig das Produkt SOAP-LSF installieren (steht auf allen Portalen
zur Verfügung).

BASIS ist dabei immer das führende System, d.h. Plone stellt die Daten nur dar, Änderungen
müssen immer in BASIS vorgenommen werden. Im Hintergrund werden die Veranstaltungsdaten
jede Stunde automatisch abgeglichen (eine Änderung in BASIS wird damit u.U. erst nach
einer Stunde in Plone angezeigt).

In jedem Ordner können Sie dann über Hinzufügen → SoapLSF Vorlesungsverzeichnisinhalte
anzeigen lassen. Sie können beliebig viele Soap-LSF-Objekte anlegen (und z.B. an einer
Stelle nur auf die Unterüberschrift "Exkursionen" oder ähnliches verweisen.

Wenn Sie Daten aus dem Vorlesungsverzeichnis einbinden, stimmen Sie sich bitte unbedingt
mit dem BASIS-Verantwortlichen in Ihrem Bereich ab - die Nutzung der
Vorlesungsverzeichnis unterliegt je nach Fach und Studiengang ganz unterschiedlichen
Bedingungen.

.. _fig_add_soap_lsf:

.. figure:: 
   ./images/soap-lsf-uebersicht.*
   :width: 90%
   :alt: Soap-LSF hinzufügen

   Soap-LSF hinzufügen


In jedem SoapLSF gibt es einen Knopf Aktualisieren - damit können Sie den Plone-Cache
außerhalb des 60-Minuten-Rhythmus direkt überschreiben.

Nutzungsbedingungen:
====================

Die Plone-Integration von BASIS-Daten kann nur dauerhaft angeboten werden, wenn sich alle
an die nachfolgenden Nutzungsbedingungen halten - bei Nichteinhalten behalten wir uns ein
Deaktivieren des Tools bzw. der betroffenen Seite vor. Verweisen Sie nur auf Ihre eigenen
Überschriften/Studiengänge bzw. die notwendigen Überschriftenbereiche, bitte keinesfalls
auf das Gesamtvorlesungsverzeichnis! Verweisen Sie nicht auf in BASIS nicht
freigeschaltete Semester bzw. nur dann, wenn dies mit den BASIS-Verantwortlichen in Ihrem
Bereich abgesprochen ist.

Sobald Sie ein Soap-LSF-Objekt angelegt haben, tragen Sie bitte einen eindeutigen Titel
und eine Beschreibung ein (hier empfiehlt es sich z.B. auf das Semester zu verweisen).

Wählen Sie dann ein Semester aus - sobald Sie dies getan haben, werden dynamisch die
Überschriften aus dem Vorlesungsverzeichnis nachgeladen. Sie können jetzt beliebig tief
in BASIS einsteigen.

Wichtig ist zu wissen, dass Soap-LSF alle Daten (Unterüberschriften incl. aller
Veranstaltungen) ab dem gewählten Knotenpunkt zieht und in Plone verfügbar macht. Wählen
Sie also z.B. die hohe Ebene Geographie -> Bachelor aus, wird der komplette Baum ab dieser
Stelle dargestellt.

In jedem SoapLSF gibt es einen Knopf Aktualisieren - damit können Sie den Plone-Cache
außerhalb des 60-Minuten-Rhythmus direkt überschreiben.

Das Soap-LSF Produkt zeigt jetzt auch die Links an, die in BASIS einer Veranstaltung
zugeordnet sind. Weiterhin wird der neue Termintyp "täglich" unterstützt.

Zum Schluss noch ein herzlicher Dank an die Plone-Kollegen der `Universität Freiburg`_,
die das Plone-Produkt Soap-LSF entwickelt haben und das freundlicherweise zur Verfügung
stellen.

.. _`Universität Freiburg`: http://www.cms.uni-freiburg.de/

