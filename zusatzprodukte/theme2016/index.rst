.. index: Uni Bonn Theme 2016
.. _sec_unibonn_theme2016:

===================
Uni Bonn Theme 2016
===================

.. warning::
   **Vorher testen**

   Das neue Theme sollte zuerst in der Testumgebung ausprobiert werden.
   Der Plone-Support hilft hierbei gerne.

   **One Way Theme-Wechsel**

   Tests im Vorfeld empfehlen sich ausdrücklich, da ein späteres "Zurück-Wechseln"
   zum alten Theme nicht möglich ist!

Installation
============

Das neue Theme 2016 wird als Zusatzprodukt zur Verfügung gestellt und installiert.
Sie finden als Administrator unter Konfiguration->Zusatzprodukte den Eintrag::

   unibonn.theme2016

Haken Sie dieses Produkt an, und klicken Sie unten auf :guilabel:`„Installieren“`. Mit der Installation
des Themes wird dieses automatisch aktiv. Gleichzeitig werden hierbei die Produkte
"Doormat", "unibonn.fontawesome" und "unibonn.startseite" mitinstalliert, falls diese
vorher noch nicht installiert waren. (Hinter den jeweiligen Produktnamen ist die jeweils aktuelle Version aufgeführt).

.. note::
   Auch wenn es für Admins klickbar ist, ist es in unserem Setup *nicht
   vorgesehen*, Produkte zu deinstallieren. Lassen Sie die Deinstallation von
   Produkten bitte nur über den Support vornehmen!

Anlegen einer Portalseite
=========================

Navigieren Sie in den Ordner, in dem die Portalseite angelegt werden soll. Hängen Sie
an die URL in Ihrem Browser folgendes an::

   create_default_startpage

Daraufhin wird Ihnen eine Startseite (eine Collage) mit vordefinierten Elementen
angelegt. Sie finden diese Seite ganz normal unter "Inhalte" in einem Ordner und können
diese z. B. problemlos als "Startseite" eines Ordners definieren.

Elemente einer Portalseite
==========================

Slider
------
Der Slider ist eine Option direkt an der Collage der Portalseite. Sie finden dort rechts
außen den Registereintrag :guilabel:`„slider settings“`.

.. _fig_slider:

.. figure:: ./images/Slider.*
   :width: 85%
   :alt: Portalseite - Slider

   Portalseite - Slider

Klicken Sie auf :guilabel:`„slider settings“`. Lassen Sie die oberen Einstellungen auf den
vorgeschlagenen Standardwerten. Sie finden ganz unten die jeweiligen Slides.
Sie könne dort einen neuen Slide hinzufügen, die Reihenfolge bestehender Slides
ändern sowie bestehende Slides editieren.

Ein Slide besteht jeweils aus drei Feldern:

Slide
   Fügen Sie in dieses Feld das Bild ein. Das Bild muss vorher außerhalb der
   Collage hochgeladen werden (nicht an dieser Stelle im FCK hochladen!). Das
   hochgeladene Bild sollte eine *Breite von 1160 Pixeln* haben. Beim Einfügen
   des Bildes nehmen Sie dann die Größe "full". Für die Höhe des Bildes gibt es
   keine Vorgabe

Slide Overlay
   In dieses Textfeld kommt der Text, der in der Ausgabe in das Feld kommt, das
   über das Sliderbild geblendet wird. Die Überschrift ist dabei schlicht Text mit
   der Auszeichnung "bold" bzw. "fett". Nach dem Fließtext besteht die Möglichkeit, einen
   "Link" zu setzen, welcher in der Ausgabe als Button dargestellt wird.

Overlay orientation
   Hier bestimmen Sie die Position des Textfeldes über dem Sliderbild (links,
   rechts, oben, unten).

Feature Buttons
---------------
In der Zeile unter dem Slider finden Sie die Feature Buttons.

.. figure:: ./images/Feature_Buttons_01.*
   :width: 100%
   :alt: Feature Buttons auf der Portalseite

   Feature Buttons auf der Portalseite

Die Feature Buttons bestehen jeweils aus einem Symbol, einer Beschriftung und einem
dazugehörigen Link.
Beim initialen Anlegen einer Portalseite werden hier schon fünf Feature Buttons
angelegt. Sie können diese dann einfach ändern. Gehen Sie dazu auf die Startseite
und dort auf :guilabel:`„Zusammensetzen“`.

.. figure:: ./images/Feature_Buttons_02.*
   :width: 40%
   :alt: Feature Button im "Zusammensetzen"-Modus

   Feature Button im "Zusammensetzen"-Modus

Sie finden dort die schon angelegten Feature Buttons. Um einen Button zu ändern,
klicken Sie darüber auf :guilabel:`„Bearbeiten“`.

.. figure:: ./images/Feature_Buttons_03.*
   :width: 50%
   :alt: Feature Button bearbeiten

   Feature Button bearbeiten

Ein Feature Button besteht aus dem Titel (der späteren Beschriftung), einem
Link und dem Symbol.

Der Link ist ein "interner Link", d. h., wenn Sie hier auf eine
externe Seite verlinken wollen, müssen Sie vorher in Ihrem Portal ein "Link Objekt" in
einem Ordner angelegt haben. Das Linkobjekt enthält die externe URL, und der
Feature Button verweist auf das interne Linkobjekt. Wenn der Feature Button auf eine
interne Seite verweisen soll, was der Normalfall sein dürfte, benötigen Sie dieses
Konstrukt natürlich nicht.

Bei dem Symbol haben Sie die komplette Auswahl aus dem "Awesome-Font". Eine Übersicht
der darin enthaltenen Icons finden Sie unter `Font Awesome`_.
Jedes Icon hat dort eine Bezeichnung (z. B. "bluetooth", "percent" etc.). Tragen Sie
diesen Namen in das Feld :guilabel:`„Feature button“` ein. Sie erhalten eine kleine Vorschau
auf das Icon.

.. _Font Awesome: https://fortawesome.github.io/Font-Awesome/icons/

Newsbereich und Portletspalte
-----------------------------
Im mittleren Teil der Portalseite ist standardmäßig der Newsbereich untergebracht.
Dieser zeigt automatisch News (vormals Pressemitteilungen) an. Wenn Sie einen
solchen Bereich noch nicht haben, diesen aber einrichten möchten, wenden Sie sich
bitte an den Plone-Support.

Im rechten Bereich gibt es eine "Portletspalte", die hier innerhalb der Collage
abgebildet ist. Das heißt, diese Portlets laufen nur rechts neben den News und nicht wie
sonst üblich über die gesamte Seite. Die hier definierten Portlets werden ausschließlich
auf dieser Portalseite angezeigt.

Kampagnen
---------
Im unteren Bereich der Portalseite finden Sie die sogenannten Kampagnen. Diese
bestehen aus einer Überschrift, einem Bild und einem kurzen beschreibenden Text.
Die Logik der Kampagnen ist analog zu den Feature Buttons, d. h., mit Anlegen der
Portalseite wurden dort vier "Dummy-Einträge" vorgenommen, die Sie jeweils auf Ihre
Bedürfnisse hin abändern können.

.. note::
   **Was nicht benötigt wird, kann entfernt werden.** Wenn Sie einzelne Elemente der
   Portalseite nicht benötigen, können Sie diese einfach aus der Collage löschen.
   Dies ist jedoch nicht rückgängig zu machen, d. h., wenn Sie entfernte Elemente
   später doch benötigen, müssen Sie wieder mit einer neuen Portalseite beginnen.

Sitemap / Doormat
=================
Ein weiteres Element des neuen Themes, das sich global über alle Seiten erstreckt,
ist eine redaktionell gepflegte "Sitemap" am Ende jeder Seite. Die
Produktbezeichnung hierfür lautet "Doormat".
Sie legen diese Doormat zentral in Ihrem Portal an. Pro Navigation Root kann es
dann jeweils eine neue Doormat geben.

Sie finden unter :guilabel:`„hinzufügen“` einen neuen Punkt :guilabel:`„doormat“`. Legen Sie hiermit ein
erstes Objekt an. Innerhalb der Doormat müssen Sie erst eine Struktur erzeugen.
Diese besteht zuerst aus „doormat columns“ (die Spaltenaufteilung) und innerhalb
einer Spalte dann aus „sections“. Eine Section ist schlussendlich ein Texteditorfeld.
Hier können Sie die Überschriften und die anzuzeigenden Links eintragen.
Es empfiehlt sich, die Doormat durch den Administrator verwalten zu lassen.

.. note::
   Berücksichtigen Sie, dass die Doormat auf jeder Seite unten angezeigt wird. Es sollte
   sich hier um redaktionell ausgewählte, strukturierte Bereiche handeln – die Doormat
   sollte i. d. R. keine vollständige Sitemap sein.
