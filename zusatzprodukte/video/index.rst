.. index:: Video, YouTube
.. _sec_eigene_youtube_kanaele_integrieren:

==============
Youtube-Kanäle
==============

In jedem Plone-Portal steht ein Zusatzprodukt zur Verfügung, das YouTube-Videos als selbstständigen Artikeltyp verwenden lässt.

.. index:: Channel-ID
.. _sec_channel-id:

Channel-ID
==========

Im Standard ist Kanal-ID für uni-bonn.tv hinterlegt. Dies führt dazu, dass Sie im Standard in der Auswahlliste Videos von uni-bonn.tv sehen. Sie können die Liste aber auch auf andere Youtube-Kanäle erweitern.

Direkt in YouTube können Sie die ID des Kanals nicht sehen. Es gibt aber einen simplen externen Webdienst, der
Ihnen zu jedem Youtube-Kanal die Channel-ID ausgibt. Tragen Sie dazu auf folgender Seite
`http://kid.yt.j.pfweb.eu/`_ die URL zum Youtube-Video in das entsprechende Feld ein,
durch Klick auf "ID ermitteln" bekommen Sie die Youtube-Channel-ID. Kopieren Sie sich
diese am besten in die Zwischenablage. Diese Channel ID soll unter
"Zope-Management-Oberfläche" eingetragen sein.

.. _fig_youtube_chanel_zmi:

.. figure:: ./images/youtube-01.*
    :width: 65%
    :alt: YouTube-Kanäle

    YouTube-Kanäle

Diese Einstellungen kann nur noch vom Plone-Support vorgenommen werden, bitte wenden Sie sich an plone@uni-bonn.de
Bitte schicken Sie uns die gewünschten Einstellungen per Mail an den Support.

Gehen Sie unter "Konfiguration" in die "Zope-Management-Oberfläche". Suchen Sie sich in der
Liste den Eintrag "portal_properties" und klicken darauf. Klicken Sie jetzt auf den
Eintrag "cwx_youtube_properties". Auf der jetzt angezeigten Seite sehen Sie im Feld
youtubequeries die eingetragenen Youtube-Kanäle. Hier können Sie weitere Einträge
hinzufügen. Es empfiehlt sich für einen neuen Eintrag zuerst den Eintrag von Uni Bonn TV
zu duplizieren und dann die Änderungen vorzunehmen. Der Eintrag für UniBonn TV sollte so
aussehen: ::

  Uni Bonn TV|{"channelId":"UCz6A1uBoVvw2mj3HJsqbo4A", "order":"rating"}

Ändern Sie den Titel am Anfang der Zeile, die Angabe dort erscheint später im Dropdownfeld
im Texteditor. Tragen Sie dann die Channel-ID ein. Achten Sie darauf, dass die Channel-ID
durch Anführungszeichen " " umschlossen ist. Klicken Sie zum Schluss auf "Save Changes"
und speichern damit den Eintrag. Der neue Kanal sollte jetzt im Dropdown zur Verfügung
stehen.


.. _`http://kid.yt.j.pfweb.eu/`: http://kid.yt.j.pfweb.eu/
