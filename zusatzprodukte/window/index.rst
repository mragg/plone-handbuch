.. index:: iFrame
.. _window:

==================
Window - iframes
==================

Das Zusatzprodukt »Window« ermöglicht es in Plone Webseiten, die auf einem anderen Webserver liegen über einen »iframe« in die eigene Seite zu integrieren. Es ist im Standard nicht installiert und muss erst vom Administrator der Plone-Site freigegeben werden.
In einigen Fällen kann diese Funktion sehr praktisch sein, wenn es beispielsweise zu kompliziert ist, die Funktionalität der außerhalb Plones liegenden Webseite in Plone nachzubilden.

Aussehen
========
Die Seite, die Sie über einen iframe integrieren sollte keinen Seitenkopf oder dergleichen enthalten. Die Seite fügt sich dann nahtlos in Ihren Plone-Auftritt ein und für Besucher sieht es nach einem einheitlichen Auftritt aus.

Beispiel: Das Plone-Handbuch als Webseite liegt auf einem externen Server und wird via iframe in die Plone-Webseite integriert.

.. _fig_window-anzeigen:

.. figure:: ./images/window-anzeigen.png
   :width: 80%
   :alt: Window / iframe anzeigen

   Window / iframe anzeigen


Window / iframe hinzufügen
==========================

Um einen iframe hinzuzufügen, klicken Sie auf "hinzufügen" und dann auf "window"

.. _fig_window-hinzufuegen:

.. figure:: ./images/window_hinzufuegen.png
   :width: 60%
   :alt: Window / iframe hinzufügen

   Window / iframe hinzufügen

In dem nachfolgenden Dialog geben Sie bitte einen Titel an und geben Sie unter »Webadressee« die zu integrierende URL an.
Mit den Feldern für »Höhe« und »Breite« müssen Sie experimentieren bis Sie eine zufriedenstellende Anzeige erreichen. Sie bestimmen hier den Platz, den der iframe einnehmen darf. Versuchen Sie eine Einstellung zu finden, die Scrollbars innerhalb des iframes vermeidet. Blenden Sie bei Bedarf die Portletspalten aus, um dem iframe mehr Platz zu ermöglichen.
Die restlichen Optionen lassen Sie im Normalfall auf den Standardwerten.

  .. _fig_window-hinzufuegen_2:

  .. figure:: ./images/window_hinzufuegen_2.png
     :width: 80%
     :alt: Window / iframe hinzufügen

     Window / iframe hinzufügen

Sicherheit, Integrität und https
--------------------------------
Achten Sie bitte darauf nur vertrauenswürdige ("sichere") Seiten via iframe in Ihren Auftritt zu integrieren. Über den iframe sieht die fremde Seite aus wie eine Uni-Bonn-URL. Achten Sie auch darauf, dass Sie die "Erlaubnis" haben, die Seite via iframe zu integrieren.
Mittelfristig werden alle Uni-Bonn-Plones auf https umgestellt und sind dann nur noch über https zu erreichen. Für iframes heißt dies, dass diese dann nur noch angezeigt werden, wenn es sich hier auch um eine https-URL handelt.
